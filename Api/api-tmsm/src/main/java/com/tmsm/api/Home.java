package com.tmsm.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/home")
public class Home {

	@RequestMapping(method = RequestMethod.GET)
	public String home() {
		return "Home";
	}
	
	@RequestMapping(value = "/name" , method =RequestMethod.GET)
	public String getName() {
		return "Rachata";
	}
	
	
	@RequestMapping(value = "/fullname" , method =RequestMethod.GET)
	public String getFullname() {
		return "Rachata Suriya";
	}
	
	
}
