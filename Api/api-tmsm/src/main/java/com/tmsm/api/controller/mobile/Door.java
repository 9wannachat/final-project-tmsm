package com.tmsm.api.controller.mobile;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.print.attribute.standard.Media;
import javax.xml.crypto.Data;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.Status;
import com.tmsm.api.model.door.ResDoor;
import com.tmsm.api.model.mobile.ReqDoor;
import com.tmsm.api.utility.Database;

@RestController
@RequestMapping("/door")
public class Door {
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResDoor> ResponseDoor() {
		ResDoor door = new ResDoor();
		PreparedStatement preparedStatement = null;
		Connection con = Database.connectDatabase();
		ResultSet resultSet = null;
		
		try {
			String sql ="select * from door_user inner join user_detail on user_detail_id = user where status = 0";
			preparedStatement = con.prepareStatement(sql);
			resultSet = preparedStatement.executeQuery();
			
			List<com.tmsm.api.model.door.Door> listDoor = new ArrayList<com.tmsm.api.model.door.Door>();
			
			while (resultSet.next()) {
				com.tmsm.api.model.door.Door mdoor = new com.tmsm.api.model.door.Door();
				mdoor.setId(resultSet.getInt(1));
				mdoor.setUser(resultSet.getInt(2));
				mdoor.setThing(resultSet.getInt(3));
				mdoor.setStatus(resultSet.getInt(4));
				mdoor.setDt(resultSet.getString(5));				
				mdoor.setFname(resultSet.getString(7));
				mdoor.setLname(resultSet.getString(8));
				
				listDoor.add(mdoor);
			}
			
			door.setCodeStatus(200);
			door.setNameStatus("Success");
			door.setDoor(listDoor);
			
		} catch (SQLException e) {
			door.setCodeStatus(500);
			door.setNameStatus(e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(door);
		}
	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Status> reqDoor(@RequestBody ReqDoor reqDoor) {

		Connection con = null;
		PreparedStatement preparedStatement = null;

		Status status = new Status();

		try {

			con = Database.connectDatabase();
			String sql = "select * from door_user where user = ?";
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, reqDoor.getUserID());

			ResultSet res = preparedStatement.executeQuery();
			int count = Database.countResultSet(res);

			if (count == 0) {

				sql = "insert into door_user (user , thing) value(? , ?) ";
				preparedStatement = con.prepareStatement(sql);
				preparedStatement.setInt(1, reqDoor.getUserID());
				preparedStatement.setInt(2, reqDoor.getThingID());

				boolean statusSQL = preparedStatement.execute();
				if (!statusSQL) {
					status.setCodeStatus(200);
					status.setNameStatus("Success");
				} else {
					status.setCodeStatus(204);
					status.setNameStatus("Unsuccess");
				}

			} else {
				status.setCodeStatus(202);
				status.setNameStatus("Your request already exists.");
			}
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus("Unsuccess :" + e.getErrorCode() + " " + e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}

	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Status> updateStatus(@RequestBody ReqDoor reqDoor) {

		Connection con = null;
		PreparedStatement preparedStatement = null;

		Status status = new Status();
		try {
			String sql = "update door_user set status = ? where user = ?";
			con = Database.connectDatabase();
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, reqDoor.getStatus());
			preparedStatement.setInt(2, reqDoor.getUserID());

			boolean statusSQL = preparedStatement.execute();

			if (!statusSQL) {
				status.setCodeStatus(200);
				status.setNameStatus("Success");

			} else {
				status.setCodeStatus(204);
				status.setNameStatus("Unsuccess");

			}
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus("Unsuccess : " + e.getErrorCode() + " " + e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}

	@CrossOrigin
	@RequestMapping(value = "/{idUser}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Status> checkPrivilege(@PathVariable("idUser") int idUser) {

		Connection con = null;
		PreparedStatement preparedStatement = null;

		Status status = new Status();

		try {

			con = Database.connectDatabase();
			String sql = "select * from door_user where user = ?";
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, idUser);

			ResultSet res = preparedStatement.executeQuery();
			int count = Database.countResultSet(res);

			if (count == 0) {

				status.setCodeStatus(204);
				status.setNameStatus("No request");

			} else {


				int statusDoor = res.getInt("status");

				if (statusDoor == 1) {
					status.setCodeStatus(200);
					status.setNameStatus("Can control");
				} else {
					status.setCodeStatus(204);
					status.setNameStatus("Unable to control");
				}
			}
		} catch (SQLException e) {
			status.setCodeStatus(500);
			status.setNameStatus("Unsuccess :" + e.getErrorCode() + " " + e.getMessage());
		} finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}

	}
}
