package com.tmsm.api.controller.mobile;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tmsm.api.model.Status;
import com.tmsm.api.utility.Database;


@RestController
@RequestMapping("/image")
public class Images {	
	

	@RequestMapping(method = RequestMethod.POST) 
	@CrossOrigin
	public ResponseEntity<Status>  uploadImage2(@RequestParam("images") MultipartFile file , @RequestParam("areaName") String areaName ,@RequestParam("areaID") Integer areaID){

		Status status = new Status();
		PreparedStatement prepared = null;
		Connection con = Database.connectDatabase();
		
		String fileName = null;
    	if (!file.isEmpty()) {
            try {   
	                fileName = file.getOriginalFilename();
	                File f = new File("/home/server2/img/"+fileName);  
	                
	                Set<PosixFilePermission> pfpSet = new HashSet<PosixFilePermission>();
	        		
	        		pfpSet.add(PosixFilePermission.OWNER_READ);
	        		pfpSet.add(PosixFilePermission.OWNER_WRITE);
	        		pfpSet.add(PosixFilePermission.OWNER_EXECUTE);
	        		
	        		pfpSet.add(PosixFilePermission.GROUP_READ);
	        		pfpSet.add(PosixFilePermission.GROUP_WRITE);
	        		pfpSet.add(PosixFilePermission.GROUP_EXECUTE);		
	        	
	        		pfpSet.add(PosixFilePermission.OTHERS_EXECUTE);
	        		pfpSet.add(PosixFilePermission.OTHERS_READ);
	        		pfpSet.add(PosixFilePermission.OTHERS_WRITE);	
	        		
	                byte[] bytes = file.getBytes();
	                BufferedOutputStream buffStream =  new BufferedOutputStream(new FileOutputStream(f));                
	                buffStream.write(bytes);
	                buffStream.close();
	                
	                Files.setPosixFilePermissions(Paths.get("/home/server2/img/"+fileName), pfpSet);
	                
	                System.out.println(areaName);
	                
	                String pathFile = "/home/server2/img/"+fileName;
	                String sql = "insert into detail_area (dear_name,dear_image,dear_area) values (?,?,?)";
	                
	                byte ptext[] = areaName.getBytes("ISO-8859-1"); 
	                String value = new String(ptext, "UTF-8"); 
	                
	                prepared = con.prepareStatement(sql);
	                prepared.setString(1, value);
	                prepared.setString(2, fileName);
	                prepared.setInt(3, areaID);
	                
	                boolean statusCreate = prepared.execute();
	                
	                if(!statusCreate) {
	                	status.setCodeStatus(200);
	                	status.setNameStatus("Success");
	                }else {
	                	status.setCodeStatus(204);
	                	status.setNameStatus("Unsuccess");
	                }	               
	                
            } catch (SQLException e) {
	            	status.setCodeStatus(500);
	                status.setNameStatus(e.getMessage());	               
             }finally {
            	 Database.closeConnection(con);
            	 return ResponseEntity.ok().body(status);
             }
        } else {
        	status.setCodeStatus(204);
            status.setNameStatus("File Empty");
            return ResponseEntity.ok().body(status);

        }
	
    }
	
	
	@CrossOrigin
	@RequestMapping(value = "/update" ,method = RequestMethod.PUT)
	public ResponseEntity<Status> updateArea(@RequestParam("images") MultipartFile file , @RequestParam("areaName") String areaName,@RequestParam("detailID") Integer detailID){
		Status status = new Status();
		PreparedStatement prepared = null;
		Connection con = Database.connectDatabase();
		
		String fileName = null;
    	if (!file.isEmpty()) {
            try {   
	                fileName = file.getOriginalFilename();
	                File f = new File("/home/server2/img/"+fileName);  
	                
	                Set<PosixFilePermission> pfpSet = new HashSet<PosixFilePermission>();
	        		
	        		pfpSet.add(PosixFilePermission.OWNER_READ);
	        		pfpSet.add(PosixFilePermission.OWNER_WRITE);
	        		pfpSet.add(PosixFilePermission.OWNER_EXECUTE);
	        		
	        		pfpSet.add(PosixFilePermission.GROUP_READ);
	        		pfpSet.add(PosixFilePermission.GROUP_WRITE);
	        		pfpSet.add(PosixFilePermission.GROUP_EXECUTE);		
	        	
	        		pfpSet.add(PosixFilePermission.OTHERS_EXECUTE);
	        		pfpSet.add(PosixFilePermission.OTHERS_READ);
	        		pfpSet.add(PosixFilePermission.OTHERS_WRITE);	
	        		
	                byte[] bytes = file.getBytes();
	                BufferedOutputStream buffStream =  new BufferedOutputStream(new FileOutputStream(f));                
	                buffStream.write(bytes);
	                buffStream.close();
	                
	                Files.setPosixFilePermissions(Paths.get("/home/server2/img/"+fileName), pfpSet);
	                
	                String pathFile = "/home/server2/img/"+fileName;
	                
	                String sql = "update detail_area set dear_name= ?,dear_image= ? where dear_id = ?";
	                prepared = con.prepareStatement(sql);
	                prepared.setString(1, areaName);
	                prepared.setString(2, pathFile);	                
	                prepared.setInt(3, detailID);
	                
	                boolean statusCreate = prepared.execute();
	                
	                if(!statusCreate) {
	                	status.setCodeStatus(200);
	                	status.setNameStatus("Success");
	                }else {
	                	status.setCodeStatus(204);
	                	status.setNameStatus("Unsuccess");
	                }	               
	                
            } catch (SQLException e) {
	            	status.setCodeStatus(500);
	                status.setNameStatus(e.getMessage());	               
             }finally {
            	 Database.closeConnection(con);
            	 return ResponseEntity.ok().body(status);
             }
        } else {
        	
				status.setCodeStatus(500);
                status.setNameStatus("Files Empty");	
                return ResponseEntity.ok().body(status);
        }
	}
	
	
	

}
