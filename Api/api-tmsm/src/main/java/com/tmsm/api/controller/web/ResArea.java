package com.tmsm.api.controller.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tmsm.api.model.mobile.Thing;
import com.tmsm.api.model.mobile.ThingsPosition;
import com.tmsm.api.model.web.ReqIDArea;
import com.tmsm.api.utility.Database;
import com.tmsm.model.strong.DetailArea;
import com.tmsm.model.strong.ListArea;
import com.tmsm.model.strong.ResAreaDevices;

@RestController
@CrossOrigin
@RequestMapping("/allarea")
public class ResArea {
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<ListArea> getArea(){
		ListArea area = new ListArea();
		
		PreparedStatement preparedStatement = null;
		Connection con = Database.connectDatabase(); 
		ResultSet re = null;
		
		try {
			String sql = "select * from area_of_devices";
			preparedStatement = con.prepareStatement(sql);
			re = preparedStatement.executeQuery();
			
			List<ResAreaDevices> list = new ArrayList<ResAreaDevices>();
			
			while(re.next()) {
				String sql2 = "select * from detail_area where dear_area = ?";
				preparedStatement = con.prepareStatement(sql2);
				preparedStatement.setInt(1, re.getInt(1));
				ResultSet resultSet = preparedStatement.executeQuery();
				
				List<DetailArea> detailAreas = new ArrayList<DetailArea>();
				
				while(resultSet.next()) {
					DetailArea detailArea = new DetailArea();
					
					detailArea.setDearId(resultSet.getInt(1));
					detailArea.setDearName(resultSet.getString(2));
					detailArea.setDearImg(resultSet.getString(3));
					detailArea.setDearArea(resultSet.getInt(4));
					
					detailAreas.add(detailArea);
				}
				
				ResAreaDevices devices = new ResAreaDevices();
				
				devices.setAreaId(re.getInt(1));
				devices.setAreaName(re.getString(2));
				devices.setAreaLat(re.getDouble(3));
				devices.setAreaLng(re.getDouble(4));
				devices.setDetailAreas(detailAreas);
				
				list.add(devices);
			}
			
			area.setAreaDevices(list);
			area.setCodeStatus(200);
			area.setNameStatus("Success");
			
		} catch (SQLException e) {
			area.setCodeStatus(500);
			area.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(area);
		}
	}
	
	
	@CrossOrigin
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
	public ResponseEntity<ReqIDArea> resDetail (@PathVariable("id") int areaId) {
		ReqIDArea area = new ReqIDArea();
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Connection con = Database.connectDatabase();
		
		try {
			String sql = "select * from detail_area where dear_area = ?";
			
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, areaId);
			resultSet = preparedStatement.executeQuery();
			
			while(resultSet.next()) {
				
				
				String sql2 = "select * from things where things_area = ?";
				preparedStatement = con.prepareStatement(sql2);
				preparedStatement.setInt(1, areaId);
				ResultSet set = preparedStatement.executeQuery();
				
				List<ThingsPosition> things = new ArrayList<ThingsPosition>();
				
				while(set.next()) {
					ThingsPosition all = new ThingsPosition();
					all.setThingsID(set.getInt(1));
					all.setThingsName(set.getString(2));
					all.setThingsType(set.getString(4));
					all.setThingsOffsetX(set.getDouble(6));
					all.setThingsOffsetY(set.getDouble(7));
					
					things.add(all);
				}
				
				area.setCodeStatus(200);
				area.setNameStatus("Success");
				area.setDearId(resultSet.getInt(1));
				area.setDearName(resultSet.getString(2));
				area.setDearImg(resultSet.getString(3));
				area.setDearArea(resultSet.getInt(4));
				area.setThings(things);
			}
			
		} catch (SQLException e) {
			area.setCodeStatus(500);
			area.setNameStatus(e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(area);
		}
	}
}
