package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;

public class AllDevices extends Status {
	List<ResDeviceCtrlStatus> devices;

	public List<ResDeviceCtrlStatus> getDevices() {
		return devices;
	}

	public void setDevices(List<ResDeviceCtrlStatus> devices) {
		this.devices = devices;
	}
	
}
