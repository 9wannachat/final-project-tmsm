package com.tmsm.api.model.web;

import java.util.List;

public class AreaDevices {
	private int areaId;
	private String areaName;
	private Double areaLat;
	private Double areaLng;
	private List<DetailArea> detailArea;
	
	public List<DetailArea> getDetailArea() {
		return detailArea;
	}
	public void setDetailArea(List<DetailArea> detailArea) {
		this.detailArea = detailArea;
	}
	public int getAreaId() {
		return areaId;
	}
	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public Double getAreaLat() {
		return areaLat;
	}
	public void setAreaLat(Double areaLat) {
		this.areaLat = areaLat;
	}
	public Double getAreaLng() {
		return areaLng;
	}
	public void setAreaLng(Double areaLng) {
		this.areaLng = areaLng;
	}
}
