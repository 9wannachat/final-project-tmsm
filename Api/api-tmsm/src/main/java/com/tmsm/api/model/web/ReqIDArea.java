package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;
import com.tmsm.api.model.mobile.Thing;
import com.tmsm.api.model.mobile.ThingsPosition;

public class ReqIDArea extends Status{
	private int dearId;
	private String dearName;
	private String dearImg;
	private int dearArea;
	private List<ThingsPosition> things;
	
	public List<ThingsPosition> getThings() {
		return things;
	}
	public void setThings(List<ThingsPosition> things) {
		this.things = things;
	}
	public int getDearId() {
		return dearId;
	}
	public void setDearId(int dearId) {
		this.dearId = dearId;
	}
	public String getDearName() {
		return dearName;
	}
	public void setDearName(String dearName) {
		this.dearName = dearName;
	}
	public String getDearImg() {
		return dearImg;
	}
	public void setDearImg(String dearImg) {
		this.dearImg = dearImg;
	}
	public int getDearArea() {
		return dearArea;
	}
	public void setDearArea(int dearArea) {
		this.dearArea = dearArea;
	}
}
