package com.tmsm.api.model.web;

import java.util.List;

import com.tmsm.api.model.Status;

public class ResAllArea extends Status{
	private List<AreaDevices> areaDevices;

	public List<AreaDevices> getAreaDevices() {
		return areaDevices;
	}

	public void setAreaDevices(List<AreaDevices> areaDevices) {
		this.areaDevices = areaDevices;
	}
}
