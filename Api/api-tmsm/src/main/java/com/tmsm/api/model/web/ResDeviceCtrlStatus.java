package com.tmsm.api.model.web;

import java.util.List;

public class ResDeviceCtrlStatus {
	private String deviceName;
	private String devicePrivilege;
	private int deviceCH;
	private int deviceID;
	private int devicePriority;
	private int deviceThing;
	private int deviceVisibility;
	private List<DeviceStatusControll> status;
	
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public String getDevicePrivilege() {
		return devicePrivilege;
	}
	public void setDevicePrivilege(String devicePrivilege) {
		this.devicePrivilege = devicePrivilege;
	}
	public int getDeviceCH() {
		return deviceCH;
	}
	public void setDeviceCH(int deviceCH) {
		this.deviceCH = deviceCH;
	}
	public int getDeviceID() {
		return deviceID;
	}
	public void setDeviceID(int deviceID) {
		this.deviceID = deviceID;
	}
	public int getDevicePriority() {
		return devicePriority;
	}
	public void setDevicePriority(int devicePriority) {
		this.devicePriority = devicePriority;
	}
	public int getDeviceThing() {
		return deviceThing;
	}
	public void setDeviceThing(int deviceThing) {
		this.deviceThing = deviceThing;
	}
	public int getDeviceVisibility() {
		return deviceVisibility;
	}
	public void setDeviceVisibility(int deviceVisibility) {
		this.deviceVisibility = deviceVisibility;
	}
	public List<DeviceStatusControll> getStatus() {
		return status;
	}
	public void setStatus(List<DeviceStatusControll> status) {
		this.status = status;
	}
	
}
