package app.coe.com.tmsm;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;
import java.util.Map;

public class CanvasMapActivity extends AppCompatActivity {


    private LinearLayout linearLayout;
    private  String name;
    private int heightToolbar;
    private int widthScreen;
    private int heightScreen;
    private Bitmap bitmaps;



    void setBitmaps(Bitmap bitmap) {
        this.bitmaps = bitmap;
    }

    private Target mTarget = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            Log.i("onBitmapLoaded", bitmap.getHeight() + " " + bitmap.getWidth());


            setBitmaps(bitmap);

            initCanvas();
        }

        @Override
        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            Log.i("onBitmapFailed", "onBitmapFailed : " + e.getMessage());
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

            Log.i("onPrepareLoad", "onPrepareLoad");
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_canvas_map);


        FloatingActionButton btnReset = (FloatingActionButton) findViewById(R.id.btnReset);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("FloatingActionButton", "FloatingActionButton");

                if (((LinearLayout) linearLayout).getChildCount() > 0) {
                    ((LinearLayout) linearLayout).removeAllViews();
                    initCanvas();

                }
            }
        });


        Picasso.get()
                .load("http://203.158.177.150:8282/hall-1.png")
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .resize(1024, 2048)
                .into(mTarget);

        
    }

    private void initCanvas() {
        TypedValue tv = new TypedValue();
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            heightToolbar = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            widthScreen = displayMetrics.widthPixels;
            heightScreen = displayMetrics.heightPixels;



            PinchZoomPan view = new PinchZoomPan(getApplicationContext(), heightScreen, widthScreen, heightToolbar, this.bitmaps);

            int canvasW = (int) view.getImageW();
            int canvasH = (int) view.getImageH();

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(canvasW, canvasH);

            linearLayout.addView(view, params);


        }
    }
}
