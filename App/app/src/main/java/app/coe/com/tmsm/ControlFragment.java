package app.coe.com.tmsm;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationViewPager;

import java.util.ArrayList;

import app.coe.com.tmsm.adapter.ScanThingPage;


/**
 * A simple {@link Fragment} subclass.
 */
public class ControlFragment extends Fragment {



    private AHBottomNavigationViewPager viewPager;
    private static AHBottomNavigation bottomNavigation;

    private int idGroup;
    private ArrayList<AHBottomNavigationItem> bottomNavigationItems = new ArrayList<>();
    private View v;


    public ControlFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v=  inflater.inflate(R.layout.fragment_control, container, false);

        idGroup = getArguments().getInt("idGroup");
        initUI(idGroup);
        return v;
    }

    private void initUI(int idGroup) {


        bottomNavigation =   v.findViewById(R.id.bottom_navigation);
        viewPager =  v.findViewById(R.id.view_pager);

        AHBottomNavigationItem item1 = new AHBottomNavigationItem("IO", R.drawable.access_point);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem("Sensor", R.drawable.database);


        bottomNavigationItems.clear();
        bottomNavigationItems.add(item1);
        bottomNavigationItems.add(item2);

        bottomNavigation.removeAllItems();
        bottomNavigation.addItems(bottomNavigationItems);

        bottomNavigation.setTranslucentNavigationEnabled(true);

        ControlPageIOSensor adapter = new ControlPageIOSensor(getChildFragmentManager() , idGroup);

        viewPager.removeAllViews();
        viewPager.setAdapter(adapter);


        bottomNavigation.setCurrentItem(0);
        viewPager.setCurrentItem(0);

        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {

                Log.i("position" ,position+"");


                viewPager.setCurrentItem(position);

                Log.i("PAGE COUNT "  , String.valueOf(viewPager.getCurrentItem()));

                return true;
            }
        });


    }
    public static void showOrHideBottomNavigation(boolean show) {
        if (show) {
            bottomNavigation.restoreBottomNavigation(true);
        } else {
            bottomNavigation.hideBottomNavigation(true);
        }
    }

}
