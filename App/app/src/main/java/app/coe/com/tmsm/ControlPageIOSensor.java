package app.coe.com.tmsm;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class ControlPageIOSensor extends FragmentPagerAdapter {

    private int idGroup;

    public ControlPageIOSensor(FragmentManager fm  , int idGroup) {
        super(fm);
        this.idGroup = idGroup;
    }


    @Override
    public Fragment getItem(int i) {
        if(i == 0)
            return  new IOControlFragment(idGroup);
        else if(i == 1)
            return new SensorFragment(idGroup);
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
