package app.coe.com.tmsm;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import java.util.List;
import app.coe.com.tmsm.models.Device;



/**
 * A simple {@link Fragment} subclass.
 */
public class DeviceFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    public DeviceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_device, container, false);


        mRecyclerView = (RecyclerView) v.findViewById(R.id.device);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext() , LinearLayout.VERTICAL));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));




        List<Device> list  = (List<Device>)getArguments().getSerializable("data");

        int types = getArguments().getInt("types");

        if(list != null){
            mAdapter = new app.coe.com.tmsm.adapter.Device(types , list, getContext());

            mRecyclerView.setAdapter(mAdapter);
            Log.i("LIST DEBUG " , list.size()+"");
        }

        return v;
    }



}
