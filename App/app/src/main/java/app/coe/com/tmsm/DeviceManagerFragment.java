package app.coe.com.tmsm;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import app.coe.com.tmsm.adapter.DeviceOfUser;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.Device;
import app.coe.com.tmsm.models.ReqUser;
import app.coe.com.tmsm.models.RootResDeviceOfUser;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class DeviceManagerFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;


    public DeviceManagerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v =  inflater.inflate(R.layout.fragment_device_manager, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.deviceManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext() , LinearLayout.VERTICAL));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        callAPI(TMSMPreferences.getUserID(getContext()));
        return v;
    }



    public void callAPI(int reqUser){

        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);
        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        FeedData feedData = retrofit.create(FeedData.class);

        String token = TMSMPreferences.getToken(getContext());

        Call<RootResDeviceOfUser> call =  feedData.getDeviceOfUser(token , reqUser);

        call.enqueue(new Callback<RootResDeviceOfUser>() {
            @Override
            public void onResponse(Call<RootResDeviceOfUser> call, Response<RootResDeviceOfUser> response) {

                        if(response.body().getData() != null){
                            mAdapter = new DeviceOfUser(response.body().getData(), getContext());

                            mRecyclerView.setAdapter(mAdapter);
                        }else{

                            Log.i("ERROR RES "  , response.body().getNameStatus());
                            Toast.makeText(getContext() , "No device" , Toast.LENGTH_LONG).show();
                        }
            }

            @Override
            public void onFailure(Call<RootResDeviceOfUser> call, Throwable t) {
                TMSMPreferences.toLogin(getContext());
                Toast.makeText(getContext() , t.getMessage() , Toast.LENGTH_LONG).show();
            }
        });

    }

}
