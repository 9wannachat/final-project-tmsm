package app.coe.com.tmsm;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

import app.coe.com.tmsm.utility.MqttHelper;
import app.coe.com.tmsm.utility.TMSMPreferences;


/**
 * A simple {@link Fragment} subclass.
 */
public class DoorControlFragment extends Fragment {



    TextView txtStatus;
    Button btnOpen;
    Button btnClose;
    Button btnStop;

    private MqttHelper mqttHelper;

    public DoorControlFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_door_control, container, false);


        startMqtt();

        btnOpen = (Button)v.findViewById(R.id.btnOpen);
        btnClose = (Button)v.findViewById(R.id.btnClose);
        btnStop = (Button)v.findViewById(R.id.btnStop);

        txtStatus = (TextView)v.findViewById(R.id.txtStatus);


        btnOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                try {
                    int idUser = TMSMPreferences.getUserID(getContext());
                    JSONObject json = new JSONObject();
                    json.put("status", 1);
                    json.put("idUser" , idUser);

                    Log.d("data" , json.toString());
                    sendDataToMqttServer(json.toString());
                }catch (JSONException e){

                }



            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int idUser = TMSMPreferences.getUserID(getContext());
                    JSONObject json = new JSONObject();
                    json.put("status", 0);
                    json.put("idUser" , idUser);
                    Log.d("data" , json.toString());
                    sendDataToMqttServer(json.toString());
                }catch (JSONException e){

                }
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int idUser = TMSMPreferences.getUserID(getContext());
                    JSONObject json = new JSONObject();
                    json.put("status", 2);
                    json.put("idUser" , idUser);
                    Log.d("data" , json.toString());
                    sendDataToMqttServer(json.toString());
                }catch (JSONException e){

                }
            }
        });

        return v;
    }


    private void startMqtt(){



        long s = System.currentTimeMillis() % 1000;
        mqttHelper = new MqttHelper(getContext() , "ControlMobile"+s);
        mqttHelper.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {
                mqttHelper.subscribeToTopic("doorFeedBack");
                Log.i("MQTT " , "mqttHelper adapter");
            }

            @Override
            public void connectionLost(Throwable throwable) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {

                if(topic.equals("doorFeedBack")){





                    try {
                        JSONObject jsonObject = new JSONObject(mqttMessage.toString());

                        Log.i("feedBack " , jsonObject.toString());
                        int status = jsonObject.getInt("status");


                        if(status == 1){
                            txtStatus.setText("Status : Open");
                        }else if(status == 0){
                            txtStatus.setText("Status : Close");
                        }


                    }catch (JSONException err){

                        Log.i("Error", err.toString());
                    }



                }

            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

            }
        });
    }

    public void sendDataToMqttServer(String data){


        Log.i("data" , data);
        MqttMessage message = new MqttMessage();
        message.setPayload(data.getBytes());
        message.setQos(0);
        message.setId(1);

        try {

            mqttHelper.mqttAndroidClient.publish("doorStatus" , message);

        }catch (MqttException mqtt){

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("onPause"  , "onPause Door");
        mqttHelper.unSubscribeToTopic("doorStatus  ");
    }
}
