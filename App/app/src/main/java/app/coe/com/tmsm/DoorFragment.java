package app.coe.com.tmsm;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import app.coe.com.tmsm.adapter.DoorReq;
import app.coe.com.tmsm.adapter.PermitTimer;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.ResDoor;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class DoorFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    public DoorFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v= inflater.inflate(R.layout.fragment_door, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.reqDoor);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext() , LinearLayout.VERTICAL));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        int privilege = TMSMPreferences.getPrivilege(getContext());
        int userID = TMSMPreferences.getUserID(getContext());


        if(privilege == 1){
            callAdminDoor();
        }else if(privilege == 2){
            callAdminDoorArea();
        }
        return v;
    }

    public void callAdminDoor() {

        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();


        FeedData feedData = retrofit.create(FeedData.class);
        String token = TMSMPreferences.getToken(getContext());



        feedData.getReqDoor(token).enqueue(new Callback<ResDoor>() {
            @Override
            public void onResponse(Call<ResDoor> call, Response<ResDoor> response) {

                if(response.body().getCodeStatus() == 200){
                    mAdapter = new DoorReq(response.body().getDoor(), getContext());
                    mRecyclerView.setAdapter(mAdapter);
                }else{
                    Toast.makeText(getContext() , response.body().getNameStatus(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResDoor> call, Throwable t) {
                TMSMPreferences.toLogin(getContext());
                Toast.makeText(getContext() , t.getMessage() , Toast.LENGTH_SHORT).show();
            }
        });




    }


    public void callAdminDoorArea() {

        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);

        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();


        FeedData feedData = retrofit.create(FeedData.class);
        String token = TMSMPreferences.getToken(getContext());


        int idUser = TMSMPreferences.getUserID(getContext());

        feedData.getReqDoor(token , idUser).enqueue(new Callback<ResDoor>() {
            @Override
            public void onResponse(Call<ResDoor> call, Response<ResDoor> response) {

                if(response.body().getCodeStatus() == 200){
                    mAdapter = new DoorReq(response.body().getDoor(), getContext());
                    mRecyclerView.setAdapter(mAdapter);
                }else{
                    Toast.makeText(getContext() , response.body().getNameStatus(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResDoor> call, Throwable t) {
                TMSMPreferences.toLogin(getContext());
                Toast.makeText(getContext() , t.getMessage() , Toast.LENGTH_SHORT).show();
            }
        });




    }

}
