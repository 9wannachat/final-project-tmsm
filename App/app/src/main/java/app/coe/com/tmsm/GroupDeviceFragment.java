package app.coe.com.tmsm;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import app.coe.com.tmsm.adapter.DeviceGroupList;
import app.coe.com.tmsm.adapter.GroupDevice;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.RootDeviceGroupUser;
import app.coe.com.tmsm.models.RootGruopDevice;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class GroupDeviceFragment extends Fragment {


    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private int idGroup;
    public GroupDeviceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment



        View v = inflater.inflate(R.layout.fragment_group_device, container, false);;
        mRecyclerView = (RecyclerView) v.findViewById(R.id.groupDelete);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext() , LinearLayout.VERTICAL));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        idGroup = getArguments().getInt("idGroup");


        Log.i("ddd" , idGroup + "");


        callAPI();
        return v;
    }

    public void callAPI(){


        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);

        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        FeedData feedData = retrofit.create(FeedData.class);


        int User = TMSMPreferences.getUserID(getContext());

        String token = TMSMPreferences.getToken(getContext());

        feedData.getDeviceGroup(token , User , idGroup).enqueue(new Callback<RootDeviceGroupUser>() {
            @Override
            public void onResponse(Call<RootDeviceGroupUser> call, Response<RootDeviceGroupUser> response) {

                if(response.body().getCodeStatus() == 200){

                    mAdapter = new DeviceGroupList(response.body().getData(), getContext());

                    mRecyclerView.setAdapter(mAdapter);

                }else{
                    Toast.makeText(getContext() , response.body().getNameStatus() , Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<RootDeviceGroupUser> call, Throwable t) {
                TMSMPreferences.toLogin(getContext());
                    Toast.makeText(getContext() , t.getMessage()  , Toast.LENGTH_SHORT).show();
            }
        });



    }


}
