package app.coe.com.tmsm;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import app.coe.com.tmsm.adapter.DeviceOfUser;
import app.coe.com.tmsm.adapter.GroupDevice;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.ReqCreateGroup;
import app.coe.com.tmsm.models.ReqPrivilege;
import app.coe.com.tmsm.models.ResLogin;
import app.coe.com.tmsm.models.RootGruopDevice;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class GroupFragment extends Fragment {


    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    public GroupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_group, container, false);


        mRecyclerView = (RecyclerView) v.findViewById(R.id.groupSelect);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext() , LinearLayout.VERTICAL));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        Button btnCreat = (Button)  v.findViewById(R.id.btnCreate);

        btnCreat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               Log.i("ewfefw" ,  TMSMPreferences.getUserID(getContext())+"");
                createGroupNameAlert();
            }
        });

        callAPI(TMSMPreferences.getUserID(getContext()));
        return v;
    }


    public void callAPI(int userID){

        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);


        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)

                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        FeedData feedData = retrofit.create(FeedData.class);

        String token = TMSMPreferences.getToken(getContext());
        Call<RootGruopDevice> call =  feedData.getDeviceGroup(token, userID);


        call.enqueue(new Callback<RootGruopDevice>() {
            @Override
            public void onResponse(Call<RootGruopDevice> call, Response<RootGruopDevice> response) {
                Log.i("Group" , response.code()+"");



                if(response.body().getGroups() != null && response.body().getCodeStatus() == 200){

                    app.coe.com.tmsm.models.GroupDevice groupDevice = new app.coe.com.tmsm.models.GroupDevice();


                    groupDevice.setNameGroup("ทั้งหมด");
                    groupDevice.setIdGroup(0);

                    List<app.coe.com.tmsm.models.GroupDevice> list = new ArrayList<app.coe.com.tmsm.models.GroupDevice>();

                    list.add(groupDevice);

                    list.addAll(response.body().getGroups());
                    mAdapter = new GroupDevice(list, getContext());

                    mRecyclerView.setAdapter(mAdapter);
                }else{
                    app.coe.com.tmsm.models.GroupDevice groupDevice = new app.coe.com.tmsm.models.GroupDevice();
                    groupDevice.setNameGroup("ทั้งหมด");
                    groupDevice.setIdGroup(0);

                    List<app.coe.com.tmsm.models.GroupDevice> list = new ArrayList<app.coe.com.tmsm.models.GroupDevice>();

                    list.add(groupDevice);


                    mAdapter = new GroupDevice(list, getContext());

                    mRecyclerView.setAdapter(mAdapter);

                }

            }

            @Override
            public void onFailure(Call<RootGruopDevice> call, Throwable t) {
                TMSMPreferences.toLogin(getContext());
            }
        });



    }

    private void createGroupNameAlert(){

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("ป้อนชื่อกลุ่ม");


        final EditText input = new EditText(getContext());

        input.setInputType(InputType.TYPE_CLASS_TEXT );
        builder.setView(input);


        builder.setPositiveButton("สร้าง", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {

                callAPI( input.getText().toString());
            }
        });
        builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();

    }


    public void callAPI(String name){


        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        final FeedData feedData = retrofit.create(FeedData.class);

        int uid = TMSMPreferences.getUserID(getContext());

        ReqCreateGroup reqCreateGroup = new ReqCreateGroup();
        reqCreateGroup.setNameGroup(name);
        reqCreateGroup.setUserID(uid);


        String token = TMSMPreferences.getToken(getContext());


        Call<Status> call = feedData.createGroup(token , reqCreateGroup);
        call.enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                Toast.makeText(getContext() , response.body().getNameStatus(), Toast.LENGTH_LONG).show();


                callAPI(uid);
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                TMSMPreferences.toLogin(getContext());
                Toast.makeText(getContext() , t.getMessage() , Toast.LENGTH_LONG).show();
            }
        });

    }


}
