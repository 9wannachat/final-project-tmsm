package app.coe.com.tmsm;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import app.coe.com.tmsm.adapter.GroupManage;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.ReqCreateGroup;
import app.coe.com.tmsm.models.ReqDevice;
import app.coe.com.tmsm.models.RootGruopDevice;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class GroupListFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    String nameDevice;
    int deviceID;
    public GroupListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_group_list, container, false);


        mRecyclerView = (RecyclerView) v.findViewById(R.id.groupSelect);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext() , LinearLayout.VERTICAL));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        nameDevice = getArguments().getString("deviceName");
        deviceID = getArguments().getInt("deviceID");


        Log.i("dd" , nameDevice + deviceID);


        Button btnAddGroup = (Button) v.findViewById(R.id.btnAddGroup);

        btnAddGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertInput();
            }
        });




        int user = TMSMPreferences.getUserID(getContext());
        callAPI(user , nameDevice , deviceID);
        return v;
    }



    private void showAlertInput (){



        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Title");


        final EditText input = new EditText(getContext());


        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String  nameGroup = input.getText().toString();

                Log.i("AlertDialog" , nameGroup);

                ReqCreateGroup reqCreateGroup = new ReqCreateGroup();
                reqCreateGroup.setNameGroup(nameGroup);
                reqCreateGroup.setUserID(TMSMPreferences.getUserID(getContext()));

                callAPI(reqCreateGroup);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }




    public void callAPI(ReqCreateGroup reqCreateGroup ){


        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);

        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        FeedData feedData = retrofit.create(FeedData.class);
        String token = TMSMPreferences.getToken(getContext());

        Call<Status> call =  feedData.createGroup(token , reqCreateGroup);
        call.enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                Log.i("Debug " , response.body().getNameStatus());
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                TMSMPreferences.toLogin(getContext());
            }
        });
    }

    public void callAPI(int userID , String name , int id){


        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);

        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        FeedData feedData = retrofit.create(FeedData.class);

        String token = TMSMPreferences.getToken(getContext());
        Call<RootGruopDevice> call =  feedData.getDeviceGroup(token , userID);


        call.enqueue(new Callback<RootGruopDevice>() {
            @Override
            public void onResponse(Call<RootGruopDevice> call, Response<RootGruopDevice> response) {
                Log.i("Group" , response.body().getCodeStatus()+"");



                if(response.body().getGroups() != null){
                    mAdapter = new GroupManage(response.body().getGroups(), getContext() , name , id);

                    mRecyclerView.setAdapter(mAdapter);
                }else{
                    Toast.makeText(getContext() , "ไม่มี Device" , Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<RootGruopDevice> call, Throwable t) {
                TMSMPreferences.toLogin(getContext());
            }
        });



    }


    


}
