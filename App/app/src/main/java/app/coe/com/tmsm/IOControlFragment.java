package app.coe.com.tmsm;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.List;

import app.coe.com.tmsm.adapter.DeviceOfUser;
import app.coe.com.tmsm.adapter.IODevice;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.IODeviceOfUser;
import app.coe.com.tmsm.models.ReqPrivilege;
import app.coe.com.tmsm.models.RootIODeviceOfUser;
import app.coe.com.tmsm.models.RootResDeviceOfUser;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.utility.MqttHelper;
import app.coe.com.tmsm.utility.OnBtnClickListener;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class IOControlFragment extends Fragment  {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private MqttHelper mqttHelper;

    private List<IODeviceOfUser> data;

    private  Handler handle;
    private Runnable runable;
    private int index ;
    private int group;
    private  SwipeRefreshLayout swipeRefreshLayout;
    private int idGroup;


    public IOControlFragment(){}

    public IOControlFragment(int idGroup) {

        this.idGroup = idGroup;
        Log.i("idGroup" , idGroup + "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_iocontrol, container, false);

        Log.i("idGroup" , idGroup + "");

        mRecyclerView = (RecyclerView) v.findViewById(R.id.iocontrol);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext() , LinearLayout.VERTICAL));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        swipeRefreshLayout = (SwipeRefreshLayout)v.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override

            public void onRefresh() {

                Log.i("onRefresh" , "onRefresh");
                handle = new Handler();
                callAPI(TMSMPreferences.getUserID(getContext()));
                runable = new Runnable() {

                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);

                        handle.removeCallbacks(runable); // stop runable.
                    }
                };
                handle.postDelayed(runable, 3000); // delay 3 s.
            }
        });


        Log.i("idGroup" , idGroup+"");

        if(idGroup == 0){
            callAPI(TMSMPreferences.getUserID(getContext()));
        }else{
            callAPIGroup(TMSMPreferences.getUserID(getContext()) , idGroup);
        }


        startMqtt();

        return v;
    }

    public void callAPI(int userID){

        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);

        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        Log.i("callAPI" , "callAPI");
        FeedData feedData = retrofit.create(FeedData.class);

        String token = TMSMPreferences.getToken(getContext());

        Call<RootIODeviceOfUser> call =  feedData.getDeviceIO(token , userID);
        call.enqueue(new Callback<RootIODeviceOfUser>() {
            @Override
            public void onResponse(Call<RootIODeviceOfUser> call, Response<RootIODeviceOfUser> response) {

                swipeRefreshLayout.setRefreshing(false);

                if(response.body().getDevices() != null){
                    data = response.body().getDevices();
                    mAdapter = new IODevice(data, getContext());
                    mRecyclerView.setAdapter(mAdapter);
                }else{

                    Toast.makeText(getContext() , "No Device" , Toast.LENGTH_LONG).show();
                }

                
            }

            @Override
            public void onFailure(Call<RootIODeviceOfUser> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                Log.i("onFailure" , t.getMessage());
                TMSMPreferences.toLogin(getContext());
            }
        });
    }

    public void callAPIGroup(int userID , int group){

        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);

        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        Log.i("callAPI" , "callAPI");
        FeedData feedData = retrofit.create(FeedData.class);
        String token = TMSMPreferences.getToken(getContext());
        Call<RootIODeviceOfUser> call =  feedData.getDeviceIO(token , userID , group);

        call.enqueue(new Callback<RootIODeviceOfUser>() {
            @Override
            public void onResponse(Call<RootIODeviceOfUser> call, Response<RootIODeviceOfUser> response) {

                swipeRefreshLayout.setRefreshing(false);

                if(response.body().getDevices() != null){
                    data = response.body().getDevices();
                    mAdapter = new IODevice(data, getContext());
                    mRecyclerView.setAdapter(mAdapter);
                }else{

                    Toast.makeText(getContext() , "No Device" , Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<RootIODeviceOfUser> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                Log.i("onFailure" , t.getMessage());
                TMSMPreferences.toLogin(getContext());
            }
        });

    }



    private void startMqtt(){


        long s = System.currentTimeMillis() % 1000;
        mqttHelper = new MqttHelper(getContext() , "feedBack"+s);
        mqttHelper.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {
                Log.i("MQTT " , "mqttHelper feedBackMobile");
                mqttHelper.subscribeToTopic("fb_io_mobile");
            }

            @Override
            public void connectionLost(Throwable throwable) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {

                if(topic.equals("fb_io_mobile")){
                    Log.i("Topic Sub " , mqttMessage.toString());
                    feedBackUpdate(mqttMessage.toString());
                }

            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

            }
        });

    }


    private void feedBackUpdate(String json){


        boolean status = false;
        int thing = 0;
        int ch = 0;

        try {

            JSONObject jsonObject = new JSONObject(json);
            status = jsonObject.getBoolean("status");
            thing = jsonObject.getInt("thing");
            ch = jsonObject.getInt("ch");


        }catch (JSONException err){

            Log.i("Error", err.toString());
        }

        for(IODeviceOfUser device : data){

            if(device.getThingsID() == thing && device.getDeviceCH() == ch){

                device.setRealStatus(status);
            }
        }

        mAdapter.notifyDataSetChanged();

    }


    @Override
    public void onPause() {
        super.onPause();


        Log.i("onPause" , "onPause");
        mqttHelper.unSubscribeToTopic("fb_io_mobile");
    }
}
