package app.coe.com.tmsm;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.provider.Settings;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import java.util.List;

import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.ReqLogin;
import app.coe.com.tmsm.models.ResLogin;
import app.coe.com.tmsm.utility.CryptoHash;
import app.coe.com.tmsm.utility.Privilege;

import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {


    Button btnLogin;
    EditText txtUser;
    EditText txtPass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        SharedPreferences myPrefs = this.getSharedPreferences("TMSM", Context.MODE_PRIVATE);
        String url = myPrefs.getString("TMSM_URL",null);


        if(url == null){

            TMSMPreferences.setURL(LoginActivity.this);

        }
        Log.i("url" , url+"");


        int userID = TMSMPreferences.getUserID(getApplicationContext());


        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean statusConnect = cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();



        if(!statusConnect){

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("กรุณาเชื่อมต่ออินเตอร์เน็ต")
                    .setCancelable(false)
                    .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //do things
                            android.os.Process.killProcess(android.os.Process.myPid());
                            System.exit(1);

                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();

        }


        if (userID != -1) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }


        btnLogin = (Button) findViewById(R.id.btnLogin);

        txtUser = (EditText) findViewById(R.id.txtUser);

        txtPass = (EditText) findViewById(R.id.txtPass);



        TextView register = (TextView) findViewById(R.id.register);


        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this , RegisterActivity.class));
            }
        });

        String urlCon = TMSMPreferences.getURL(LoginActivity.this);

        String urls = TMSMPreferences.getURL(LoginActivity.this);
        Log.i("url con " , url);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(urls)
                .addConverterFactory(GsonConverterFactory.create())
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        final FeedData feedData = retrofit.create(FeedData.class);
        btnLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                ReqLogin reqLogin = new ReqLogin();
                reqLogin.setUsername(txtUser.getText().toString());
                reqLogin.setPassword(CryptoHash.hash256(txtPass.getText().toString()));


                Call<ResLogin> call = feedData.userLogin(reqLogin);

                call.enqueue(new Callback<ResLogin>() {
                    @Override
                    public void onResponse(Call<ResLogin> call, Response<ResLogin> response) {


                        if (response.body().getCodeStatus() == 200) {




                            Log.i("response" , response.body().getAdminArea()+"");
                            Toast.makeText(getApplicationContext(), response.body().getNameStatus(), Toast.LENGTH_LONG).show();

                            SharedPreferences sp = getSharedPreferences("TMSM", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sp.edit();

                            editor.putInt("LOGIN_DATA", response.body().getIdUser());
                            editor.putInt("LOGIN_Privilege", response.body().getLevelPrivilege());
                            editor.putString("LOGIN_Token", response.body().getAccessToken());


                            editor.commit();
                            if(response.body().getAdminArea() != null){


                                Log.i("areea" , "wefewf");
                                TMSMPreferences.setAdminArea(getApplicationContext() , response.body().getAdminArea());

                              List<Integer> data =  TMSMPreferences.getAdminArea(getApplicationContext());

                               Log.i("datadata" , data.size() + " " + data.toString());


                            }


                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);

                        } else {
                            Toast.makeText(getApplicationContext(), response.body().getNameStatus() + " ewd", Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResLogin> call, Throwable t) {


                        TMSMPreferences.setURL(getApplicationContext());
                        Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

                    }
                });
            }
        });

    }


}




