package app.coe.com.tmsm;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.security.acl.Group;

import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.OffLineResRoot;
import app.coe.com.tmsm.models.OfflineResDevice;
import app.coe.com.tmsm.models.ProfileUser;
import app.coe.com.tmsm.models.ReqDoor;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.utility.Sync;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    boolean doorControl = false;

    private NavigationView navigationView;
private TextView txtFullname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        View headerView = navigationView.getHeaderView(0);
        txtFullname = (TextView) headerView.findViewById(R.id.txtFullname);
        txtFullname.setText("N/A");




        Sync sync = new Sync(MainActivity.this);
        sync.getData();


        int idUser = TMSMPreferences.getUserID(getApplicationContext());

        callAPIProfile(idUser);
        int privilege  = TMSMPreferences.getPrivilege(getApplicationContext());


        Menu nav_Menu = navigationView.getMenu();
        if(privilege == 4 || privilege == 3 || privilege == 5 || privilege == 6){


            nav_Menu.findItem(R.id.permit).setVisible(false);
            nav_Menu.findItem(R.id.permitUpdate).setVisible(false);
            nav_Menu.findItem(R.id.reqTimer).setVisible(false);
            nav_Menu.findItem(R.id.permitTimer).setVisible(false);
//            nav_Menu.findItem(R.id.user).setVisible(false);
            nav_Menu.findItem(R.id.reqDoor).setVisible(false);


        }else if(privilege == 2){

//            nav_Menu.findItem(R.id.user).setVisible(false);
        }

        callAPIPrivilageDoor(idUser);

        GroupFragment groupFragment = new GroupFragment();
        replaceFragmentManager(groupFragment);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (id == R.id.scanThings) {

            RootScanThingsFragment rootScanThingsFragment = new RootScanThingsFragment();

            replaceFragmentManager(rootScanThingsFragment);

        } else if (id == R.id.logout) {
            TMSMPreferences.removeUserID(getApplicationContext());
            TMSMPreferences.removePrivilege(getApplicationContext());
            TMSMPreferences.removeArea(getApplicationContext());
            TMSMPreferences.removeURL(getApplicationContext());
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);

        } else if (id == R.id.deviceManager) {
            DeviceManagerFragment deviceManagerFragment = new DeviceManagerFragment();
            replaceFragmentManager(deviceManagerFragment);

        } else if (id == R.id.device) {
            GroupFragment groupFragment = new GroupFragment();
            replaceFragmentManager(groupFragment);

//        } else if (id == R.id.map) {
//            MapThingOfUserFragment mapThingOfUserFragment = new MapThingOfUserFragment();
//            replaceFragmentManager(mapThingOfUserFragment);
        } else if (id == R.id.offline) {

            startActivity(new Intent(MainActivity.this, OfflineScanActivity.class));
        } else if (id == R.id.door) {

            if (doorControl) {

                DoorControlFragment doorControlFragment = new DoorControlFragment();
                replaceFragmentManager(doorControlFragment);


            } else {
                Toast.makeText(getApplicationContext(), "No Privilege", Toast.LENGTH_LONG).show();


            }
        }else if(id == R.id.permit){
            PermitReqFragment reqFragment = new PermitReqFragment();
            replaceFragmentManager(reqFragment);
        }else if(id == R.id.permitUpdate){

            PermitAllowFragment permitAllowFragment = new PermitAllowFragment();
            replaceFragmentManager(permitAllowFragment);


        }else if(id == R.id.reqTimer){
            ReqTimerFragment reqTimerFragment = new ReqTimerFragment();
            replaceFragmentManager(reqTimerFragment);

        }else if(id == R.id.permitTimer){
            PermitTimerFragment timerFragment = new PermitTimerFragment();
            replaceFragmentManager(timerFragment);
//        }else if(id == R.id.user){
//            ReqAdminFragment timerFragment = new ReqAdminFragment();
//            replaceFragmentManager(timerFragment);
        }else if(id == R.id.reqDoor){
            DoorFragment doorFragment = new DoorFragment();
            replaceFragmentManager(doorFragment);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void replaceFragmentManager(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.mainLayout, fragment);
        transaction.commit();
    }

    public void callAPIPrivilageDoor(int idUser) {

        String url = TMSMPreferences.getURL(getApplicationContext());
        Log.i("url con " , url);

        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();


        FeedData feedData = retrofit.create(FeedData.class);
        String token = TMSMPreferences.getToken(getApplicationContext());

        feedData.checkPrivilegeDoor(token , idUser).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {


                if(response.code() == 401){
                    TMSMPreferences.session(MainActivity.this);
                    return;

                }

                int status = response.body().getCodeStatus();

                if (status == 200) {
                    doorControl = true;

                } else {

                    Menu nav_Menu = navigationView.getMenu();
                    nav_Menu.findItem(R.id.door).setVisible(false);

                    Toast.makeText(getApplicationContext(), response.body().getNameStatus(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                TMSMPreferences.toLogin(MainActivity.this);
            }
        });


    }


    public void callAPIProfile(int idUser) {

        String url = TMSMPreferences.getURL(getApplicationContext());
        Log.i("url con " , url);

        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();


        FeedData feedData = retrofit.create(FeedData.class);
        String token = TMSMPreferences.getToken(getApplicationContext());
        feedData.getProfileUser(token , idUser).enqueue(new Callback<ProfileUser>() {
            @Override
            public void onResponse(Call<ProfileUser> call, Response<ProfileUser> response) {
                if(response.body().getCodeStatus() == 200){
                    txtFullname.setText(response.body().getProfile().getFirstName() + " " + response.body().getProfile().getLastName());
                }
            }

            @Override
            public void onFailure(Call<ProfileUser> call, Throwable throwable) {

                TMSMPreferences.toLogin(MainActivity.this);
            }
        });


    }



}
