package app.coe.com.tmsm;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import app.coe.com.tmsm.adapter.GroupDevice;
import app.coe.com.tmsm.adapter.MapThing;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.ReqCreateGroup;
import app.coe.com.tmsm.models.ResThingMap;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class MapThingOfUserFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    public MapThingOfUserFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_map_thing_of_user, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.mapThing);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext() , LinearLayout.VERTICAL));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        callAPI();
        return v;
    }

    public void callAPI(){

        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        final FeedData feedData = retrofit.create(FeedData.class);

        int uid = TMSMPreferences.getUserID(getContext());

        String token = TMSMPreferences.getToken(getContext());

        feedData.getThingsMap(token , uid).enqueue(new Callback<ResThingMap>() {
            @Override
            public void onResponse(Call<ResThingMap> call, Response<ResThingMap> response) {



                if(response.body().getCodeStatus() == 200){

                    mAdapter = new MapThing(response.body().getData(), getContext());
                    mRecyclerView.setAdapter(mAdapter);

                }else{
                    Toast.makeText(getContext() , response.body().getNameStatus() , Toast.LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<ResThingMap> call, Throwable t) {
                TMSMPreferences.toLogin(getContext());
                Toast.makeText(getContext() , t.getMessage() , Toast.LENGTH_LONG);
            }
        });


    }


}
