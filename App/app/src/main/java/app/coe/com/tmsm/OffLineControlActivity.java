package app.coe.com.tmsm;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import app.coe.com.tmsm.adapter.OffLine;
import app.coe.com.tmsm.adapter.OfflineDevice;
import app.coe.com.tmsm.models.RealmDevice;
import app.coe.com.tmsm.models.RealmThing;
import io.realm.Realm;
import io.realm.RealmResults;


public class OffLineControlActivity extends AppCompatActivity {

    private Realm realm;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_off_line_control);

        mRecyclerView = (RecyclerView) findViewById(R.id.offLine);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayout.VERTICAL));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));


        int idThing = getIntent().getIntExtra("idThing", 0);

        if (idThing != 0) {

            realm = Realm.getDefaultInstance();

            RealmResults<RealmDevice> resultDevice = realm.where(RealmDevice.class).equalTo("thingID"  , idThing).findAll();

            Log.i("resultDevice" , resultDevice.toString());

            List<RealmDevice> dataset = new ArrayList<>();

            for (RealmDevice realmDevice : resultDevice) {
                dataset.add(realmDevice);
            }

            if(dataset != null){


                mAdapter = new OfflineDevice(dataset, getApplicationContext());
                mRecyclerView.setAdapter(mAdapter);
            }else{
                Toast.makeText(getApplicationContext() , "No Device offline control" , Toast.LENGTH_SHORT).show();
            }


        } else {
            Toast.makeText(getApplicationContext(), "Error Select Device", Toast.LENGTH_LONG).show();
        }
    }





}
