package app.coe.com.tmsm;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import app.coe.com.tmsm.adapter.AllThing;
import app.coe.com.tmsm.adapter.OffLine;
import app.coe.com.tmsm.adapter.ScanThings;
import app.coe.com.tmsm.models.RealmThing;
import app.coe.com.tmsm.models.Thing;
import io.realm.Realm;
import io.realm.RealmResults;

public class OfflineScanActivity extends AppCompatActivity {


    private WifiManager wifi;
    private WifiScanReceiver wifiReceiver;
    private ProgressDialog dialog;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;


    private Realm realm;
    private RealmResults<RealmThing> realmThings;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_scan);


        realm = Realm.getDefaultInstance();

        dialog = ProgressDialog.show(OfflineScanActivity.this, "Scan Things",
                "Loading data from the server...", true);

        mRecyclerView = (RecyclerView) findViewById(R.id.offline_recy);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayout.VERTICAL));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));


        wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifiReceiver = new OfflineScanActivity.WifiScanReceiver();
        wifi.startScan();

    }


    @Override
    public void onPause() {
        getApplicationContext().unregisterReceiver(wifiReceiver);
        super.onPause();
    }

    @Override
    public void onResume() {
       getApplicationContext().registerReceiver(
                wifiReceiver,
                new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)
        );
        super.onResume();
    }


    private class WifiScanReceiver extends BroadcastReceiver {

        public void onReceive(Context c, Intent intent) {

            dialog.dismiss();


            List<ScanResult> wifiScanList = wifi.getScanResults();


            if (wifiScanList != null) {

//                Toast.makeText(getApplicationContext()  , "Scan Success" , Toast.LENGTH_LONG).show();


                realmThings = realm.where(RealmThing.class).findAll();
//                Log.i("realmThings" ,realmThings.toString());

                List<RealmThing> dataset = new ArrayList<RealmThing>();

                for (int i = 0; i < wifiScanList.size(); i++) {

                    app.coe.com.tmsm.models.ScanThings data = new app.coe.com.tmsm.models.ScanThings(wifiScanList.get(i).SSID, wifiScanList.get(i).BSSID);


//                    Log.i("WIFI " , data.getNameThings() + " : " + data.getMacAddress());

                    for (RealmThing realmThing : realmThings) {

                        if (realmThing.getThingMAC() != null) {
                            if (realmThing.getThingMAC().toUpperCase().equals(data.getMacAddress().toUpperCase())) {
//                                Log.i("TEST WIFI" , "dataset");
                                dataset.add(realmThing);
                            }
                        }

                    }

                }



                if(dataset.size() >= 1){

                    mAdapter = new OffLine(dataset, getApplicationContext());
                    mRecyclerView.setAdapter(mAdapter);
                }else{

                    Toast.makeText(getApplicationContext(), "No Thing Offline Mode" , Toast.LENGTH_LONG).show();
                }

            }else{
                Toast.makeText(getApplicationContext()  , "No Thing Offline Mode" , Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
