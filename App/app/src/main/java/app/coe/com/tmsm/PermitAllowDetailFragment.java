package app.coe.com.tmsm;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.ReqPermitPrivilage;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class PermitAllowDetailFragment extends Fragment {



    private TextView dateStart;
    private  TextView dateEnd;

    private SwitchDateTimeDialogFragment dateTimeFragmentStart;

    private SwitchDateTimeDialogFragment dateTimeFragmentEnd;
    private static final String TAG_DATETIME_FRAGMENT_STRAT = "TAG_DATETIME_FRAGMENT_START";
    private static final String TAG_DATETIME_FRAGMENT_END = "TAG_DATETIME_FRAGMENT_END";


    public PermitAllowDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_permit_allow_detail, container, false);

        RadioGroup radioGroupAppove = (RadioGroup) v.findViewById(R.id.approve);
        RadioButton radioButtonYes = (RadioButton) v.findViewById(R.id.yes);
        RadioButton radioButtonNo = (RadioButton) v.findViewById(R.id.no);

        RadioGroup radioGroupGrant = (RadioGroup) v.findViewById(R.id.grant);
        RadioButton radioButtonPermanent = (RadioButton) v.findViewById(R.id.permanent);
        RadioButton radioButtonTemporary = (RadioButton) v.findViewById(R.id.temporary);


        CheckBox chR = (CheckBox)v.findViewById(R.id.chkRU);
        CheckBox chW = (CheckBox)v.findViewById(R.id.chkWU);
        CheckBox chE = (CheckBox)v.findViewById(R.id.chkEU);

        dateStart = (TextView) v.findViewById(R.id.txtDateStart);
        dateEnd = (TextView) v.findViewById(R.id.txtDateEnd);


        RadioGroup radioGroupEnable= (RadioGroup) v.findViewById(R.id.enable);
        RadioButton radioButtonEnabled = (RadioButton) v.findViewById(R.id.enabled);
        RadioButton radioButtonDisabled = (RadioButton) v.findViewById(R.id.disabled);


        Button btnUpdate = (Button) v.findViewById(R.id.btnPermit);



        radioButtonEnabled.setChecked(true);

        String startDate = getArguments().getString("startDate");
        String endDate = getArguments().getString("endDate");

        int permitID = getArguments().getInt("permitID");
        int grant = getArguments().getInt("grant");

        String privileges = getArguments().getString("privilege");
        int approve = getArguments().getInt("approve");

        int type = getArguments().getInt("type");


        Log.i("privileges" , privileges);

        char privilege[] = privileges.toCharArray();

        Log.i("privileges" , privilege.toString());

        if(privilege[0] == '1'){
            chR.setChecked(true);
        }

        if(privilege[1] == '1'){
            chW.setChecked(true);
        }

        if(privilege[2] == '1'){
            chE.setChecked(true);
        }


        dateStart.setText(startDate);
        dateEnd.setText(endDate);

        if(grant == 1){

            radioButtonPermanent.setChecked(true);
        }else{
            radioButtonTemporary.setChecked(true);
        }

        if(approve == 0){

            radioButtonNo.setChecked(true);
        }else{
            radioButtonYes.setChecked(true);
        }



        datetimeStart();
        datetimeEnd();



        if(type == 4){
            radioButtonPermanent.setEnabled(false);
            radioButtonTemporary.setEnabled(false);

            chE.setEnabled(false);
            chR.setEnabled(false);
            chR.setEnabled(false);

            radioButtonYes.setEnabled(false);
            radioButtonNo.setEnabled(false);
        }

        dateStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateTimeFragmentStart.startAtCalendarView();
                dateTimeFragmentStart.setDefaultDateTime(new Date());
                dateTimeFragmentStart.setDefaultMinute(0);

                dateTimeFragmentStart.show(getFragmentManager(), TAG_DATETIME_FRAGMENT_STRAT);
            }
        });


        dateEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateTimeFragmentEnd.startAtCalendarView();

                dateTimeFragmentEnd.setDefaultMinute(0);
                dateTimeFragmentEnd.setDefaultDateTime(new Date());
                dateTimeFragmentEnd.show(getFragmentManager(), TAG_DATETIME_FRAGMENT_END);

            }
        });


        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int status = 0;

                if(radioButtonYes.isChecked()){
                    status = 1;
                }

                if(radioButtonNo.isChecked()){
                    status = 0;
                }


                int grant = 1;

                if(radioButtonPermanent.isChecked()){
                    grant = 1;
                }
                if(radioButtonTemporary.isChecked()){
                    grant = 2;
                }



                String privilege = "";

                if(chR.isChecked()) privilege+="1";
                else privilege +="0";


                if(chW.isChecked()) privilege+="1";
                else privilege +="0";


                if(chE.isChecked()) privilege+="1";
                else privilege +="0";


                String startDate = dateStart.getText().toString();
                String endDate = dateEnd.getText().toString();

                boolean enable = true;

                if(radioButtonEnabled.isChecked()){
                    enable = true;
                }

                if(radioButtonDisabled.isChecked()){
                    enable = false;
                }



                int device = getArguments().getInt("device");
                int user = getArguments().getInt("user");



                ReqPermitPrivilage reqPermitPrivilage = new ReqPermitPrivilage();
                reqPermitPrivilage.setPermitGrantStatus(grant);
                reqPermitPrivilage.setPermitID(permitID);
                reqPermitPrivilage.setPermitStatus(1);
                reqPermitPrivilage.setPrivilege(privilege);
                reqPermitPrivilage.setEnable(true);
                reqPermitPrivilage.setPermitTimeMax(startDate);
                reqPermitPrivilage.setPermitTimeMin(endDate);
                reqPermitPrivilage.setPermitStatus(status);
                reqPermitPrivilage.setEnable(enable);
                reqPermitPrivilage.setUser(user);
                reqPermitPrivilage.setDevice(device);

                callAPI(reqPermitPrivilage);



            }
        });



        return v;
    }


    private void datetimeStart(){

        dateTimeFragmentStart = (SwitchDateTimeDialogFragment) getFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT_STRAT);
        if(dateTimeFragmentStart == null) {
            dateTimeFragmentStart = SwitchDateTimeDialogFragment.newInstance(
                    getString(R.string.label_datetime_dialog),
                    getString(android.R.string.ok),
                    getString(android.R.string.cancel)
            );
        }

        final SimpleDateFormat myDateFormat = new SimpleDateFormat("yyyy:dd:MM HH:mm", java.util.Locale.getDefault());

        dateTimeFragmentStart.set24HoursMode(true);
        dateTimeFragmentStart.setHighlightAMPMSelection(false);

        try {
            dateTimeFragmentStart.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
            Log.e("Datetime ", e.getMessage());
        }


        dateTimeFragmentStart.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
            @Override
            public void onPositiveButtonClick(Date date) {

                dateStart.setText(myDateFormat.format(date));
            }

            @Override
            public void onNegativeButtonClick(Date date) {

            }


            @Override
            public void onNeutralButtonClick(Date date) {



                // Optional if neutral button does'nt exists
                dateStart.setText("");
            }
        });
    }


    private void datetimeEnd(){

        dateTimeFragmentEnd = (SwitchDateTimeDialogFragment) getFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT_END);
        if(dateTimeFragmentEnd == null) {
            dateTimeFragmentEnd = SwitchDateTimeDialogFragment.newInstance(
                    getString(R.string.label_datetime_dialog),
                    getString(android.R.string.ok),
                    getString(android.R.string.cancel)
            );
        }

        final SimpleDateFormat myDateFormat = new SimpleDateFormat("yyyy:dd:MM HH:mm", java.util.Locale.getDefault());

        dateTimeFragmentEnd.set24HoursMode(true);
        dateTimeFragmentEnd.setHighlightAMPMSelection(false);

        try {
            dateTimeFragmentEnd.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
            Log.e("Datetime ", e.getMessage());
        }

        dateTimeFragmentEnd.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
            @Override
            public void onPositiveButtonClick(Date date) {

                dateEnd.setText(myDateFormat.format(date));




            }

            @Override
            public void onNegativeButtonClick(Date date) {

            }


            @Override
            public void onNeutralButtonClick(Date date) {

                Log.i("myDateFormat" , myDateFormat.format(date)+"");

                // Optional if neutral button does'nt exists
                dateEnd.setText("");
            }
        });
    }




    public void callAPI(ReqPermitPrivilage body){

        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);
        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();


        FeedData feedData = retrofit.create(FeedData.class);


        String token = TMSMPreferences.getToken(getContext());

        feedData.permit(token , body).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {

                Toast.makeText(getContext() , response.body().getNameStatus() , Toast.LENGTH_LONG).show();

                getFragmentManager().popBackStack();
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {

                Toast.makeText(getContext() , t.getMessage() , Toast.LENGTH_LONG).show();
                getFragmentManager().popBackStack();
                TMSMPreferences.toLogin(getContext());
            }
        });
    }

}
