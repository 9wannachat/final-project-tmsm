package app.coe.com.tmsm;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import app.coe.com.tmsm.adapter.PermitAllow;
import app.coe.com.tmsm.adapter.PermitReq;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.PermitResRoot;
import app.coe.com.tmsm.models.RootPermitRequest;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class PermitAllowFragment extends Fragment {


    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    public PermitAllowFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_permit_allow, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.permitAllow);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext() , LinearLayout.VERTICAL));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));



        int privilege = TMSMPreferences.getPrivilege(getContext());


        Log.i("privilege" , "wefwefwefwefwefwef");
        if(privilege == 1){
            callAPIAdmin();
        }else if(privilege == 2){

            int user = TMSMPreferences.getUserID(getContext());
            callAPIAdminArea(user);
        }

        return v;
    }


    public void callAPIAdmin(){
        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);

        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        FeedData feedData = retrofit.create(FeedData.class);

        String token = TMSMPreferences.getToken(getContext());
      feedData.getAllowPermit(token).enqueue(new Callback<PermitResRoot>() {
          @Override
          public void onResponse(Call<PermitResRoot> call, Response<PermitResRoot> response) {

              if(response.body().getCodeStatus() == 200 && response.body().getData() != null){
                  mAdapter = new PermitAllow(response.body().getData(), getContext());
                  mRecyclerView.setAdapter(mAdapter);
              }

          }

          @Override
          public void onFailure(Call<PermitResRoot> call, Throwable t) {
              TMSMPreferences.toLogin(getContext());
          }
      });


    }

    public void callAPIAdminArea(int user){


        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);

        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        FeedData feedData = retrofit.create(FeedData.class);
        String token = TMSMPreferences.getToken(getContext());

        feedData.getAllowPermitArea(token , user).enqueue(new Callback<PermitResRoot>() {
            @Override
            public void onResponse(Call<PermitResRoot> call, Response<PermitResRoot> response) {

                if(response.body().getCodeStatus() == 200 && response.body().getData() != null){
                                    mAdapter = new PermitAllow(response.body().getData(), getContext());
                mRecyclerView.setAdapter(mAdapter);

                }

            }

            @Override
            public void onFailure(Call<PermitResRoot> call, Throwable t) {
                TMSMPreferences.toLogin(getContext());
            }
        });


    }


}
