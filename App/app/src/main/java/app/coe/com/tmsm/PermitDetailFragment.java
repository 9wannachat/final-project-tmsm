package app.coe.com.tmsm;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import app.coe.com.tmsm.adapter.IODevice;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.ReqPermitPrivilage;
import app.coe.com.tmsm.models.RootIODeviceOfUser;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class PermitDetailFragment extends Fragment {


    private TextView txtDateStart;
    private TextView txtDateEnd;
    private SwitchDateTimeDialogFragment dateTimeFragmentStart;

    private SwitchDateTimeDialogFragment dateTimeFragmentEnd;
    private static final String TAG_DATETIME_FRAGMENT_STRAT = "TAG_DATETIME_FRAGMENT_START";
    private static final String TAG_DATETIME_FRAGMENT_END = "TAG_DATETIME_FRAGMENT_END";

    public PermitDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_permit_detail, container, false);

        txtDateStart = (TextView) v.findViewById(R.id.txtDateStart);
        txtDateEnd = (TextView) v.findViewById(R.id.txtDateEnd);

        RadioGroup radioGroup = (RadioGroup)v.findViewById(R.id.grant);
        RadioButton radioButtonPermanent =  (RadioButton) v.findViewById(R.id.permanent);
        RadioButton radioButtontemporary =  (RadioButton) v.findViewById(R.id.temporary);

        CheckBox checkBoxR = (CheckBox)v.findViewById(R.id.chkRU);
        CheckBox checkBoxW = (CheckBox)v.findViewById(R.id.chkWU);
        CheckBox checkBoxE = (CheckBox)v.findViewById(R.id.chkEU);

        Button btnPermit = (Button) v.findViewById(R.id.btnPermit);



        String startDate = getArguments().getString("startDate");
        String endDate = getArguments().getString("endDate");
        String privileges = getArguments().getString("privilege");

        int type = getArguments().getInt("type");

        datetimeStart();
        datetimeEnd();


            txtDateStart.setText(startDate);
            txtDateEnd.setText(endDate);
      





        char privilege[] = privileges.toCharArray();


        if(type == 4){

            checkBoxR.setEnabled(false);
            checkBoxW.setEnabled(false);
            checkBoxE.setEnabled(false);

            radioButtonPermanent.setChecked(true);

            radioButtonPermanent.setEnabled(false);
            radioButtontemporary.setEnabled(false);
        }


        if(privilege[0] == '1'){
            checkBoxR.setChecked(true);
        }

        if(privilege[1] == '1'){
            checkBoxW.setChecked(true);
        }

        if(privilege[2] == '1'){
            checkBoxE.setChecked(true);
        }


        txtDateStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(type != 4){
                    dateTimeFragmentStart.startAtCalendarView();
                    dateTimeFragmentStart.setDefaultDateTime(new Date());
                    dateTimeFragmentStart.setDefaultMinute(0);

                    dateTimeFragmentStart.show(getFragmentManager(), TAG_DATETIME_FRAGMENT_STRAT);
                }

            }
        });


        txtDateEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(type != 4){
                    dateTimeFragmentEnd.startAtCalendarView();

                    dateTimeFragmentEnd.setDefaultMinute(0);
                    dateTimeFragmentEnd.setDefaultDateTime(new Date());
                    dateTimeFragmentEnd.show(getFragmentManager(), TAG_DATETIME_FRAGMENT_END);
                }


            }
        });


        int permitID = 0;
        int grant;

            permitID = getArguments().getInt("permitID");
            grant= getArguments().getInt("grant");


            if(type == 4){
                grant = 1;
            }
            if(grant == 1){

                radioButtonPermanent.setChecked(true);
            }else if(grant == 2){

                radioButtontemporary.setChecked(true);



        }


        int finalPermitID = permitID;
        btnPermit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int selectedId = radioGroup.getCheckedRadioButtonId();



                int grant = 2;

                if(selectedId == R.id.permanent){

                    grant = 1;
                }else if(selectedId == R.id.temporary){

                    grant = 2;

                }


                String privilege = "";

                if(checkBoxR.isChecked()) privilege+="1";
                else privilege +="0";


                if(checkBoxW.isChecked()) privilege+="1";
                else privilege +="0";


                if(checkBoxE.isChecked()) privilege+="1";
                else privilege +="0";



                int device = getArguments().getInt("device");
                int user = getArguments().getInt("user");

                ReqPermitPrivilage reqPermitPrivilage = new ReqPermitPrivilage();
                reqPermitPrivilage.setPermitGrantStatus(grant);
                reqPermitPrivilage.setPermitID(finalPermitID);
                reqPermitPrivilage.setPermitStatus(1);
                reqPermitPrivilage.setPrivilege(privilege);
                reqPermitPrivilage.setEnable(true);
                reqPermitPrivilage.setPermitTimeMax(txtDateEnd.getText().toString());
                reqPermitPrivilage.setPermitTimeMin(txtDateStart.getText().toString());
                reqPermitPrivilage.setDevice(device);
                reqPermitPrivilage.setUser(user);

                callAPI(reqPermitPrivilage);


            }
        });


        return v;
    }


    private void datetimeStart(){

        dateTimeFragmentStart = (SwitchDateTimeDialogFragment) getFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT_STRAT);
        if(dateTimeFragmentStart == null) {
            dateTimeFragmentStart = SwitchDateTimeDialogFragment.newInstance(
                    getString(R.string.label_datetime_dialog),
                    getString(android.R.string.ok),
                    getString(android.R.string.cancel)
            );
        }

        final SimpleDateFormat myDateFormat = new SimpleDateFormat("yyyy:dd:MM HH:mm", java.util.Locale.getDefault());

        dateTimeFragmentStart.set24HoursMode(true);
        dateTimeFragmentStart.setHighlightAMPMSelection(false);

        try {
            dateTimeFragmentStart.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
            Log.e("Datetime ", e.getMessage());
        }


        dateTimeFragmentStart.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
            @Override
            public void onPositiveButtonClick(Date date) {

                txtDateStart.setText(myDateFormat.format(date));
            }

            @Override
            public void onNegativeButtonClick(Date date) {

            }


            @Override
            public void onNeutralButtonClick(Date date) {



                // Optional if neutral button does'nt exists
                txtDateStart.setText("");
            }
        });
    }


    private void datetimeEnd(){

        dateTimeFragmentEnd = (SwitchDateTimeDialogFragment) getFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT_END);
        if(dateTimeFragmentEnd == null) {
            dateTimeFragmentEnd = SwitchDateTimeDialogFragment.newInstance(
                    getString(R.string.label_datetime_dialog),
                    getString(android.R.string.ok),
                    getString(android.R.string.cancel)
            );
        }

        final SimpleDateFormat myDateFormat = new SimpleDateFormat("yyyy:dd:MM HH:mm", java.util.Locale.getDefault());

        dateTimeFragmentEnd.set24HoursMode(true);
        dateTimeFragmentEnd.setHighlightAMPMSelection(false);

        try {
            dateTimeFragmentEnd.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
            Log.e("Datetime ", e.getMessage());
        }

        dateTimeFragmentEnd.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
            @Override
            public void onPositiveButtonClick(Date date) {

                txtDateEnd.setText(myDateFormat.format(date));




            }

            @Override
            public void onNegativeButtonClick(Date date) {

            }


            @Override
            public void onNeutralButtonClick(Date date) {

                Log.i("myDateFormat" , myDateFormat.format(date)+"");

                // Optional if neutral button does'nt exists
                txtDateEnd.setText("");
            }
        });
    }



    public void callAPI(ReqPermitPrivilage body){

        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);

        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        Log.i("callAPI" , "callAPI");
        FeedData feedData = retrofit.create(FeedData.class);


        String token = TMSMPreferences.getToken(getContext());

        feedData.permit(token , body).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {

                Toast.makeText(getContext() , response.body().getNameStatus() , Toast.LENGTH_LONG).show();

                getFragmentManager().popBackStack();
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {

                Toast.makeText(getContext() , t.getMessage() , Toast.LENGTH_LONG).show();
                getFragmentManager().popBackStack();
                TMSMPreferences.toLogin(getContext());
            }
        });
    }


}
