package app.coe.com.tmsm;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import app.coe.com.tmsm.adapter.GroupDevice;
import app.coe.com.tmsm.adapter.PermitReq;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.RootGruopDevice;
import app.coe.com.tmsm.models.RootPermitRequest;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class PermitReqFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    public PermitReqFragment() {
        // Required empty public constructor
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_permit_req, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.permitReq);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext() , LinearLayout.VERTICAL));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));



        int privilege = TMSMPreferences.getPrivilege(getContext());

        Log.i("privilege" , privilege+"");

        if(privilege == 1){
            callAPIAdmin();
        }else if(privilege ==2){

            int user = TMSMPreferences.getUserID(getContext());

            callAPIAdminArea(user);
//            getPermitReqWithUser
        }

        return v;
    }



    public void callAPIAdmin(){

        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);
        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        FeedData feedData = retrofit.create(FeedData.class);

        String token = TMSMPreferences.getToken(getContext());
        feedData.getPermitReq(token).enqueue(new Callback<RootPermitRequest>() {
            @Override
            public void onResponse(Call<RootPermitRequest> call, Response<RootPermitRequest> response) {

                if(response.body().getData() != null){
                    mAdapter = new PermitReq(response.body().getData(), getContext());

                    mRecyclerView.setAdapter(mAdapter);
                }

            }

            @Override
            public void onFailure(Call<RootPermitRequest> call, Throwable t) {
                TMSMPreferences.toLogin(getContext());
                Toast.makeText(getContext() , t.getMessage() , Toast.LENGTH_LONG).show();
            }
        });



    }


    public void callAPIAdminArea(int idUser){


        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);
        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        FeedData feedData = retrofit.create(FeedData.class);
        String token = TMSMPreferences.getToken(getContext());

        feedData.getPermitReqWithUser(token , idUser).enqueue(new Callback<RootPermitRequest>() {
            @Override
            public void onResponse(Call<RootPermitRequest> call, Response<RootPermitRequest> response) {

                if(response.body().getData() != null){
                    mAdapter = new PermitReq(response.body().getData(), getContext());

                    mRecyclerView.setAdapter(mAdapter);
                }


            }

            @Override
            public void onFailure(Call<RootPermitRequest> call, Throwable t) {
                TMSMPreferences.toLogin(getContext());
                Toast.makeText(getContext() , t.getMessage() , Toast.LENGTH_LONG).show();
            }
        });



    }


}
