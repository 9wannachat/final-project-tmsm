package app.coe.com.tmsm;


import android.content.Context;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;


public class PinchZoomPan extends View {


    private Bitmap mBitmap;
    private int mImageWidth;
    private int mImageHeight;

    private float mPositionX;
    private float mPositionY;
    private float mLastTouchX;
    private float mLastTouchY;

    private Context context;

    private static final int INVALID_POINTER_ID = -1;
    private int mActivePointerID = INVALID_POINTER_ID;

    private ScaleGestureDetector mScaleDetector;
    private float mScaleFactor = 1.0f;
    private final static float mMinZoom = 1f;
    private final static float mMaxZoom = 5.0f;

    private int height;
    private int width;
    private int toolbar;
    private Bitmap bitmap;


    private double ratioW;
    private double ratioH;

    private double canvasW;
    private double canvasH;




    final private String redColor = "#ff001a";
    final private String greenColor = "#62ff5f";
    final private String orangeColor = "#f79036";


    private String uidLocal;
    private String uid;

    private double price;
    private String place;
    private String zone;

    private boolean zoneClick = false;
    public PinchZoomPan(Context context, int height, int width, int toolbar, Bitmap bitmap) {
        super(context);

        this.height = height;
        this.width = width;
        this.toolbar = toolbar;
        this.bitmap = bitmap;

        this.context = context;
        this.mImageWidth = bitmap.getWidth();
        this.mImageHeight = bitmap.getHeight();



        this.bitmap = calImageRatio(bitmap);




        Log.i("calImageRatio bitmap H", this.bitmap.getHeight() + "");
        Log.i("calImageRatio bitmap W", this.bitmap.getWidth() + "");


        mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        //the scale gesture detector should inspect all the touch events

        mScaleDetector.onTouchEvent(event);

        if(event.getAction() == MotionEvent.ACTION_UP) {
//            mScaleDetector.onTouchEvent(event);

//            zoneClick = false;
//            for (int i = 0; i < data.size(); i++) {
//
//                for (Map.Entry<String, Lot> lot : data.get(i).entrySet()) {
//
//                    double x = (lot.getValue().getAccessX1() / (this.mImageWidth / this.canvasW));
//                    double y = (lot.getValue().getAccessY1() / (this.mImageHeight / this.canvasH));
//
//                    double w = (lot.getValue().getChw() / (this.mImageWidth / this.canvasW));
//                    double h = (lot.getValue().getChh() / (this.mImageHeight / this.canvasH));
//
//
//                    double left = x;
//                    double right = x + w;
//
//                    double top = y;
//                    double bottom = y + h;
//
//
//                    if (mScaleFactor == 1 && mPositionX == 0 && mPositionY == 0) {
//                        left = left + 0;
//                        right = right + 0;
//                        top = top + 0;
//                        bottom = bottom + 0;
//                    } else if (mScaleFactor == 1 && mPositionX != 0 && mPositionY != 0) {
//                        left = left + mPositionX;
//                        right = right + mPositionX;
//                        top = top + mPositionY;
//                        bottom = bottom + mPositionY;
//                    } else {
//
//                        left = (left * mScaleFactor) + mPositionX;
//                        right = (right * mScaleFactor) + mPositionX;
//                        top = (top * mScaleFactor) + mPositionY;
//                        bottom = (bottom * mScaleFactor) + mPositionY;
//                    }
//
//
//                    if (right >= event.getX() && left <= event.getX() && bottom >= event.getY() && top <= event.getY()) {
//
//                        Log.i("Click ", lot.getKey());
//
//                        Log.i("LIST CLCIK ", this.data.size() + "");
//
//                        for (Map<String, Lot> mapLot : this.data) {
//
//                            if (mapLot.containsKey(lot.getKey())) {
//                                Lot lotDetect = mapLot.get(lot.getKey());
//
//                                if (lotDetect.getParkStatus() == 1 || lotDetect.getReserveStatus() == 1) {
//
//
//                                    if (lotDetect.getUid() != null && lotDetect.getUid().equals(this.uidLocal)) {
//
//
//                                        Log.i("UID ", lotDetect.getUid());
//                                        Log.i("UID Local ", uidLocal);
//                                        Intent intent = new Intent(this.context, AgreeToBookActivity.class);
//                                        intent.putExtra("place", place);
//                                        intent.putExtra("zone", zone);
//                                        intent.putExtra("price", price);
//                                        intent.putExtra("lot", lot.getKey());
//                                        intent.putExtra("status", true);
//                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                        this.context.startActivity(intent);
//                                        zoneClick = true;
//                                        break;
//
//                                    } else {
//
//
////                                        createAlert("ช่องจอดรถไม่สามารถจองได้ เนื่องจากช่องจอดรถมีคนจองแล้ว" );
//                                        Toast.makeText(context , "ช่องจอดรถไม่สามารถจองได้ เนื่องจากช่องจอดรถมีคนจองแล้ว" , Toast.LENGTH_LONG).show();
//                                        Log.i("Status ", "ช่องจอดรถไม่สามารถจองได้ เนื่องจากช่องจอดรถมีคนจองแล้ว");
//                                    }
//
//                                } else {
//                                    if (lotDetect.getReserveAble() != 1) {
//
//
////                                        createAlert("ช่องจอดรถไม่สามารถจองได้ คุณสามารถจองได้เฉพาะช่องจอดที่มีสัญลักษณ์ R" );
//                                        Toast.makeText(context , "ช่องจอดรถไม่สามารถจองได้ คุณสามารถจองได้เฉพาะช่องจอดที่มีสัญลักษณ์ R" , Toast.LENGTH_LONG).show();
//
//
//                                        Log.i("STATUS ", "ช่องจอดรถไม่สามารถจองได้ คุณสามารถจองได้เฉพาะช่องจอดที่มีสัญลักษณ์ R");
//                                    } else {
//                                        Log.i("Status ", "จองได้");
//                                        Intent intent = new Intent(this.context, AgreeToBookActivity.class);
//                                        intent.putExtra("place", place);
//                                        intent.putExtra("zone", zone);
//                                        intent.putExtra("price", price);
//                                        intent.putExtra("lot", lot.getKey());
//                                        intent.putExtra("status", false);
//                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                        this.context.startActivity(intent);
//                                        zoneClick = true;
//                                        break;
//
//                                    }
//
//
//                                }
//
//                            }
//
//                            if (zoneClick) {
//
//                                break;
//                            }
//
//                        }
//
//
//                        if (zoneClick) {
//
//                            break;
//                        }
//
//                    } else {
//
////                    Log.i("Click " , "No Area");
//                    }
//
//                    if (zoneClick) {
//
//                        break;
//                    }
//
//                }
//
//                if (zoneClick) {
//
//                    break;
//                }
//
//
//            }

        }

        final int action = event.getAction();

        switch (action & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN: {

                //get x and y cords of where we touch the screen
                final float x = event.getX();
                final float y = event.getY();

                //remember where touch event started
                mLastTouchX = x;
                mLastTouchY = y;

                //save the ID of this pointer
                mActivePointerID = event.getPointerId(0);

                break;
            }
            case MotionEvent.ACTION_MOVE: {

                //find the index of the active pointer and fetch its position
                final int pointerIndex = event.findPointerIndex(mActivePointerID);
                final float x = event.getX(pointerIndex);
                final float y = event.getY(pointerIndex);

                if (!mScaleDetector.isInProgress()) {

                    //calculate the distance in x and y directions
                    final float distanceX = x - mLastTouchX;
                    final float distanceY = y - mLastTouchY;

                    mPositionX += distanceX;
                    mPositionY += distanceY;

                    //redraw canvas call onDraw method
                    invalidate();

                }
                //remember this touch position for next move event
                mLastTouchX = x;
                mLastTouchY = y;


                break;
            }

            case MotionEvent.ACTION_UP: {
                mActivePointerID = INVALID_POINTER_ID;
                break;
            }

            case MotionEvent.ACTION_CANCEL: {
                mActivePointerID = INVALID_POINTER_ID;
                break;

            }

            case MotionEvent.ACTION_POINTER_UP: {
                //Extract the index of the pointer that left the screen
                final int pointerIndex = (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
                final int pointerId = event.getPointerId(pointerIndex);
                if (pointerId == mActivePointerID) {
                    //Our active pointer is going up Choose another active pointer and adjust
                    final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
                    mLastTouchX = event.getX(newPointerIndex);
                    mLastTouchY = event.getY(newPointerIndex);
                    mActivePointerID = event.getPointerId(newPointerIndex);
                }
                break;
            }
        }


        return true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        canvas.save();
        canvas.translate(mPositionX, mPositionY);
        canvas.scale(mScaleFactor, mScaleFactor);



        Paint paint = new Paint();
        paint.setColor(Color.BLUE);

        paint.setStyle(Paint.Style.FILL);

        canvas.drawBitmap(bitmap, new Matrix(), new Paint());

        canvas.save();

//        for (int i = 0; i < data.size(); i++) {
//
//            for (Map.Entry<String, Lot> lot : data.get(i).entrySet()) {
//
//
//                Log.i("PosX1 ", lot.getValue().getPosX1() + "");
//                Log.i("PosY1 ", lot.getValue().getPosY1() + "");
//
//
//                double x = lot.getValue().getPosX1();
//                double y = lot.getValue().getPosY1();
//
//                double w = lot.getValue().getChw();
//                double h = lot.getValue().getChh();
//
//                int type = lot.getValue().getReserveAble();
//                double angle = lot.getValue().getAngle();
//
//                String uid = lot.getValue().getUid();
//                int parkStatus = lot.getValue().getParkStatus();
//                int reserveStatus = lot.getValue().getReserveStatus();
//
//
//                drawRecMap(canvas, x, y, w, h, angle, type, parkStatus, reserveStatus, uid);
//            }
//
//        }


        canvas.restore();

    }


    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {

            mScaleFactor *= scaleGestureDetector.getScaleFactor();
            //don't to let the image get too large or small
            mScaleFactor = Math.max(mMinZoom, Math.min(mScaleFactor, mMaxZoom));


            Log.i("scaleGestureDetector", mScaleFactor + " ");

            Log.i("mPositionX", mPositionX + " ");
            Log.i("mPositionY", mPositionY + " ");

            invalidate();


            return true;
        }
    }

    private Bitmap calImageRatio(Bitmap bitmapResize) {


        double imageH = bitmapResize.getHeight();
        double imageW = bitmapResize.getWidth();


        double screenW = width;
        double screenH = (height - toolbar) - (1 * toolbar);


        double n1 = getMax(getMax(imageW, imageH), getMax(screenW, screenH));
        double n2 = getMin(getMax(imageW, imageH), getMax(screenW, screenH));

        double ratioW = (n1) / (n2);


        double n3 = getMax(getMin(imageW, imageH), getMin(screenW, screenH));
        double n4 = getMin(getMin(imageW, imageH), getMin(screenW, screenH));


        double ratioH = (n3) / (n4);


        double imageScale = getMax(ratioW, ratioH);


        this.canvasW = imageW / imageScale;
        this.canvasH = imageH / imageScale;


        this.ratioW = ratioW;
        this.ratioH = ratioH;

        return Bitmap.createScaledBitmap(bitmap, (int) canvasW, (int) canvasH, true);

    }

    private double getMax(double n1, double n2) {
        if (n1 >= n2) {
            return n1;
        } else {
            return n2;
        }

    }

    private double getMin(double n1, double n2) {
        if (n1 <= n2) {
            return n1;
        } else {
            return n2;
        }

    }

    public double getImageH() {

        return canvasH;
    }

    public double getImageW() {
        return canvasW;
    }


    public void drawRecMap(Canvas canvas, double rx, double ry, double rw, double rh, double angle, int type, int parkStatus, int reserveStatus, String uid) {

        canvas.save();

        double rawX = rx;
        double rawY = ry;


        double rawW = rw;
        double rawH = rh;

        double x = (rawX / (this.mImageWidth / this.canvasW));
        double y = (rawY / (this.mImageHeight / this.canvasH));


        double w = (rawW / (this.mImageWidth / this.canvasW));
        double h = (rawH / (this.mImageHeight / this.canvasH));


        Rect areaRect = new Rect(0, 0, (int) w, (int) h);
        Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

        canvas.translate((float) x, (float) y);
        canvas.rotate((float) angle);

        if (parkStatus == 1 || reserveStatus == 1) {

            if(uidLocal != null && uid != null ){

                if (uidLocal.trim().equals(uid.trim())) {

                    Log.i("uid " , uid + "  " + uidLocal);
                    mPaint.setColor(Color.parseColor(orangeColor));
                    canvas.drawRect(areaRect, mPaint);
                } else {
                    mPaint.setColor(Color.parseColor(redColor));
                    canvas.drawRect(areaRect, mPaint);
                }
            }else{
                mPaint.setColor(Color.parseColor(redColor));
                canvas.drawRect(areaRect, mPaint);
            }


        }else if(parkStatus == 0 || reserveStatus == 0){

            mPaint.setColor(Color.parseColor(greenColor));
            canvas.drawRect(areaRect, mPaint);
        }

        if (type == 1) {
            String pageTitle = "R";

            mPaint.setTextSize(40);
            RectF bounds = new RectF(areaRect);

            bounds.right = mPaint.measureText(pageTitle, 0, pageTitle.length());

            bounds.bottom = mPaint.descent() - mPaint.ascent();

            bounds.left += (areaRect.width() - bounds.right) / 2.0f;
            bounds.top += (areaRect.height() - bounds.bottom) / 2.0f;

            mPaint.setColor(Color.WHITE);
            canvas.drawText(pageTitle, bounds.left, bounds.top - mPaint.ascent(), mPaint);
        }

        canvas.restore();



    }




}
