package app.coe.com.tmsm;

import android.content.Context;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;

import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;


public class PinchZoomPanDevice extends View {


    private Bitmap mBitmap;
    private int mImageWidth;
    private int mImageHeight;

    private float mPositionX;
    private float mPositionY;
    private float mLastTouchX;
    private float mLastTouchY;

    private Context context;

    private static final int INVALID_POINTER_ID = -1;
    private int mActivePointerID = INVALID_POINTER_ID;

    private ScaleGestureDetector mScaleDetector;
    private float mScaleFactor = 1.0f;
    private final static float mMinZoom = 1f;
    private final static float mMaxZoom = 5.0f;

    private int height;
    private int width;
    private int toolbar;
    private Bitmap bitmap;


    private double ratioW;
    private double ratioH;

    private double canvasW;
    private double canvasH;


    private double offsetX;
    private double offsetY;

    private boolean zoneClick = false;
    public PinchZoomPanDevice(Context context, int height, int width, int toolbar, Bitmap bitmap , double offsetX , double offsetY) {
        super(context);

        this.height = height;
        this.width = width;
        this.toolbar = toolbar;
        this.bitmap = bitmap;

        this.context = context;
        this.mImageWidth = bitmap.getWidth();
        this.mImageHeight = bitmap.getHeight();

        this.offsetX = offsetX;
        this.offsetY = offsetY;

        this.bitmap = calImageRatio(bitmap);




        Log.i("calImageRatio bitmap H", this.bitmap.getHeight() + "");
        Log.i("calImageRatio bitmap W", this.bitmap.getWidth() + "");


        mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mScaleDetector.onTouchEvent(event);

        final int action = event.getAction();

        switch (action & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN: {

                //get x and y cords of where we touch the screen
                final float x = event.getX();
                final float y = event.getY();

                //remember where touch event started
                mLastTouchX = x;
                mLastTouchY = y;

                //save the ID of this pointer
                mActivePointerID = event.getPointerId(0);

                break;
            }
            case MotionEvent.ACTION_MOVE: {

                //find the index of the active pointer and fetch its position
                final int pointerIndex = event.findPointerIndex(mActivePointerID);
                final float x = event.getX(pointerIndex);
                final float y = event.getY(pointerIndex);

                if (!mScaleDetector.isInProgress()) {

                    //calculate the distance in x and y directions
                    final float distanceX = x - mLastTouchX;
                    final float distanceY = y - mLastTouchY;

                    mPositionX += distanceX;
                    mPositionY += distanceY;

                    //redraw canvas call onDraw method
                    invalidate();

                }
                //remember this touch position for next move event
                mLastTouchX = x;
                mLastTouchY = y;


                break;
            }

            case MotionEvent.ACTION_UP: {
                mActivePointerID = INVALID_POINTER_ID;
                break;
            }

            case MotionEvent.ACTION_CANCEL: {
                mActivePointerID = INVALID_POINTER_ID;
                break;

            }

            case MotionEvent.ACTION_POINTER_UP: {
                //Extract the index of the pointer that left the screen
                final int pointerIndex = (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
                final int pointerId = event.getPointerId(pointerIndex);
                if (pointerId == mActivePointerID) {
                    //Our active pointer is going up Choose another active pointer and adjust
                    final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
                    mLastTouchX = event.getX(newPointerIndex);
                    mLastTouchY = event.getY(newPointerIndex);
                    mActivePointerID = event.getPointerId(newPointerIndex);
                }
                break;
            }
        }


        return true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        canvas.save();
        canvas.translate(mPositionX, mPositionY);
        canvas.scale(mScaleFactor, mScaleFactor);

        canvas.drawBitmap(bitmap, new Matrix(), new Paint());

        canvas.save();

        double x = (offsetX / (this.mImageWidth / this.canvasW));
        double y = (offsetY / (this.mImageHeight / this.canvasH));

        Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(context.getResources().getColor(R.color.orangeColor));

        canvas.translate((float) x, (float) y);
        canvas.drawCircle(0, 0, 20, mPaint);
        canvas.restore();

    }


    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {

            mScaleFactor *= scaleGestureDetector.getScaleFactor();
            //don't to let the image get too large or small
            mScaleFactor = Math.max(mMinZoom, Math.min(mScaleFactor, mMaxZoom));


            Log.i("scaleGestureDetector", mScaleFactor + " ");

            Log.i("mPositionX", mPositionX + " ");
            Log.i("mPositionY", mPositionY + " ");

            invalidate();


            return true;
        }
    }

    private Bitmap calImageRatio(Bitmap bitmapResize) {


        double imageH = bitmapResize.getHeight();
        double imageW = bitmapResize.getWidth();


        double screenW = width;
        double screenH = (height - toolbar) - (1 * toolbar);


        double n1 = getMax(getMax(imageW, imageH), getMax(screenW, screenH));
        double n2 = getMin(getMax(imageW, imageH), getMax(screenW, screenH));

        double ratioW = (n1) / (n2);


        double n3 = getMax(getMin(imageW, imageH), getMin(screenW, screenH));
        double n4 = getMin(getMin(imageW, imageH), getMin(screenW, screenH));


        double ratioH = (n3) / (n4);


        double imageScale = getMax(ratioW, ratioH);


        this.canvasW = imageW / imageScale;
        this.canvasH = imageH / imageScale;


        this.ratioW = ratioW;
        this.ratioH = ratioH;

        return Bitmap.createScaledBitmap(bitmap, (int) canvasW, (int) canvasH, true);

    }

    private double getMax(double n1, double n2) {
        if (n1 >= n2) {
            return n1;
        } else {
            return n2;
        }

    }

    private double getMin(double n1, double n2) {
        if (n1 <= n2) {
            return n1;
        } else {
            return n2;
        }

    }

    public double getImageH() {

        return canvasH;
    }

    public double getImageW() {
        return canvasW;
    }


}
