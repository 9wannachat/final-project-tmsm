package app.coe.com.tmsm;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import app.coe.com.tmsm.adapter.DeviceOfUser;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.Register;
import app.coe.com.tmsm.models.RootResDeviceOfUser;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.utility.CryptoHash;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        EditText username = (EditText)findViewById(R.id.txtUser);
        EditText password = (EditText)findViewById(R.id.txtPass);

        EditText fname = (EditText)findViewById(R.id.txtFName);
        EditText lname = (EditText)findViewById(R.id.txtLName);

        EditText phone = (EditText)findViewById(R.id.txtPhone);
        EditText email = (EditText)findViewById(R.id.txtEmail);

        Button btnRegis = (Button)findViewById(R.id.btnRegis);



        btnRegis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Register register = new Register();
                register.setEmail(email.getText().toString());
                register.setfName(fname.getText().toString());
                register.setlName(lname.getText().toString());
                register.setPhonenumber(phone.getText().toString());


                String passwordSHA256 = CryptoHash.hash256(password.getText().toString());
                register.setUsername(username.getText().toString());
                register.setPassword(passwordSHA256);

                callAPI(register);

            }
        });

    }


    public void callAPI(Register register){

        String url = TMSMPreferences.getURL(RegisterActivity.this);
        Log.i("url con " , url);
        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        FeedData feedData = retrofit.create(FeedData.class);



        feedData.userRegis(register).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {

                if(response.body().getCodeStatus() == 200){
                    Toast.makeText(getApplicationContext() , "Register Success" , Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                TMSMPreferences.toLogin(RegisterActivity.this);
            }
        });



    }

}
