package app.coe.com.tmsm;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import app.coe.com.tmsm.adapter.PermitTimer;
import app.coe.com.tmsm.adapter.ReqAdmin;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.ReqAdminArea;
import app.coe.com.tmsm.models.RootReqTimer;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReqAdminFragment extends Fragment {


    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    public ReqAdminFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View v= inflater.inflate(R.layout.fragment_req_admin, container, false);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.reqAdmin);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext() , LinearLayout.VERTICAL));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        callAPI();
        return v;
    }

    private void callAPI(){

        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        final FeedData feedData = retrofit.create(FeedData.class);



        String token = TMSMPreferences.getToken(getContext());

        feedData.getReqAdmin(token).enqueue(new Callback<ReqAdminArea>() {
            @Override
            public void onResponse(Call<ReqAdminArea> call, Response<ReqAdminArea> response) {

                if(response.code() == 401){
                    TMSMPreferences.toLogin(getContext());
                }else if(response.code() == 200){


                    if(response.body().getCodeStatus() ==  200){

                        Log.i("sss" , response.body().getData().size()+"");
                        mAdapter = new ReqAdmin(response.body().getData(), getContext());

                        mRecyclerView.setAdapter(mAdapter);
                    }

                }
            }

            @Override
            public void onFailure(Call<ReqAdminArea> call, Throwable t) {
                TMSMPreferences.toLogin(getContext());
            }
        });

    }

}
