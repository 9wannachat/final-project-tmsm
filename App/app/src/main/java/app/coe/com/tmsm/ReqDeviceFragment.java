package app.coe.com.tmsm;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.w3c.dom.Text;

import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.ReqDevice;
import app.coe.com.tmsm.models.ReqPrivilege;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import okhttp3.HttpUrl;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ReqDeviceFragment extends Fragment {


    TextView nameDvice;
    TextView areaDevice;


    public ReqDeviceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_req_device, container, false);


        CheckBox chkR = (CheckBox)v.findViewById(R.id.chkR);
        CheckBox chkW = (CheckBox)v.findViewById(R.id.chkW);
        CheckBox chkE = (CheckBox)v.findViewById(R.id.chkE);

        CheckBox chkRU = (CheckBox)v.findViewById(R.id.chkRU);
        CheckBox chkWU = (CheckBox)v.findViewById(R.id.chkWU);
        CheckBox chkEU = (CheckBox)v.findViewById(R.id.chkEU);




        String deviceName =  getArguments().getString("deviceName");
        String area = getArguments().getString("area");
        String privilage= getArguments().getString("privilage");


        int types = getArguments().getInt("types");


        if(types == 4){
            chkRU.setChecked(true);
            chkRU.setEnabled(false);
            chkWU.setEnabled(false);
            chkEU.setEnabled(false);
        }


        char[] chk = privilage.toCharArray();

        if(chk[0] == '1'){

            chkR.setChecked(true);
        }

        if(chk[1] == '1'){

            chkW.setChecked(true);
        }


        if(chk[2] == '1'){

            chkE.setChecked(true);
        }


        int deviceID = getArguments().getInt("deviceID");
        int userID =   getArguments().getInt("userID");


        nameDvice = (TextView) v.findViewById(R.id.txtNameDevice);

        areaDevice = (TextView) v.findViewById(R.id.txtAreaDevice);


        EditText txtNameDeviceOfUser  = (EditText) v.findViewById(R.id.txtNameDeviceOfUser);
        txtNameDeviceOfUser.setText(deviceName);



        nameDvice.setText(deviceName);
        areaDevice.setText(area);



        Log.i("deviceID"  , deviceID+"");
        Log.i("userID"  , userID+"");

        callAPI(deviceID , userID);


        Button btnSave = (Button) v.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                String privilegeUser="";

                if(chkRU.isChecked())

                    privilegeUser+="1";

                else
                    privilegeUser+="0";


                if(chkWU.isChecked())
                    privilegeUser+="1";
                else
                    privilegeUser+="0";



                if(chkEU.isChecked())
                    privilegeUser+="1";
                else
                    privilegeUser+="0";



                ReqPrivilege reqPrivilege = new ReqPrivilege();

                reqPrivilege.setDeviceID(deviceID);
                reqPrivilege.setUserID(userID);
                reqPrivilege.setPrivilege(privilegeUser);
                reqPrivilege.setNameDevice(txtNameDeviceOfUser.getText().toString());

                callAPIPrivilage(reqPrivilege);
            }
        });






        return v;

    }

    public void callAPI(int deviceID , int userID){

        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);
        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        FeedData feedData = retrofit.create(FeedData.class);

        String token = TMSMPreferences.getToken(getContext());
        feedData.checkPermit(token , userID, deviceID).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {

                Log.i("Res " , response.code()+"");
                if(response.body().getCodeStatus() != 200){
                    getFragmentManager().popBackStack();
                    Toast.makeText(getContext() , "You have already submitted the request.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {

                Toast.makeText(getContext() , t.getMessage() , Toast.LENGTH_LONG).show();
                TMSMPreferences.toLogin(getContext());
            }
        });
    }


    public void callAPIPrivilage(ReqPrivilege reqPrivilege){

        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);
        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        FeedData feedData = retrofit.create(FeedData.class);

        String token = TMSMPreferences.getToken(getContext());
        feedData.reqDevice(token , reqPrivilege).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                Toast.makeText(getContext() , response.body().getNameStatus() , Toast.LENGTH_LONG).show();
                getFragmentManager().popBackStack();
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                Toast.makeText(getContext() , t.getMessage(), Toast.LENGTH_LONG).show();
                getFragmentManager().popBackStack();
                TMSMPreferences.toLogin(getContext());
            }
        });


    }

}
