package app.coe.com.tmsm;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.w3c.dom.Text;

import app.coe.com.tmsm.adapter.DeviceOfUser;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.ReqPrivilege;
import app.coe.com.tmsm.models.ResPrivilegeDeviceOfUser;
import app.coe.com.tmsm.models.RootResDeviceOfUser;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReqPermitFragment extends Fragment {


    TextView deviceName;
    TextView status;
    Button btnSubmit;
    TextView privilege;

    public ReqPermitFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_req_permit, container, false);

       int idUser =  getArguments().getInt("userID");
       int idDevice =  getArguments().getInt("deviceID");
       String privilagePermit =  getArguments().getString("privilage");
       String nameDevice = getArguments().getString("deviceName");



        deviceName = (TextView) v.findViewById(R.id.deviceName);
        status = (TextView) v.findViewById(R.id.status);

        btnSubmit = (Button) v.findViewById(R.id.btnSubmit);

        TextView privilegeDevice = (TextView) v.findViewById(R.id.privilegeDevice);

        privilegeDevice.setText("Privilage of Device : " + privilagePermit);

        deviceName.setText(nameDevice);


        CheckBox chkRU = (CheckBox) v.findViewById(R.id.chkRU);
        CheckBox chkWU = (CheckBox) v.findViewById(R.id.chkWU);
        CheckBox chkEU = (CheckBox) v.findViewById(R.id.chkEU);


        callAPIGetPrivilege(idUser ,idDevice );
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReqPrivilege reqPrivilege = new ReqPrivilege();


                String privilage = "";

                if(chkRU.isChecked()){
                    privilage  += "1";
                }else{
                    privilage  += "0";
                }


                if(chkWU.isChecked()){
                    privilage  += "1";
                }else{
                    privilage  += "0";
                }


                if(chkEU.isChecked()){
                    privilage  += "1";
                }else{
                    privilage  += "0";
                }


                reqPrivilege.setDeviceID(idDevice);
                reqPrivilege.setUserID(idUser);
                reqPrivilege.setPrivilege(privilage);
                reqPrivilege.setNameDevice(nameDevice);

                callAPI(reqPrivilege);
            }
        });
        return v;
    }


    public void callAPI(ReqPrivilege reqPrivilege){

        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);
        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        FeedData feedData = retrofit.create(FeedData.class);
        String token = TMSMPreferences.getToken(getContext());
        Call<Status> call =  feedData.reqPirvilage(token , reqPrivilege);
        call.enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                Toast.makeText(getContext()  , response.body().getNameStatus() , Toast.LENGTH_LONG).show();

                getFragmentManager().popBackStack();
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                Toast.makeText(getContext()  , t.getMessage() , Toast.LENGTH_LONG).show();
                TMSMPreferences.toLogin(getContext());
            }
        });


    }


    public void callAPIGetPrivilege(int idUser , int idDevice){
        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);

        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        FeedData feedData = retrofit.create(FeedData.class);
        String token = TMSMPreferences.getToken(getContext());
        feedData.getPrivilegeOfUser(token , idUser , idDevice).enqueue(new Callback<ResPrivilegeDeviceOfUser>() {
            @Override
            public void onResponse(Call<ResPrivilegeDeviceOfUser> call, Response<ResPrivilegeDeviceOfUser> response) {

                if(response.body().getCodeStatus() == 200){

                    String s = response.body().getStatusPrivilege();

                    if(s.equals("Does not Allow")){
                        status.setText("Status : " + s );

                    }else{

                        int gn = response.body().getGrantID();

                        if(gn == 1){
                            status.setText("Status : " + s + " " + response.body().getGrantName());

                        }else{

                            status.setText("Status : " + s + " " + response.body().getGrantName() + " " + response.body().getTimeMin() +" - " + response.body().getTimeMax() );
                        }

                    }

                }else{
                    status.setText("Status : ");
                    Toast.makeText(getContext()  , response.body().getNameStatus() , Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResPrivilegeDeviceOfUser> call, Throwable t) {
                Toast.makeText(getContext() , t.getMessage() , Toast.LENGTH_LONG).show();
                TMSMPreferences.toLogin(getContext());
            }
        });


    }

}
