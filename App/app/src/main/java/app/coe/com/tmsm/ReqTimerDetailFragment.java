package app.coe.com.tmsm;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.ReqTimer;
import app.coe.com.tmsm.models.RootReqTimer;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReqTimerDetailFragment extends Fragment {


    public ReqTimerDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_req_timer_detail, container, false);

        TextView txtDevicename = (TextView) v.findViewById(R.id.txtDeviceName);
        TextView txtUsername = (TextView) v.findViewById(R.id.txtUserName);
        TextView txtStart = (TextView) v.findViewById(R.id.txtStart);
        TextView txtEnd = (TextView) v.findViewById(R.id.txtEnd);
        TextView txtStatus  = (TextView) v.findViewById(R.id.txtStatus);
        Button btnOK =(Button) v.findViewById(R.id.btnOK);

        String deviceName = getArguments().getString("deviceName");
        String user = getArguments().getString("user");
        String start = getArguments().getString("start");
        String end = getArguments().getString("end");
        String status = getArguments().getString("status");



        txtDevicename.setText(deviceName);
        txtUsername.setText(user);
        txtStart.setText(start);
        txtEnd.setText(end);
        txtStatus.setText(status);

        int id = getArguments().getInt("id");

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReqTimer reqTimer = new ReqTimer();
                reqTimer.settID(id);
                reqTimer.settStatus(true);
                reqTimer.settEnable(true);

                callAPI(reqTimer);
            }


        });





        return v;
    }

    public void callAPI(ReqTimer reqTimer){

        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        final FeedData feedData = retrofit.create(FeedData.class);

        String token = TMSMPreferences.getToken(getContext());
        feedData.updateReqTimer(token , reqTimer).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                Toast.makeText(getContext() , response.body().getNameStatus() , Toast.LENGTH_SHORT).show();
                getFragmentManager().popBackStack();
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                Toast.makeText(getContext() , t.getMessage() , Toast.LENGTH_SHORT).show();
                getFragmentManager().popBackStack();
                TMSMPreferences.toLogin(getContext());
            }
        });


    }

}
