package app.coe.com.tmsm;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import app.coe.com.tmsm.adapter.IODevice;
import app.coe.com.tmsm.adapter.Sensor;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.IODeviceOfUser;
import app.coe.com.tmsm.models.RootIODeviceOfUser;
import app.coe.com.tmsm.utility.MqttHelper;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class SensorFragment extends Fragment {
    private int idGroup;

    private MqttHelper mqttHelper;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private List<IODeviceOfUser> data;
    public SensorFragment() {
        // Required empty public constructor
    }

    public SensorFragment(int idGroup) {

        this.idGroup = idGroup;
        Log.i("idGroup", idGroup + "");


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_sensor, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.sensor);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext() , LinearLayout.VERTICAL));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        Log.i("sensor " , idGroup+"");


        if(idGroup == 0){

            int idUser = TMSMPreferences.getUserID(getContext());
            callAPI(idUser);
        }else{
            int idUser = TMSMPreferences.getUserID(getContext());

            callAPIGroup(idUser , idGroup);
        }

        startMqtt();
        return v;


    }

    public void callAPI(int userID){

        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);

        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        Log.i("callAPI" , "callAPI");
        FeedData feedData = retrofit.create(FeedData.class);

        String token = TMSMPreferences.getToken(getContext());

        feedData.getDeviceSensor(token , userID).enqueue(new Callback<RootIODeviceOfUser>() {
            @Override
            public void onResponse(Call<RootIODeviceOfUser> call, Response<RootIODeviceOfUser> response) {

                if(response.code() == 200){

                    if(response.body().getCodeStatus() == 200){
                        data = response.body().getDevices();
                        mAdapter = new Sensor(data, getContext());
                        mRecyclerView.setAdapter(mAdapter);
                    }


                }else{
                    TMSMPreferences.toLogin(getContext());
                }
            }

            @Override
            public void onFailure(Call<RootIODeviceOfUser> call, Throwable t) {
                TMSMPreferences.toLogin(getContext());
            }
        });
    }


    public void callAPIGroup(int userID , int group){

        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);

        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        Log.i("callAPI" , "callAPI");
        FeedData feedData = retrofit.create(FeedData.class);

        String token = TMSMPreferences.getToken(getContext());

        feedData.getDeviceSensor(token , userID , group).enqueue(new Callback<RootIODeviceOfUser>() {
            @Override
            public void onResponse(Call<RootIODeviceOfUser> call, Response<RootIODeviceOfUser> response) {

                if(response.code() == 200){

                    if(response.body().getCodeStatus() == 200){

                        data = response.body().getDevices();

                        Log.i("Data Sensor " , data.size()+"");

                        mAdapter = new Sensor(data, getContext());
                        mRecyclerView.setAdapter(mAdapter);
                    }

                }else{
                    TMSMPreferences.toLogin(getContext());
                }
            }

            @Override
            public void onFailure(Call<RootIODeviceOfUser> call, Throwable t) {
                TMSMPreferences.toLogin(getContext());
            }
        });
    }

    private void startMqtt(){


        long s = System.currentTimeMillis() % 1000;
        mqttHelper = new MqttHelper(getContext() , "feedBackSensor"+s);
        mqttHelper.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {
                Log.i("MQTT " , "mqttHelper feedBackMobile");
                mqttHelper.subscribeToTopic("manage-sensor");
            }

            @Override
            public void connectionLost(Throwable throwable) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {

                if(topic.equals("manage-sensor")){
                    Log.i("Topic Sub " , mqttMessage.toString());
                    feedBackValue(mqttMessage.toString());
                }

            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

            }
        });

    }


   private void  feedBackValue(String json){



       double val;
       int thing;
       int ch;
       try {

           JSONObject jsonObject = new JSONObject(json);
           val = jsonObject.getDouble("value");
           thing = jsonObject.getInt("idThing");

           for(IODeviceOfUser device : data){

               Log.i("device " , device.getThingsID() + " " + device.getDeviceCH() );
               if(device.getThingsID() == thing){


                   Log.i("Sensoree" , " - -- - - - - -  -- - -");
                   if(device.getUnit() == 1){

                       if(val == 1.0){
                           device.setValSensor("พบการเคลื่อนไหว");
                       }else{
                           device.setValSensor("ไม่พบการเคลื่อนไหว");
                       }

                   }else{
                       device.setValSensor(val + " " + device.getUnitName());
                   }

               }
           }

           mAdapter.notifyDataSetChanged();

       }catch (JSONException err){

           Log.i("Error", err.toString());
       }

    }




}
