package app.coe.com.tmsm;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import app.coe.com.tmsm.adapter.SensingSensor;
import app.coe.com.tmsm.adapter.Sensor;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.IODeviceOfUser;
import app.coe.com.tmsm.models.RootIODeviceOfUser;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class SensorSensingFragment extends Fragment {


    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private List<IODeviceOfUser> data;
    private int idDevice;
    public SensorSensingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View v=  inflater.inflate(R.layout.fragment_sensor_sensing, container, false);;

        int idUser  = TMSMPreferences.getUserID(getContext());
        idDevice = getArguments().getInt("deviceID");
        mRecyclerView = (RecyclerView) v.findViewById(R.id.sensorSensing);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext() , LinearLayout.VERTICAL));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        callAPI(idUser);

        return v;
    }

    public void callAPI(int userID){

        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);

        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();
        FeedData feedData = retrofit.create(FeedData.class);

        String token = TMSMPreferences.getToken(getContext());

        feedData.getDeviceSensor(token , userID).enqueue(new Callback<RootIODeviceOfUser>() {
            @Override
            public void onResponse(Call<RootIODeviceOfUser> call, Response<RootIODeviceOfUser> response) {

                if(response.code() == 200){

                    if(response.body().getCodeStatus() == 200){
                        data = response.body().getDevices();
                        mAdapter = new SensingSensor(data, getContext() , idDevice);
                        mRecyclerView.setAdapter(mAdapter);
                    }


                }else{
                    TMSMPreferences.toLogin(getContext());
                }
            }

            @Override
            public void onFailure(Call<RootIODeviceOfUser> call, Throwable t) {
                TMSMPreferences.toLogin(getContext());
            }
        });
    }


}
