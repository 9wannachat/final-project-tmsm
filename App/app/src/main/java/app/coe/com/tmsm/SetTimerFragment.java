package app.coe.com.tmsm;


import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import app.coe.com.tmsm.adapter.IODevice;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.RootIODeviceOfUser;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.models.Timer;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class SetTimerFragment extends Fragment {


    TextView txtStart;
    TextView txtEnd;

    private SwitchDateTimeDialogFragment dateTimeFragmentStart;

    private SwitchDateTimeDialogFragment dateTimeFragmentEnd;
    private static final String TAG_DATETIME_FRAGMENT_STRAT = "TAG_DATETIME_FRAGMENT_START";
    private static final String TAG_DATETIME_FRAGMENT_END = "TAG_DATETIME_FRAGMENT_END";

    public SetTimerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_set_timer, container, false);

        txtStart = (TextView) v.findViewById(R.id.txtStart);
        txtEnd = (TextView) v.findViewById(R.id.txtEnd);

        RadioGroup selectSet = (RadioGroup) v.findViewById(R.id.selectSet);
        RadioButton time = (RadioButton)v.findViewById(R.id.time);
        RadioButton dateTime = (RadioButton)v.findViewById(R.id.dateTime);

        RadioButton on = (RadioButton)v.findViewById(R.id.on);
        RadioButton off = (RadioButton)v.findViewById(R.id.off);


        Button btnSet = (Button) v.findViewById(R.id.btnSet);

        datetimeStart();
        datetimeEnd();

        selectSet.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                txtEnd.setText("");
                txtStart.setText("");
            }
        });


        txtStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(time.isChecked()){
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            txtStart.setText( selectedHour + ":" + selectedMinute);
                        }
                    }, hour, minute, true);//Yes 24 hour time
                    mTimePicker.show();
                }

                if(dateTime.isChecked()){

                    dateTimeFragmentStart.startAtCalendarView();
                    dateTimeFragmentStart.setDefaultDateTime(new Date());
                    dateTimeFragmentStart.setDefaultMinute(0);

                    dateTimeFragmentStart.show(getFragmentManager(), TAG_DATETIME_FRAGMENT_STRAT);


                }


            }
        });


        txtEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (time.isChecked()){
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            txtEnd.setText( selectedHour + ":" + selectedMinute);
                        }
                    }, hour, minute, true);//Yes 24 hour time
                    mTimePicker.show();
                }


                if(dateTime.isChecked()){

                    dateTimeFragmentEnd.startAtCalendarView();

                    dateTimeFragmentEnd.setDefaultMinute(0);
                    dateTimeFragmentEnd.setDefaultDateTime(new Date());
                    dateTimeFragmentEnd.show(getFragmentManager(), TAG_DATETIME_FRAGMENT_END);
                }


            }
        });



        btnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int privilege = TMSMPreferences.getPrivilege(getContext());
                int user = TMSMPreferences.getUserID(getContext());

                int deviceID = getArguments().getInt("deviceID");
                Timer timer = new Timer();

                timer.setPrivilege(privilege);
                timer.settUser(user);
                timer.settDevice(deviceID);


                if(dateTime.isChecked()){

                    timer.settDatetimeMin(txtStart.getText().toString());
                    timer.settDatetimeMax(txtEnd.getText().toString());


                }

                if(time.isChecked()){
                    timer.settTimeMin(txtStart.getText().toString());
                    timer.settTimeMax(txtEnd.getText().toString());
                }

                if(on.isChecked()){
                    timer.setStatusDevice(true);
                }

                if(off.isChecked()){
                    timer.setStatusDevice(false);
                }



                Log.i("Time Max " , timer.gettDatetimeMax()+"");
                Log.i("Time Min " , timer.gettDatetimeMin()+"");

                Log.i("Time Max s " , timer.gettTimeMax()+"");
                Log.i("Time Min  s" , timer.gettTimeMin()+"");


                callAPI(timer);
            }
        });

        return v;
    }


    public void callAPI(Timer timer){

        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);

        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        Log.i("callAPI" , "callAPI");
        FeedData feedData = retrofit.create(FeedData.class);
        String token = TMSMPreferences.getToken(getContext());

        feedData.setTimer(token , timer).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                Log.i("Error" , response.body().getNameStatus() );


                Toast.makeText(getContext() , response.body().getNameStatus() , Toast.LENGTH_SHORT).show();
                getFragmentManager().popBackStack();
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {

                Log.i("Error" , t.getMessage());
//
                Toast.makeText(getContext() , t.getMessage() , Toast.LENGTH_LONG).show();
                getFragmentManager().popBackStack();
                TMSMPreferences.toLogin(getContext());
            }
        });

    }



    private void datetimeStart(){

        dateTimeFragmentStart = (SwitchDateTimeDialogFragment) getFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT_STRAT);
        if(dateTimeFragmentStart == null) {
            dateTimeFragmentStart = SwitchDateTimeDialogFragment.newInstance(
                    getString(R.string.label_datetime_dialog),
                    getString(android.R.string.ok),
                    getString(android.R.string.cancel)
            );
        }

        final SimpleDateFormat myDateFormat = new SimpleDateFormat("yyyy:MM:dd HH:mm", java.util.Locale.getDefault());

        dateTimeFragmentStart.set24HoursMode(true);
        dateTimeFragmentStart.setHighlightAMPMSelection(false);

        try {
            dateTimeFragmentStart.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
            Log.e("Datetime ", e.getMessage());
        }


        dateTimeFragmentStart.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
            @Override
            public void onPositiveButtonClick(Date date) {

                txtStart.setText(myDateFormat.format(date));
            }

            @Override
            public void onNegativeButtonClick(Date date) {

            }


            @Override
            public void onNeutralButtonClick(Date date) {



                // Optional if neutral button does'nt exists
                txtStart.setText("");
            }
        });



    }


    private void datetimeEnd(){

        dateTimeFragmentEnd = (SwitchDateTimeDialogFragment) getFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT_END);
        if(dateTimeFragmentEnd == null) {
            dateTimeFragmentEnd = SwitchDateTimeDialogFragment.newInstance(
                    getString(R.string.label_datetime_dialog),
                    getString(android.R.string.ok),
                    getString(android.R.string.cancel)
            );
        }

        final SimpleDateFormat myDateFormat = new SimpleDateFormat("yyyy:MM:dd HH:mm", java.util.Locale.getDefault());

        dateTimeFragmentEnd.set24HoursMode(true);
        dateTimeFragmentEnd.setHighlightAMPMSelection(false);

        try {
            dateTimeFragmentEnd.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
            Log.e("Datetime ", e.getMessage());
        }

        dateTimeFragmentEnd.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
            @Override
            public void onPositiveButtonClick(Date date) {

                txtEnd.setText(myDateFormat.format(date));
            }

            @Override
            public void onNegativeButtonClick(Date date) {

            }


            @Override
            public void onNeutralButtonClick(Date date) {
                txtEnd.setText("");
            }
        });
    }


}
