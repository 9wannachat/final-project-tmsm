package app.coe.com.tmsm;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.Privilege;
import app.coe.com.tmsm.models.ReqPrivilege;
import app.coe.com.tmsm.models.RootPrivilege;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.models.Test;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TestActivity extends AppCompatActivity {



    private Realm realm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);




        callAPI(TMSMPreferences.getUserID(getApplicationContext()));



    }


    public void callAPI(int userID){

        String url = TMSMPreferences.getURL(TestActivity.this);
        Log.i("url con " , url);
        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        FeedData feedData = retrofit.create(FeedData.class);

        String token = TMSMPreferences.getToken(getApplicationContext());

        Call<RootPrivilege> call =  feedData.getPrivilege(token , userID);
       call.enqueue(new Callback<RootPrivilege>() {
           @Override
           public void onResponse(Call<RootPrivilege> call, Response<RootPrivilege> response) {
               Log.i("onResponse" , "onResponse");

               createPrivilege(response.body().getPrivileges());
           }

           @Override
           public void onFailure(Call<RootPrivilege> call, Throwable t) {
               Log.i("onFailure" , "onFailure");
               TMSMPreferences.toLogin(getApplicationContext());
           }
       });


    }


    private void createPrivilege(List<Privilege> list){


        realm=  Realm.getDefaultInstance();

        realm.executeTransactionAsync(new Realm.Transaction() {
                                          @Override
                                          public void execute(Realm realm) {
                                              realm.delete(Privilege.class);
                                              for(Privilege privilege : list){

                                                  realm.insert(privilege);
                                              }
                                          }

                                      }, new Realm.Transaction.OnSuccess() {
                                          @Override
                                          public void onSuccess() {


                                              Log.i("onSuccess" , "SUCCESS");

                                              List<Privilege> list = realm.where(Privilege.class).findAll();

                                              Log.i("onSuccess" , list.size()+"");


                                          }
                                      }, new Realm.Transaction.OnError() {
                                          @Override
                                          public void onError(Throwable error) {
                                              Log.i("onError" , error.getMessage());
                                          }
                                     }

        );
    }
}
