package app.coe.com.tmsm;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import app.coe.com.tmsm.adapter.ReqTimer;
import app.coe.com.tmsm.adapter.Timer;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.RootReqTimer;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class TimerFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    public TimerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v  = inflater.inflate(R.layout.fragment_timer, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.timer);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext() , LinearLayout.VERTICAL));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        int idDevice = getArguments().getInt("deviceID");

        Button setTimer = (Button)v.findViewById(R.id.btnSetTimer);


        callAPI(idDevice);
        int deviceID = getArguments().getInt("deviceID");
        setTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putInt("deviceID" , deviceID);


                SetTimerFragment setTimerFragment =  new SetTimerFragment();
                setTimerFragment.setArguments(bundle);

                FragmentTransaction transaction = ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.mainLayout ,setTimerFragment);
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });

        return v;
    }

    public void callAPI(int idDevice){

        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        final FeedData feedData = retrofit.create(FeedData.class);
        String token = TMSMPreferences.getToken(getContext());

        int user = TMSMPreferences.getUserID(getContext());
      feedData.getTimerUser(token , user , idDevice).enqueue(new Callback<RootReqTimer>() {
          @Override
          public void onResponse(Call<RootReqTimer> call, Response<RootReqTimer> response) {

              if(response.body().getCodeStatus() == 200){

                  mAdapter = new Timer(response.body().getData(), getContext());

                  mRecyclerView.setAdapter(mAdapter);
              }else{
                  Toast.makeText(getContext() , response.body().getNameStatus(), Toast.LENGTH_SHORT).show();
              }
          }

          @Override
          public void onFailure(Call<RootReqTimer> call, Throwable t) {
              TMSMPreferences.toLogin(getContext());
              Toast.makeText(getContext() , t.getMessage() , Toast.LENGTH_SHORT).show();
          }
      });
    }

}
