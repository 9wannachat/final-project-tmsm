package app.coe.com.tmsm.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.Serializable;
import java.util.List;

import app.coe.com.tmsm.DeviceFragment;
import app.coe.com.tmsm.R;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.Device;
import app.coe.com.tmsm.models.ReqDoor;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.models.Thing;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class AllThing  extends RecyclerView.Adapter<AllThing.ViewHolder>{

    private List<Thing> data;
    private Context context;
    public AllThing(List<Thing> data, Context context ){

        this.data = data;
        this.context = context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.all_things, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        Thing thing = data.get(i);


        viewHolder.nameThings.setText(thing.getThingNickName());
        viewHolder.area.setText(thing.getThingRootArea() + " " + thing.getThingArea());
        viewHolder.type.setText(thing.getThingTypeName());
    }

    @Override
    public int getItemCount() {
        return data.size();

    }

    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView nameThings;
        public TextView area;
        public TextView type;

        public ViewHolder(View view) {
            super(view);

            nameThings = (TextView) view.findViewById(R.id.nameThings);
            area = (TextView) view.findViewById(R.id.area);
            type = (TextView) view.findViewById(R.id.type);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.i("TAG_DEBUG", getLayoutPosition() + "");
            Log.i("TAG_DEBUG", data.get(getLayoutPosition()).getThingMAC());

            String rootArea = data.get(getLayoutPosition()).getThingRootArea();
            String area = data.get(getLayoutPosition()).getThingArea();


            Log.i("Status area ", " " + data.get(getLayoutPosition()).getIdArea());

            long type = data.get(getLayoutPosition()).getThingType();






            if (type == 5) {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("send request");
                builder.setMessage("Send a request to use door");

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callAPIPrivilageDoor(data.get(getLayoutPosition()).getThingID());

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();

            } else {
                List<Device> list = data.get(getLayoutPosition()).getDevices();
                long types =  data.get(getLayoutPosition()).getThingType();

                Bundle bundle = new Bundle();
                bundle.putSerializable("data", (Serializable) list);
                bundle.putInt("types" , (int)types);




                DeviceFragment deviceFragment = new DeviceFragment();
                deviceFragment.setArguments(bundle);

                FragmentTransaction transaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.mainLayout, deviceFragment);

                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.addToBackStack(null);
                transaction.commit();
            }




        }
    }


    public void callAPIPrivilageDoor(long thing){
        Log.i("callAPIPrivilageDoor" , "callAPIPrivilageDoor");

        String url = TMSMPreferences.getURL(context);
        Log.i("url con " , url);

        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        FeedData feedData = retrofit.create(FeedData.class);


        ReqDoor reqDoor = new ReqDoor();

        int idUser = TMSMPreferences.getUserID(context);
        reqDoor.setUserID(idUser);
        reqDoor.setThingID((int)thing);

        String token = TMSMPreferences.getToken(context);

        feedData.reqDoor(token , reqDoor).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                Toast.makeText(context , response.body().getNameStatus() , Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                Toast.makeText(context , t.getMessage() , Toast.LENGTH_LONG).show();

                TMSMPreferences.toLogin(context);

            }
        });



    }
}
