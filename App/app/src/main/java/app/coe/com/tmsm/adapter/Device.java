package app.coe.com.tmsm.adapter;

import android.app.AlertDialog;
import android.content.Context;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.List;


import app.coe.com.tmsm.CanvasDeviceActivity;
import app.coe.com.tmsm.CanvasMapActivity;
import app.coe.com.tmsm.DeviceFragment;
import app.coe.com.tmsm.R;
import app.coe.com.tmsm.ReqDeviceFragment;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.AllThing;
import app.coe.com.tmsm.models.ReqDevice;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.models.Thing;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Device  extends RecyclerView.Adapter<Device.ViewHolder>{

    private List<app.coe.com.tmsm.models.Device> data;
    private Context context;
    private int types;
    public Device (int types , List<app.coe.com.tmsm.models.Device> data, Context context ){

        this.data = data;
        this.types = types;
        this.context = context;

    }

    @NonNull
    @Override
    public Device.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.device, viewGroup, false);

        return new Device.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Device.ViewHolder viewHolder, int i) {

        app.coe.com.tmsm.models.Device device = data.get(i);

        viewHolder.nameDevice.setText(device.getDetailName());
        viewHolder.chDevice.setText("Chanel : " + device.getDeviceCH());



    }

    @Override
    public int getItemCount() {
        return data.size();

    }

    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public TextView nameDevice;
        public TextView chDevice;


        public ViewHolder(View view) {
            super(view);

            nameDevice = (TextView) view.findViewById(R.id.nameDevice);
            chDevice = (TextView) view.findViewById(R.id.chDevice);


            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            // setup the alert builder
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(data.get(getLayoutPosition()).getDetailName());
            builder.setMessage(data.get(getLayoutPosition()).getDetailName() + " " + data.get(getLayoutPosition()).getNameArea());

            builder.setPositiveButton("Send a request to use", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    int userID = TMSMPreferences.getUserID(context);
                    int deviceID = data.get(getLayoutPosition()).getDeviceID();


                    String deviceName = data.get(getLayoutPosition()).getDetailName();
                    String area = data.get(getLayoutPosition()).getPositionName() + " " + data.get(getLayoutPosition()).getNameArea();
                    String privilage = data.get(getLayoutPosition()).getDetailPrivilege();
                    if(userID != -1){



                        Bundle bundle = new Bundle();
                        bundle.putInt("deviceID"  , deviceID);
                        bundle.putInt("userID" , userID);
                        bundle.putString("area" , area);
                        bundle.putString("deviceName" , deviceName);
                        bundle.putString("privilage" , privilage);

                        bundle.putInt("types" , types);

                        ReqDeviceFragment deviceFragment =  new ReqDeviceFragment();

                        deviceFragment.setArguments(bundle);
                        FragmentTransaction transaction = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.mainLayout ,deviceFragment);

                        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                        transaction.addToBackStack(null);
                        transaction.commit();


//                        callAPI(reqDevice);
                    }else{
                        Toast.makeText(context , "Error, please log out." , Toast.LENGTH_LONG).show();
                    }
                }
            });
            builder.setNegativeButton("See location", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {


                    String url = data.get(getLayoutPosition()).getImage();
                    double offsetX = data.get(getLayoutPosition()).getOffsetX();
                    double offsetY = data.get(getLayoutPosition()).getOffsetY();

                    Intent intent = new Intent(context , CanvasDeviceActivity.class);
                    intent.putExtra("url" , url);
                    intent.putExtra("offsetX" , offsetX);
                    intent.putExtra("offsetY" , offsetY);

                    context.startActivity(intent);

                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();



        }

    }



}
