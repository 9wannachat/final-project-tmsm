package app.coe.com.tmsm.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import app.coe.com.tmsm.CanvasDeviceActivity;
import app.coe.com.tmsm.ControlFragment;
import app.coe.com.tmsm.GroupDeviceFragment;
import app.coe.com.tmsm.R;
import app.coe.com.tmsm.ReqDeviceFragment;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.DeviceGroupUser;
import app.coe.com.tmsm.models.RootDeviceGroupUser;
import app.coe.com.tmsm.models.RootGruopDevice;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DeviceGroupList extends RecyclerView.Adapter<DeviceGroupList.ViewHolder> {


    private List<DeviceGroupUser> data;
    private Context context;


    public DeviceGroupList(List<DeviceGroupUser> data, Context context ) {
        this.context = context;
        this.data = data;

    }


    @NonNull
    @Override
    public DeviceGroupList.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_group_device, viewGroup, false);

        return new DeviceGroupList.ViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull DeviceGroupList.ViewHolder viewHolder, int i) {

        DeviceGroupUser dd = data.get(i);


        viewHolder.nameDevice.setText(dd.getDeviceName());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView nameDevice;


        public ViewHolder(View view) {
            super(view);

            nameDevice = (TextView) view.findViewById(R.id.txtNameDevice);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {


            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("ลบ");
            builder.setMessage("ต้องการลย " + data.get(getLayoutPosition()).getDeviceName() + " ออกจากกลุ่ม");

            builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {


                    int gid = data.get(getLayoutPosition()).getGdID();
                    int index = getLayoutPosition();
                    callAPI(gid , index);

//
                }
            });
            builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();


        }
    }

    public void callAPI(int gid , int index){

        String url = TMSMPreferences.getURL(context);
        Log.i("url con " , url);

        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        FeedData feedData = retrofit.create(FeedData.class);

        String token = TMSMPreferences.getToken(context);
        feedData.deleteDeviceGroup(token , gid).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                if(response.body().getCodeStatus() == 200){

                    data.remove(index);
                    notifyDataSetChanged();
                }
                Toast.makeText(context , response.body().getNameStatus() , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                TMSMPreferences.toLogin(context);
                Toast.makeText(context , t.getMessage() , Toast.LENGTH_SHORT).show();
            }
        });
    }
}
