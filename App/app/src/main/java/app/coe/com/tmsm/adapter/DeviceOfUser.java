package app.coe.com.tmsm.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.List;

import app.coe.com.tmsm.DeviceFragment;
import app.coe.com.tmsm.DeviceManagerFragment;
import app.coe.com.tmsm.GroupListFragment;
import app.coe.com.tmsm.R;
import app.coe.com.tmsm.ReqPermitFragment;
import app.coe.com.tmsm.SensorSensingFragment;
import app.coe.com.tmsm.TimerFragment;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.ReqCreateGroup;
import app.coe.com.tmsm.models.ReqDevice;
import app.coe.com.tmsm.models.ResDeviceOfUser;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DeviceOfUser extends RecyclerView.Adapter<DeviceOfUser.ViewHolder> {


    private List<ResDeviceOfUser> data;
    private Context context;

    public DeviceOfUser(List<ResDeviceOfUser> data, Context context) {

        this.data = data;
        this.context = context;

    }

    @NonNull
    @Override
    public DeviceOfUser.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.device_of_user, viewGroup, false);

        return new DeviceOfUser.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DeviceOfUser.ViewHolder viewHolder, int i) {

        ResDeviceOfUser device = data.get(i);

        viewHolder.nameDevice.setText(device.getDefName());
        viewHolder.chDevice.setText("Chanel : " + device.getDeviceCH());

        if (device.getPermitStatus() == 1) {
            viewHolder.privilegeDevice.setText("อนุญาตแล้ว");
        } else {
            viewHolder.privilegeDevice.setText("ยังไม่อนุญาต");
        }


        char privilege[] = device.getPermitPrivilage().toCharArray();


        if (privilege[0] == '1') {
            viewHolder.checkBoxR.setChecked(true);
        }

        if (privilege[1] == '1') {
            viewHolder.checkBoxW.setChecked(true);
        }

        if (privilege[2] == '1') {
            viewHolder.checkBoxE.setChecked(true);
        }


        Log.i("device status ", device.getPermitStatus() + "");
    }

    @Override
    public int getItemCount() {
        return data.size();

    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public TextView nameDevice;
        public TextView chDevice;
        public TextView privilegeDevice;

        public CheckBox checkBoxR;
        public CheckBox checkBoxW;
        public CheckBox checkBoxE;

        public ViewHolder(View view) {
            super(view);

            nameDevice = (TextView) view.findViewById(R.id.nameDevice);
            chDevice = (TextView) view.findViewById(R.id.chDevice);
            privilegeDevice = (TextView) view.findViewById(R.id.privilegeDevice);
            checkBoxR = (CheckBox) view.findViewById(R.id.chkRU);
            checkBoxW = (CheckBox) view.findViewById(R.id.chkWU);
            checkBoxE = (CheckBox) view.findViewById(R.id.chkEU);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            int userID = TMSMPreferences.getUserID(context);
            int deviceID = data.get(getLayoutPosition()).getDefDevice();
            String privilage = data.get(getLayoutPosition()).getDetailPrivilege();
            String deviceName = data.get(getLayoutPosition()).getDefName();

            int permitStatus = data.get(getLayoutPosition()).getPermitStatus();


            int type = data.get(getLayoutPosition()).getType();

            char privileges[] = data.get(getLayoutPosition()).getPermitPrivilage().toCharArray();

            Log.i("TYPE", type + "");

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(data.get(getLayoutPosition()).getDetailName());
            builder.setMessage(deviceName);


            int areaDevice = data.get(getLayoutPosition()).getAreaID();
            int privilege = TMSMPreferences.getPrivilege(context);

            int areaAdmin = 0;
            if (privilege == 2) {
                areaAdmin = TMSMPreferences.getAdminArea(context).get(0);
            }
            Log.i("device area ", data.get(getLayoutPosition()).getAreaID() + "");

            if (permitStatus == 1) {


                if (privilege != 1 && (areaDevice != areaAdmin)) {


                    builder.setPositiveButton("ร้องขอสิทธิ์", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Bundle bundle = new Bundle();
                            bundle.putInt("userID", userID);
                            bundle.putInt("deviceID", deviceID);
                            bundle.putString("privilage", privilage);
                            bundle.putString("deviceName", deviceName);

                            Log.i("Device Manager ", userID + " " + deviceID + " " + privilage);

                            ReqPermitFragment reqPermitFragment = new ReqPermitFragment();
                            reqPermitFragment.setArguments(bundle);

                            FragmentTransaction transaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                            transaction.replace(R.id.mainLayout, reqPermitFragment);

                            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                            transaction.addToBackStack(null);
                            transaction.commit();
                        }
                    });

                }


                builder.setNegativeButton("จัดกลุ่ม", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        Bundle bundle = new Bundle();
                        bundle.putString("deviceName", deviceName);
                        bundle.putInt("deviceID", data.get(getLayoutPosition()).getDefDevice());

                        GroupListFragment groupListFragment = new GroupListFragment();
                        groupListFragment.setArguments(bundle);

                        FragmentTransaction transaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.mainLayout, groupListFragment);
                        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                });

                int finalAreaAdmin = areaAdmin;
                builder.setNeutralButton("จัดการอุปกรณ์", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        AlertDialog.Builder builderManager = new AlertDialog.Builder(context);
                        builderManager.setTitle(data.get(getLayoutPosition()).getDetailName());
                        builderManager.setMessage("จัดการอุปกรณ์ " + deviceName);

                        int status = data.get(getLayoutPosition()).getPermitStatus();

                        if (status == 1) {

                            if (privilege != 1 && (areaDevice != finalAreaAdmin)) {
                                builderManager.setPositiveButton("ลบอุปกรณ์", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        int i = getLayoutPosition();
                                        int idUser = TMSMPreferences.getUserID(context);

                                        int idDevice = data.get(i).getDefDevice();

                                        int status = data.get(getLayoutPosition()).getPermitStatus();
                                        int id = data.get(getLayoutPosition()).getDefID();
                                        Log.i("TEST", idUser + " " + idDevice);
                                        callAPIDelete(i, idUser, idDevice, status, id);
                                    }
                                });
                            }
                        } else {

                        }


                        builderManager.setNegativeButton("เปลี่ยนชื่ออุปกรณ์", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                int def = data.get(getLayoutPosition()).getDefID();
                                createAlertRename(getLayoutPosition(), def);

                            }
                        });

                        if (type != 4 && privileges[1] == '1') {
                            builderManager.setNeutralButton("ขั้นสูง", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    AlertDialog.Builder builderManagerAd = new AlertDialog.Builder(context);
                                    builderManagerAd.setTitle(data.get(getLayoutPosition()).getDetailName());
                                    builderManagerAd.setMessage("ขั้นสูง " + deviceName);


                                    builderManagerAd.setNegativeButton("ตั้งเวลา", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Bundle bundle = new Bundle();
                                            bundle.putInt("deviceID", data.get(getLayoutPosition()).getDefDevice());


                                            TimerFragment timerFragment = new TimerFragment();
                                            timerFragment.setArguments(bundle);

                                            FragmentTransaction transaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                                            transaction.replace(R.id.mainLayout, timerFragment);
                                            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                                            transaction.addToBackStack(null);
                                            transaction.commit();

                                        }
                                    });


                                    builderManagerAd.setPositiveButton("Remote Sensing", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Bundle bundle = new Bundle();
                                            bundle.putInt("deviceID", data.get(getLayoutPosition()).getDefDevice());


                                            SensorSensingFragment sensorSensingFragment = new SensorSensingFragment();
                                            sensorSensingFragment.setArguments(bundle);

                                            FragmentTransaction transaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                                            transaction.replace(R.id.mainLayout, sensorSensingFragment);
                                            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                                            transaction.addToBackStack(null);
                                            transaction.commit();

                                        }
                                    });


                                    builderManagerAd.show();


                                }
                            });
                        }


                        AlertDialog dialogmanager = builderManager.create();
                        dialogmanager.show();

                    }
                });


                AlertDialog dialog = builder.create();
                dialog.show();

            } else {


                if (privilege != 1 && (areaDevice != areaAdmin)) {
                    builder.setPositiveButton("ลบคำขอ", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            int i = getLayoutPosition();
                            int idUser = TMSMPreferences.getUserID(context);

                            int idDevice = data.get(i).getDefDevice();

                            int status = data.get(getLayoutPosition()).getPermitStatus();
                            int id = data.get(getLayoutPosition()).getDefID();
                            Log.i("TEST", idUser + " " + idDevice);
                            callAPIDelete(i, idUser, idDevice, status, id);
                        }
                    });
                }

                AlertDialog dialog = builder.create();
                dialog.show();

            }


        }

    }

    private void createAlertRename(int index, int defID) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("ป้อนชื่อ");


        final EditText input = new EditText(context);

        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);


        builder.setPositiveButton("สร้าง", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String name = input.getText().toString();
                ResDeviceOfUser deviceOfUser = new ResDeviceOfUser();
                deviceOfUser.setDefName(name);
                deviceOfUser.setDefID(defID);
                callAPI(index, deviceOfUser);
            }
        });
        builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();

    }

    public void callAPI(int index, ResDeviceOfUser resDeviceOfUser) {

        String url = TMSMPreferences.getURL(context);
        Log.i("url con " , url);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        final FeedData feedData = retrofit.create(FeedData.class);
        String token = TMSMPreferences.getToken(context);
        feedData.updateNameDevice(token, resDeviceOfUser).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {

                if (response.body().getCodeStatus() == 200) {

                    data.get(index).setDefName(resDeviceOfUser.getDefName());
                    notifyDataSetChanged();
                    Toast.makeText(context, response.body().getNameStatus(), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, response.body().getNameStatus(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                TMSMPreferences.toLogin(context);
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }


    public void callAPIDelete(int index, int idUser, int idDevice, int status, int id) {

        String url = TMSMPreferences.getURL(context);
        Log.i("url con " , url);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        final FeedData feedData = retrofit.create(FeedData.class);
        String token = TMSMPreferences.getToken(context);

        feedData.deleteDeviceOfUser(token, idUser, idDevice, status, id).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {

                if (response.body().getCodeStatus() == 200) {
                    data.remove(index);
                    notifyDataSetChanged();
                }

                Toast.makeText(context, response.body().getNameStatus(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                TMSMPreferences.toLogin(context);
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }
}
