package app.coe.com.tmsm.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import app.coe.com.tmsm.CanvasDeviceActivity;
import app.coe.com.tmsm.R;
import app.coe.com.tmsm.ReqDeviceFragment;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.Door;
import app.coe.com.tmsm.models.ResDoor;
import app.coe.com.tmsm.models.RootReqTimer;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DoorReq extends RecyclerView.Adapter<DoorReq.ViewHolder> {



    private List<Door> data;
    private Context context;

    public DoorReq (List<Door> data, Context context ){

        this.data = data;

        this.context = context;

    }

    @NonNull
    @Override
    public DoorReq.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_door_req, viewGroup, false);

        return new DoorReq.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DoorReq.ViewHolder viewHolder, int i) {

        Door door = data.get(i);



        viewHolder.nameUser.setText(door.getFname() + " " + door.getLname());


        if(door.getStatus() == 0){
            viewHolder.status.setText("ยังไม่อนุญาต");
        }else{
            viewHolder.status.setText("อนุญาตแล้ว");
        }
    }

    @Override
    public int getItemCount() {
        return data.size();

    }


    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public TextView nameUser;
        public TextView status;



        public ViewHolder(View view) {
            super(view);

            nameUser = (TextView) view.findViewById(R.id.txtUser);

            status = (TextView)view.findViewById(R.id.status);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Door");
            builder.setMessage("Door");



            int  id = data.get(getLayoutPosition()).getId();

            int index = getLayoutPosition();
            int status = data.get(getLayoutPosition()).getStatus();
            if(status == 0 || status == 1){

                builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callAPIDELETE(id , index);

                    }
                });

            }

            if(status == 0){
                builder.setNegativeButton("Permit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        int thing = data.get(getLayoutPosition()).getThing();
                        int user = data.get(getLayoutPosition()).getUser();
                        app.coe.com.tmsm.models.DoorReq door = new app.coe.com.tmsm.models.DoorReq();
                        door.setStatus(1);
                        door.setThingID(thing);
                        door.setUserID(user);;

                        callAPIUpdate(door , index);
                    }
                });
            }


            AlertDialog dialog = builder.create();
            dialog.show();


        }

    }


    private void callAPIDELETE(int id , int index){

        String url = TMSMPreferences.getURL(context);
        Log.i("url con " , url);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        final FeedData feedData = retrofit.create(FeedData.class);

        String token = TMSMPreferences.getToken(context);

        feedData.deleteDoor(token  , id).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {

                if(response.code() == 200){
                    if(response.body().getCodeStatus() == 200){

                        data.remove(index);
                        notifyDataSetChanged();
                    }else{
                        Toast.makeText(context , response
                        .body().getNameStatus() , Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                TMSMPreferences.toLogin(context);
                Toast.makeText(context , t.getMessage() , Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void callAPIUpdate(app.coe.com.tmsm.models.DoorReq door, int index){

        String url = TMSMPreferences.getURL(context);
        Log.i("url con " , url);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        final FeedData feedData = retrofit.create(FeedData.class);

        String token = TMSMPreferences.getToken(context);


        feedData.updateStatusDoor(token, door).enqueue(new Callback<ResDoor>() {
            @Override
            public void onResponse(Call<ResDoor> call, Response<ResDoor> response) {
                if(response.code() == 200){
                    if(response.body().getCodeStatus()== 200){

                        data.get(index).setStatus(1);
                        notifyDataSetChanged();

                    }else{
                        Toast.makeText(context , response.body().getNameStatus() , Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(context , "Unsccuess" , Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResDoor> call, Throwable t) {
                TMSMPreferences.toLogin(context);
                Toast.makeText(context , t.getMessage() , Toast.LENGTH_SHORT).show();
            }
        });
    }


}
