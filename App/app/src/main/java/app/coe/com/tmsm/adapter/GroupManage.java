package app.coe.com.tmsm.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.List;

import app.coe.com.tmsm.ControlFragment;
import app.coe.com.tmsm.DeviceFragment;
import app.coe.com.tmsm.GroupDeviceFragment;
import app.coe.com.tmsm.R;
import app.coe.com.tmsm.TimerFragment;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GroupManage  extends RecyclerView.Adapter<GroupManage.ViewHolder> {



    private String nameDevice;
    private int idDevice;
    private List<app.coe.com.tmsm.models.GroupDevice> data;
    private Context context;


    public GroupManage(List<app.coe.com.tmsm.models.GroupDevice> data, Context context  , String nameDevice , int idDevice) {
        this.context = context;
        this.data = data;
        this.nameDevice = nameDevice;
        this.idDevice = idDevice;


    }


    @NonNull
    @Override
    public GroupManage.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.group_list, viewGroup, false);

        return new GroupManage.ViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull GroupManage.ViewHolder viewHolder, int i) {

        app.coe.com.tmsm.models.GroupDevice groupDevice = data.get(i);
        viewHolder.nameGroup.setText(groupDevice.getNameGroup());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }




    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView nameGroup;
        public Button btnDelete;
        public  Button btnEdit;
        public Button btnDevice;

        public ViewHolder(View view) {
            super(view);

            nameGroup = (TextView) view.findViewById(R.id.nameGroup);
            btnDelete = (Button) view.findViewById(R.id.btnDelete);
            btnEdit = (Button) view.findViewById(R.id.btnEdit);
            btnDevice = (Button) view.findViewById(R.id.btnDevice);

            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int index = getLayoutPosition();
                    int idGroup = data.get(index).getIdGroup();


                    updateAlert(idGroup  , index);

                }
            });

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int index = getLayoutPosition();

                    int idGroup = data.get(index).getIdGroup();

                    callAPIDELETE(idGroup ,index );

                }
            });

            btnDevice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Bundle bundle = new Bundle();
                    bundle.putSerializable("idGroup" , data.get(getLayoutPosition()).getIdGroup());


                    GroupDeviceFragment deviceFragment =  new GroupDeviceFragment();
                    deviceFragment.setArguments(bundle);

                    FragmentTransaction transaction = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.mainLayout ,deviceFragment);

                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    transaction.addToBackStack(null);
                    transaction.commit();

                }
            });



            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {



            AlertDialog.Builder builderManager = new AlertDialog.Builder(context);
            builderManager.setTitle("เพิ่มเข้ากลุ่ม");
            builderManager.setMessage("เพิ่ม " + nameDevice + " เข้ากลุ่ม " + data.get(getLayoutPosition()).getNameGroup());

            builderManager.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {


                    callAPIADD(data.get(getLayoutPosition()).getIdGroup() , idDevice);
                }
            });

            builderManager.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });



            AlertDialog dialogmanager = builderManager.create();
            dialogmanager.show();


        }
    }

    public void callAPIUPDATE(int idGroup , String name , int index){


        String url = TMSMPreferences.getURL(context);
        Log.i("url con " , url);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        final FeedData feedData = retrofit.create(FeedData.class);

        app.coe.com.tmsm.models.GroupDevice groupDevice = new app.coe.com.tmsm.models.GroupDevice ();
        groupDevice.setIdGroup(idGroup);
        groupDevice.setNameGroup(name);
        String token = TMSMPreferences.getToken(context);
        feedData.updateGroup(token , groupDevice).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {

                if(response.body().getCodeStatus() == 200){
                    data.get(index).setNameGroup(name);
                    notifyDataSetChanged();
                }
                Toast.makeText(context , response.body().getNameStatus() , Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                TMSMPreferences.toLogin(context);
                Toast.makeText(context ,t.getMessage() , Toast.LENGTH_LONG).show();
            }
        });
    }
    public void callAPIDELETE(int idGroup , int index){
        String url = TMSMPreferences.getURL(context);
        Log.i("url con " , url);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        final FeedData feedData = retrofit.create(FeedData.class);
        String token = TMSMPreferences.getToken(context);
        feedData.deleteGroup(token , idGroup).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                if(response.body().getCodeStatus() == 200){
                    data.remove(index);
                    notifyDataSetChanged();
                }
                Toast.makeText(context  , response.body().getNameStatus() , Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                TMSMPreferences.toLogin(context);
                Toast.makeText(context  , t.getMessage() , Toast.LENGTH_LONG).show();
            }
        });
    }


    public void callAPIADD(int idGroup , int device){


        String url = TMSMPreferences.getURL(context);
        Log.i("url con " , url);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        final FeedData feedData = retrofit.create(FeedData.class);

        String token = TMSMPreferences.getToken(context);

        feedData.deviceGroup(token , idGroup , device).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                Toast.makeText(context , response.body().getNameStatus() , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                TMSMPreferences.toLogin(context);
                Toast.makeText(context , t.getMessage() , Toast.LENGTH_SHORT).show();
            }
        });
    }




    private void updateAlert(int idGroup , int index){

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("ป้อนชื่อกลุ่ม");


        final EditText input = new EditText(context);

        input.setInputType(InputType.TYPE_CLASS_TEXT );
        builder.setView(input);


        builder.setPositiveButton("แก้ไข", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {

                callAPIUPDATE(idGroup , input.getText().toString() ,  index);
            }
        });
        builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();

    }


}
