package app.coe.com.tmsm.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import app.coe.com.tmsm.ControlFragment;
import app.coe.com.tmsm.R;
import app.coe.com.tmsm.models.GroupDevice;

public class GroupOfUser  extends RecyclerView.Adapter<GroupOfUser.ViewHolder>{



    private List<GroupDevice> data;
    private Context context;


    public GroupOfUser(List<GroupDevice> data, Context context ) {
        this.context = context;
        this.data = data;

    }


    @NonNull
    @Override
    public GroupOfUser.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.group_list, viewGroup, false);

        return new GroupOfUser.ViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull GroupOfUser.ViewHolder viewHolder, int i) {

        GroupDevice groupDevice = data.get(i);
        viewHolder.nameGroup.setText(groupDevice.getNameGroup());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView nameGroup;

        public ViewHolder(View view) {
            super(view);

            nameGroup = (TextView) view.findViewById(R.id.nameGroup);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {


        }
    }
}
