package app.coe.com.tmsm.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import app.coe.com.tmsm.IOControlFragment;
import app.coe.com.tmsm.R;
import app.coe.com.tmsm.models.IODeviceOfUser;
import app.coe.com.tmsm.models.ScanThings;
import app.coe.com.tmsm.utility.MqttHelper;
import app.coe.com.tmsm.utility.OnBtnClickListener;
import app.coe.com.tmsm.utility.TMSMPreferences;

import static app.coe.com.tmsm.R.*;

public class IODevice extends RecyclerView.Adapter<IODevice.ViewHolder> {



    private MqttHelper mqttHelper;

    private List<IODeviceOfUser> data;
    private Context context;


    public IODevice(List<IODeviceOfUser> data, Context context ) {
        this.context = context;
        this.data = data;

        startMqtt();
    }

    @NonNull
    @Override
    public IODevice.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(layout.io_device, viewGroup, false);

        return new IODevice.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull IODevice.ViewHolder viewHolder, int i) {


        IODeviceOfUser io  = data.get(i);



        if(io.getStatusIO() == 1){

            viewHolder.nameDevice.setText(io.getDefName() + "Read Only");
        }else{
            viewHolder.nameDevice.setText(io.getDefName());
        }







        if(io.isRealStatus()){
            viewHolder.status.setBackgroundResource(R.drawable.off);

        }else{
            viewHolder.status.setBackgroundResource(R.drawable.on);

        }


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{
        public TextView nameDevice;



        public ImageView status;


        public ViewHolder(View view) {
            super(view);

            nameDevice = (TextView) view.findViewById(id.nameDeviceIO);


            status = (ImageView) view.findViewById(id.status) ;


            status.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if(data.get(getLayoutPosition()).getStatusIO() == 1){
                        Toast.makeText(context , "Read Only" , Toast.LENGTH_SHORT).show();
                    }else{
                        boolean status = data.get(getLayoutPosition()).isRealStatus();

                        if(status){
                            try {

//                               topic mobile

                                int device = data.get(getLayoutPosition()).getDefDevice();
                                int ch = data.get(getLayoutPosition()).getDeviceCH();
                                int user = TMSMPreferences.getUserID(context);

                                int idThing = data.get(getLayoutPosition()).getThingsID();


                                Log.i("btn " , idThing +" " + ch + " "+ "open");
                                JSONObject json = new JSONObject();
                                json.put("status", 0);
                                json.put("device", device);
                                json.put("ch", ch);
                                json.put("user", user);
                                json.put("idThing", idThing);


                                sendDataToMqttServer(json.toString());

                            } catch (JSONException e) {

                            }
                        }else{


                            try {


                                int device = data.get(getLayoutPosition()).getDefDevice();
                                int ch = data.get(getLayoutPosition()).getDeviceCH();
                                int user = TMSMPreferences.getUserID(context);

                                int idThing = data.get(getLayoutPosition()).getThingsID();

                                Log.i("btn " , idThing +" " + ch + " "+ "close");

                                JSONObject json = new JSONObject();
                                json.put("status", 1);
                                json.put("device", device);
                                json.put("ch", ch);
                                json.put("user", user);
                                json.put("idThing", idThing);



                                sendDataToMqttServer(json.toString());

                            } catch (JSONException e) {

                            }

                        }
                    }


                }
            });



            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }

    private void startMqtt(){



        long s = System.currentTimeMillis() % 1000;
        mqttHelper = new MqttHelper(context , "ControlMobile"+s);
        mqttHelper.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {
                Log.i("MQTT " , "mqttHelper adapter");
            }

            @Override
            public void connectionLost(Throwable throwable) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {

            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

            }
        });
    }

    public void sendDataToMqttServer(String data){

        MqttMessage message = new MqttMessage();
        message.setPayload(data.getBytes());
        message.setQos(0);
        message.setId(1);

        try {

            mqttHelper.mqttAndroidClient.publish("mobile" , message);

        }catch (MqttException mqtt){

        }
    }
}
