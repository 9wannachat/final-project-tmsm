package app.coe.com.tmsm.adapter;

import android.content.Context;

import android.support.annotation.NonNull;

import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import app.coe.com.tmsm.R;

import app.coe.com.tmsm.models.Thing;
import app.coe.com.tmsm.models.ThingMap;

public class MapThing extends RecyclerView.Adapter<MapThing.ViewHolder> {

    private List<ThingMap> data;
    private Context context;
    public MapThing(List<ThingMap> data, Context context ){

        this.data = data;
        this.context = context;

    }

    @NonNull
    @Override
    public MapThing.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_map_thing, viewGroup, false);

        return new MapThing.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MapThing.ViewHolder viewHolder, int i) {

        ThingMap thing = data.get(i);

        viewHolder.nameArea.setText(thing.getRootArea());
        viewHolder.nameArea.setText(thing.getArea());

    }

    @Override
    public int getItemCount() {
        return data.size();

    }

    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView nameAresaRoot;
        public TextView nameArea;


        public ViewHolder(View view) {
            super(view);

            nameAresaRoot = (TextView) view.findViewById(R.id.txtNameAreaRoot);
            nameArea = (TextView) view.findViewById(R.id.txtArea);


            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }

}
