package app.coe.com.tmsm.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import app.coe.com.tmsm.OffLineControlActivity;
import app.coe.com.tmsm.R;
import app.coe.com.tmsm.models.RealmThing;
import com.jflavio1.wificonnector.*;
import com.jflavio1.wificonnector.interfaces.ConnectionResultListener;
import com.jflavio1.wificonnector.interfaces.ShowWifiListener;
import com.jflavio1.wificonnector.interfaces.WifiStateListener;

import org.json.JSONArray;

public class OffLine extends RecyclerView.Adapter<OffLine.ViewHolder> {


    private List<RealmThing>  data;
    private Context context;
    public OffLine(List<RealmThing>  data, Context context ){

        this.data = data;
        this.context = context;

    }

    @NonNull
    @Override
    public OffLine.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_offline_control, viewGroup, false);

        return new OffLine.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OffLine.ViewHolder viewHolder, int i) {

        RealmThing thing = data.get(i);

        viewHolder.nameThing.setText(thing.getThingNickname());
        viewHolder.ipThing.setText(thing.getThingIP());
        viewHolder.macThing.setText(thing.getThingMAC());
    }

    @Override
    public int getItemCount() {
        return data.size();

    }
    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView nameThing;
        public TextView ipThing;
        public TextView macThing;

        public ViewHolder(View view) {
            super(view);

            nameThing = (TextView) view.findViewById(R.id.nameThing);
            ipThing = (TextView) view.findViewById(R.id.ipThing);
            macThing = (TextView) view.findViewById(R.id.macThing);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int index = getLayoutPosition();
            connectWiFi(data.get(index));
        }
    }

    private void connectWiFi(RealmThing realmThing){

        String s = "A2:20:A6:0E:57:83";
        String name = "TMSM1567986654280";

        WifiConnector connector = new WifiConnector(context, realmThing.getThingName(), realmThing.getThingMAC(), WifiConnector.SECURITY_NONE, "");

        connector.enableWifi();

        connector.connectToWifi(new ConnectionResultListener() {
            @Override
            public void successfulConnect(String SSID) {

                Intent intent = new Intent(context , OffLineControlActivity.class);
                intent.putExtra("idThing" , realmThing.getThingID());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                Toast.makeText(context , "Connect " + SSID + "Successful" , Toast.LENGTH_LONG).show();

            }


            @Override
            public void errorConnect(int codeReason) {

                Toast.makeText(context , "Connect Error + codeReason " + codeReason , Toast.LENGTH_LONG).show();

            }

            @Override
            public void onStateChange(SupplicantState supplicantState) {

            }
        });
    }

}
