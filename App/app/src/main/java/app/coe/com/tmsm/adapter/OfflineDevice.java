package app.coe.com.tmsm.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import app.coe.com.tmsm.R;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.RealmDevice;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class OfflineDevice  extends RecyclerView.Adapter<OfflineDevice.ViewHolder> {

    private List<RealmDevice> data;
    private Context context;
    public OfflineDevice(List<RealmDevice>  data, Context context ){

        this.data = data;
        this.context = context;

    }

    @NonNull
    @Override
    public OfflineDevice.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_offline, viewGroup, false);

        return new OfflineDevice.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OfflineDevice.ViewHolder viewHolder, int i) {

        RealmDevice device = data.get(i);

        viewHolder.txtNameDevice.setText(device.getDefName());

    }

    @Override
    public int getItemCount() {
        return data.size();

    }


    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView txtNameDevice;
        public Button btnOpen;
        public Button btnClose;

        public ViewHolder(View view) {
            super(view);

            txtNameDevice = (TextView) view.findViewById(R.id.txtNameDevice);
            btnOpen = (Button) view.findViewById(R.id.btnOpen);
            btnClose = (Button) view.findViewById(R.id.btnClose);



            btnOpen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(data.get(getLayoutPosition()).getDevicePriority() == 1){
                        Toast.makeText(context , "Open" , Toast.LENGTH_SHORT).show();


                        int ch = data.get(getLayoutPosition()).getDeviceCH();

                        callAPION(ch);
                    }

                }
            });


            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(data.get(getLayoutPosition()).getDevicePriority() == 1){

                        Toast.makeText(context , "Close" , Toast.LENGTH_SHORT).show();

                        int ch = data.get(getLayoutPosition()).getDeviceCH();

                        callAPIOFF(ch);
                    }

                }
            });
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int index = getLayoutPosition();

        }
    }

    public void callAPION(int ch){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(context.getResources().getString(R.string.baseURLThing))
                .addConverterFactory(GsonConverterFactory.create())
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        final FeedData feedData = retrofit.create(FeedData.class);

        feedData.onDevice(ch).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                Log.i("onResponse" ,response.code()+"");
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

                Log.i("onFailure" , t.getMessage());
            }
        });

    }


    public void callAPIOFF(int ch){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(context.getResources().getString(R.string.baseURLThing))
                .addConverterFactory(GsonConverterFactory.create())
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        final FeedData feedData = retrofit.create(FeedData.class);


        feedData.offDevice(ch).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });

    }



}
