package app.coe.com.tmsm.adapter;

import android.content.Context;

import android.os.Bundle;
import android.support.annotation.NonNull;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import app.coe.com.tmsm.PermitAllowDetailFragment;
import app.coe.com.tmsm.R;
import app.coe.com.tmsm.models.PermitRequest;
import app.coe.com.tmsm.models.PermitRes;

public class PermitAllow extends RecyclerView.Adapter<PermitAllow.ViewHolder> {



    private List<PermitRes> data;
    private Context context;

    public PermitAllow(List<PermitRes> data, Context context ){

        this.data = data;
        this.context = context;

    }

    @NonNull
    @Override
    public PermitAllow.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_allow_permit, viewGroup, false);

        return new PermitAllow.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PermitAllow.ViewHolder viewHolder, int i) {

        PermitRes permit = data.get(i);

        viewHolder.deviceName.setText(permit.getNameDevice());
        viewHolder.userName.setText(permit.getFullname());
    }

    @Override
    public int getItemCount() {
        return data.size();

    }

    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView deviceName;
        public TextView userName;


        public ViewHolder(View view) {
            super(view);

            deviceName = (TextView) view.findViewById(R.id.txtNameDevice);
            userName = (TextView) view.findViewById(R.id.txtNameUser);


            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {



            String startDate = data.get(getLayoutPosition()).getPermitTimeMin();
            String endDate = data.get(getLayoutPosition()).getPermitTimeMax();
            int permitID = data.get(getLayoutPosition()).getPermitID();
            int grant = data.get(getLayoutPosition()).getPermitGrant();
            int device = data.get(getLayoutPosition()).getDevice();
            int approve = data.get(getLayoutPosition()).getPermitStatus();
            String privilege = data.get(getLayoutPosition()).getPermitPrivilage();
            int user = data.get(getLayoutPosition()).getPermitUser();
//
//


            int type = data.get(getLayoutPosition()).getType();


            Bundle bundle = new Bundle();
            bundle.putString("startDate" , startDate);
            bundle.putString("endDate" , endDate);
            bundle.putInt("permitID" , permitID);
            bundle.putInt("grant" , grant);
            bundle.putString("privilege" , privilege);
            bundle.putInt("approve" , approve);
            bundle.putInt("device" , device);
            bundle.putInt("user" , user);
            bundle.putInt("type" , type);
//
//
            PermitAllowDetailFragment permitDetailFragment =  new PermitAllowDetailFragment();
            permitDetailFragment.setArguments(bundle);

            FragmentTransaction transaction = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.mainLayout ,permitDetailFragment);

            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            transaction.addToBackStack(null);
            transaction.commit();


        }
    }

}
