package app.coe.com.tmsm.adapter;

import android.content.Context;

import android.os.Bundle;
import android.support.annotation.NonNull;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

import app.coe.com.tmsm.DeviceFragment;
import app.coe.com.tmsm.PermitDetailFragment;
import app.coe.com.tmsm.R;
import app.coe.com.tmsm.models.Device;
import app.coe.com.tmsm.models.PermitRequest;
import app.coe.com.tmsm.models.Thing;


public class PermitReq extends RecyclerView.Adapter<PermitReq.ViewHolder> {



    private List<PermitRequest> data;
    private Context context;
    public PermitReq(List<PermitRequest> data, Context context ){

        this.data = data;
        this.context = context;

    }

    @NonNull
    @Override
    public PermitReq.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_permit_req, viewGroup, false);

        return new PermitReq.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PermitReq.ViewHolder viewHolder, int i) {

        PermitRequest permit = data.get(i);




        viewHolder.deviceName.setText(permit.getDeviceName());
        viewHolder.user.setText(permit.getUserFName() + " " + permit.getUserLName());


        char privilege[] = permit.getPermitPrivilage().toCharArray();


        if(privilege[0] == '1'){
            viewHolder.checkBoxR.setChecked(true);
        }

        if(privilege[1] == '1'){
            viewHolder.checkBoxW.setChecked(true);
        }

        if(privilege[2] == '1'){
            viewHolder.checkBoxE.setChecked(true);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();

    }


    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView deviceName;
        public TextView user;
        public CheckBox checkBoxR;
        public CheckBox checkBoxW;
        public CheckBox checkBoxE;

        public ViewHolder(View view) {
            super(view);

            deviceName = (TextView) view.findViewById(R.id.txtDevice);
            user = (TextView) view.findViewById(R.id.txtUser);

            checkBoxR = (CheckBox) view.findViewById(R.id.chkRU);
            checkBoxW = (CheckBox) view.findViewById(R.id.chkWU);
            checkBoxE = (CheckBox) view.findViewById(R.id.chkEU);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {



            String startDate = data.get(getLayoutPosition()).getPermitTimeMin();
            String endDate = data.get(getLayoutPosition()).getPermitTimeMax();
            int permitID = data.get(getLayoutPosition()).getPermitID();
            int grant = data.get(getLayoutPosition()).getPermitGrantStatus();

            int type = data.get(getLayoutPosition()).getType();

            Log.i("type thing" , type+"");
            String privilege = data.get(getLayoutPosition()).getPermitPrivilage();



            int user = data.get(getLayoutPosition()).getPermitUser();
            int device = data.get(getLayoutPosition()).getPermitDevice();

            Log.i("yes" , device + " " + user);

            Bundle bundle = new Bundle();
            bundle.putString("startDate" , startDate);
            bundle.putString("endDate" , endDate);
            bundle.putInt("permitID" , permitID);
            bundle.putInt("grant" , grant);
            bundle.putInt("device" , device);
            bundle.putInt("user" , user);
            bundle.putInt("type" , type);

            bundle.putString("privilege" , privilege);


            PermitDetailFragment permitDetailFragment =  new PermitDetailFragment();
            permitDetailFragment.setArguments(bundle);

            FragmentTransaction transaction = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.mainLayout ,permitDetailFragment);

            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            transaction.addToBackStack(null);
            transaction.commit();


        }
    }

}
