package app.coe.com.tmsm.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import app.coe.com.tmsm.R;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.ReqTimer;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PermitTimer extends RecyclerView.Adapter<PermitTimer.ViewHolder>  {



    private List<ReqTimer> data;
    private Context context;

    public PermitTimer( List<app.coe.com.tmsm.models.ReqTimer> data, Context context ){

        this.data = data;
        this.context = context;

    }

    @NonNull
    @Override
    public PermitTimer.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_timer_permit, viewGroup, false);

        return new PermitTimer.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PermitTimer.ViewHolder viewHolder, int i) {

        app.coe.com.tmsm.models.ReqTimer req = data.get(i);


        viewHolder.nameDevice.setText(req.getDeviceName());
        viewHolder.nameUser.setText(req.getFullName());

        if(req.istSelect()){
            viewHolder.date.setText(req.gettTimeMin() + " - " + req.gettTimeMax());

        }else{
            viewHolder.date.setText(req.gettDatetimeMin() + " - " + req.gettDatetimeMax());
        }

        if(req.isStatusDevice()){
            viewHolder.status.setText("เปิด");
        }else{
            viewHolder.status.setText("ปิด");
        }

    }

    @Override
    public int getItemCount() {
        return data.size();

    }


    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView nameDevice;
        public TextView nameUser;
        public TextView date;
        public TextView status;

        public ViewHolder(View view) {
            super(view);

            nameDevice = (TextView) view.findViewById(R.id.txtDeviceName);
            nameUser = (TextView) view.findViewById(R.id.txtUser);
            date = (TextView) view.findViewById(R.id.date);
            status = (TextView)view.findViewById(R.id.txtStatus);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {


            String name  = data.get(getLayoutPosition()).getDeviceName();

            int tid = data.get(getLayoutPosition()).gettID();
            int index = getLayoutPosition();
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Delete");
            builder.setMessage("Delete Timer of Device " + name);

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    callAPI(tid , index);

                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();





        }
    }


    public void callAPI(int tid , int index){

        String url = TMSMPreferences.getURL(context);
        Log.i("url con " , url);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        final FeedData feedData = retrofit.create(FeedData.class);
        String token = TMSMPreferences.getToken(context);
        feedData.deleteTimer(token , tid).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {

                if(response.body().getCodeStatus() == 200){

                    data.remove(index);
                    notifyDataSetChanged();
                }
                Toast.makeText(context , response.body().getNameStatus(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                TMSMPreferences.toLogin(context);
                Toast.makeText(context , t.getMessage() , Toast.LENGTH_SHORT).show();
            }
        });

    }

}
