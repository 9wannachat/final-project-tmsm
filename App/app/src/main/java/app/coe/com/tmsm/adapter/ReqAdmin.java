package app.coe.com.tmsm.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import app.coe.com.tmsm.CanvasDeviceActivity;
import app.coe.com.tmsm.R;
import app.coe.com.tmsm.ReqDeviceFragment;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.AdminArea;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.utility.TMSMPreferences;
import app.coe.com.tmsm.utility.UnsafeOkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ReqAdmin   extends RecyclerView.Adapter<ReqAdmin.ViewHolder> {

    private List<AdminArea> data;
    private Context context;
    public ReqAdmin (List<AdminArea> data, Context context ){

        this.data = data;
        this.context = context;

    }

    @NonNull
    @Override
    public ReqAdmin.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_req_admin, viewGroup, false);

        return new ReqAdmin.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReqAdmin.ViewHolder viewHolder, int i) {

        AdminArea adminArea = data.get(i);

        viewHolder.user.setText(adminArea.getFullName());
        viewHolder.area.setText(adminArea.getArea());



    }

    @Override
    public int getItemCount() {
        return data.size();

    }


    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public TextView user;
        public TextView area;


        public ViewHolder(View view) {
            super(view);

            user = (TextView) view.findViewById(R.id.txtUser);
            area = (TextView) view.findViewById(R.id.txtArea);


            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {


            int userID = data.get(getLayoutPosition()).getIdUser();
            int areaID = data.get(getLayoutPosition()).getAreaID();

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("send request");
            builder.setMessage("Send a request to use door");

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    callAPI(getLayoutPosition() , userID , areaID);

                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();


        }

    }

    public void callAPI(int index , int idUser , int idArea) {

        String url = TMSMPreferences.getURL(context);
        Log.i("url con " , url);

        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        FeedData feedData = retrofit.create(FeedData.class);

        String token = TMSMPreferences.getToken(context);


        feedData.permitReqArea(token , idUser , idArea).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {

                if(response.body().getCodeStatus() == 200){

                    Toast.makeText(context , response.body().getNameStatus() , Toast.LENGTH_SHORT).show();
                    data.remove(index);
                    notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                TMSMPreferences.toLogin(context);
            }
        });

    }
}
