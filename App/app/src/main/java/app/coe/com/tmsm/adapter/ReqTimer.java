package app.coe.com.tmsm.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.List;

import app.coe.com.tmsm.DeviceFragment;
import app.coe.com.tmsm.R;
import app.coe.com.tmsm.ReqTimerDetailFragment;
import app.coe.com.tmsm.models.Device;
import app.coe.com.tmsm.models.Thing;


public class ReqTimer  extends RecyclerView.Adapter<ReqTimer.ViewHolder> {


    private List<app.coe.com.tmsm.models.ReqTimer> data;
    private Context context;

    public ReqTimer( List<app.coe.com.tmsm.models.ReqTimer> data, Context context ){

        this.data = data;
        this.context = context;

    }

    @NonNull
    @Override
    public ReqTimer.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_req_timer, viewGroup, false);

        return new ReqTimer.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReqTimer.ViewHolder viewHolder, int i) {

        app.coe.com.tmsm.models.ReqTimer req = data.get(i);


        viewHolder.nameDevice.setText(req.getDeviceName());
        viewHolder.nameUser.setText(req.getFullName());


        if(req.istSelect()){
            viewHolder.date.setText(req.gettTimeMin() + " - " + req.gettTimeMax());


        }else{
            viewHolder.date.setText(req.gettDatetimeMin() + " - " + req.gettDatetimeMax());
        }

        if(req.isStatusDevice()){
            viewHolder.status.setText("Status : Open");
        }else{
            viewHolder.status.setText("Status : Close");
        }

    }

    @Override
    public int getItemCount() {
        return data.size();

    }


    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView nameDevice;
        public TextView nameUser;
        public TextView date;
        public TextView status;

        public ViewHolder(View view) {
            super(view);

            nameDevice = (TextView) view.findViewById(R.id.txtDeviceName);
            nameUser = (TextView) view.findViewById(R.id.txtUser);
            date = (TextView) view.findViewById(R.id.date);
            status = (TextView) view.findViewById(R.id.status);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {



            String deviceName = data.get(getLayoutPosition()).getDeviceName();
            String user = data.get(getLayoutPosition()).getFullName();

            int id = data.get(getLayoutPosition()).gettID();
            String start;
            String end;
            if(data.get(getLayoutPosition()).istSelect()){
                 start = data.get(getLayoutPosition()).gettTimeMin();
                 end = data.get(getLayoutPosition()).gettTimeMax();
            }else{
                start = data.get(getLayoutPosition()).gettDatetimeMin();
                end = data.get(getLayoutPosition()).gettDatetimeMax();
            }
            String status = "ปิด";

            if(data.get(getLayoutPosition()).isStatusDevice()){
                status = "เปิด";
            }

            Bundle bundle = new Bundle();
            bundle.putString("deviceName" , deviceName);
            bundle.putString("user" , user);
            bundle.putString("start" , start);
            bundle.putString("end" , end);
            bundle.putString("status" , status);
            bundle.putInt("id" , id);

            ReqTimerDetailFragment reqTimerDetailFragment =  new ReqTimerDetailFragment();
            reqTimerDetailFragment.setArguments(bundle);

            FragmentTransaction transaction = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.mainLayout ,reqTimerDetailFragment);

            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            transaction.addToBackStack(null);
            transaction.commit();



        }
    }
}
