package app.coe.com.tmsm.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import app.coe.com.tmsm.R;
import app.coe.com.tmsm.ReqDeviceFragment;
import app.coe.com.tmsm.models.IODeviceOfUser;
import app.coe.com.tmsm.utility.SessingSettingFragment;
import app.coe.com.tmsm.utility.TMSMPreferences;

public class SensingSensor   extends  RecyclerView.Adapter<SensingSensor.ViewHolder>{


    private List<IODeviceOfUser> data;
    private Context context;
    private int idDevice;

    public SensingSensor(List<IODeviceOfUser> data, Context context , int idDevice) {
        this.context = context;
        this.data = data;
        this.idDevice = idDevice;


    }

    @NonNull
    @Override
    public SensingSensor.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_sensing_sensor, viewGroup, false);

        return new SensingSensor.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SensingSensor.ViewHolder viewHolder, int i) {


        IODeviceOfUser sensor  = data.get(i);

        viewHolder.nameSensor.setText(sensor.getDefName());


    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView nameSensor;



        public ViewHolder(View view) {
            super(view);
            nameSensor = (TextView) view.findViewById(R.id.txtSensor);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            int idSensor = data.get(getLayoutPosition()).getDefDevice();
            int unit = data.get(getLayoutPosition()).getUnit();

            if(unit == 1){
                int idUser = TMSMPreferences.getUserID(context);
                Bundle bundle = new Bundle();
                bundle.putInt("deviceID"  , idDevice);
                bundle.putInt("userID" , idUser);
                bundle.putInt("sensorID" , idSensor);

                SessingSettingFragment sessingSettingFragment =  new SessingSettingFragment();

                sessingSettingFragment.setArguments(bundle);
                FragmentTransaction transaction = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.mainLayout ,sessingSettingFragment);

                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.addToBackStack(null);
                transaction.commit();
            }


        }
    }
}
