package app.coe.com.tmsm.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import app.coe.com.tmsm.R;
import app.coe.com.tmsm.models.IODeviceOfUser;

public class Sensor extends  RecyclerView.Adapter<Sensor.ViewHolder>{


    private List<IODeviceOfUser> data;
    private Context context;


    public Sensor(List<IODeviceOfUser> data, Context context ) {
        this.context = context;
        this.data = data;


    }

    @NonNull
    @Override
    public Sensor.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_sensors, viewGroup, false);

        return new Sensor.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Sensor.ViewHolder viewHolder, int i) {


        IODeviceOfUser sensor  = data.get(i);


        viewHolder.nameDevice.setText(sensor.getDefName());
        viewHolder.val.setText(sensor.getValSensor());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView nameDevice;
    public TextView val;


    public ViewHolder(View view) {
        super(view);

        nameDevice = (TextView) view.findViewById(R.id.txtDevice);
        val = (TextView) view.findViewById(R.id.txtValue);


        view.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

    }
}

}
