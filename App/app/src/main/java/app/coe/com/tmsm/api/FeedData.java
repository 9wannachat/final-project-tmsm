package app.coe.com.tmsm.api;



import app.coe.com.tmsm.models.AllThing;
import app.coe.com.tmsm.models.DoorReq;
import app.coe.com.tmsm.models.GroupDevice;
import app.coe.com.tmsm.models.OffLineResRoot;
import app.coe.com.tmsm.models.PermitResRoot;
import app.coe.com.tmsm.models.ProfileUser;
import app.coe.com.tmsm.models.Register;
import app.coe.com.tmsm.models.ReqAdminArea;
import app.coe.com.tmsm.models.ReqCreateGroup;
import app.coe.com.tmsm.models.ReqDoor;
import app.coe.com.tmsm.models.ReqLogin;
import app.coe.com.tmsm.models.ReqPermitPrivilage;
import app.coe.com.tmsm.models.ReqPrivilege;
import app.coe.com.tmsm.models.ReqTimer;
import app.coe.com.tmsm.models.RequestSensing;
import app.coe.com.tmsm.models.ResDeviceOfUser;
import app.coe.com.tmsm.models.ResDoor;
import app.coe.com.tmsm.models.ResLogin;
import app.coe.com.tmsm.models.ResPrivilegeDeviceOfUser;
import app.coe.com.tmsm.models.ResThingMap;
import app.coe.com.tmsm.models.RootDeviceGroupUser;
import app.coe.com.tmsm.models.RootGruopDevice;
import app.coe.com.tmsm.models.RootIODeviceOfUser;
import app.coe.com.tmsm.models.RootPermitRequest;
import app.coe.com.tmsm.models.RootPrivilege;
import app.coe.com.tmsm.models.RootReqTimer;
import app.coe.com.tmsm.models.RootResDeviceOfUser;
import app.coe.com.tmsm.models.Status;
import app.coe.com.tmsm.models.Timer;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface FeedData {




    @GET("/on/{ch}")
    Call <String> onDevice(@Path("ch") int ch);


    @GET("/off/{ch}")
    Call <String> offDevice(@Path("ch") int ch);



    @GET("/tmsm-2/profile/{idUser}")
    Call<ProfileUser> getProfileUser(@Header("Authorization") String token , @Path("idUser") int idUser);

    @GET("/tmsm-2/door")
    Call<ResDoor> getReqDoor(@Header("Authorization") String token);

    @GET("/tmsm-2/door/area/{idUser}")
    Call<ResDoor> getReqDoor(@Header("Authorization") String token , @Path("idUser") int idUser);



    @PUT("/tmsm-2/door")
    Call<ResDoor> updateStatusDoor(@Header("Authorization") String token , @Body DoorReq doorReq);



    @DELETE("/tmsm-2/door/{id}")
    Call<Status> deleteDoor(@Header("Authorization") String token , @Path("id") int id) ;




    @GET("/tmsm-2/door/{idUser}")

    Call <Status> checkPrivilegeDoor(@Header("Authorization") String token  , @Path("idUser") int idUser);



    @GET("/tmsm-2/reqdevices/permit")

    Call <RootPermitRequest> getPermitReq(@Header("Authorization") String token);



    @GET("/tmsm-2/reqdevices/permit/{idUser}")

    Call <RootPermitRequest> getPermitReqWithUser(@Header("Authorization") String token  , @Path("idUser") int idUser);


    @POST("/tmsm-2/reqdevices/permition")

    Call <Status> permit(@Header("Authorization") String token  , @Body ReqPermitPrivilage permitPrivilage);


    @POST("/tmsm-2/timerset")

    Call <Status> setTimer(@Header("Authorization") String token  , @Body Timer timer);



    @GET("/tmsm-2/reqdevices/permit/allow")
    Call<PermitResRoot> getAllowPermit(@Header("Authorization") String token );


    @GET("/tmsm-2/reqdevices/permit/allow/{idUser}")
    Call<PermitResRoot> getAllowPermitArea(@Header("Authorization") String token  , @Path("idUser") int idUser);



    @POST("/tmsm-2/door")
    Call <Status> reqDoor(@Header("Authorization") String token  , @Body ReqDoor reqDoor);



    @GET("/tmsm-2/mobile/things/{idUser}")
    Call <ResThingMap> getThingsMap(@Header("Authorization") String token  , @Path("idUser") int idUser);


    @GET("/tmsm-2/timerset/{idUser}/{privilege}")
    Call <RootReqTimer> getReqTimer(@Header("Authorization") String token  , @Path("idUser") int idUser , @Path("privilege") int privilege);


    @GET("/tmsm-2/timerset/{idUser}/device/{idDevice}")
    Call <RootReqTimer> getTimerUser(@Header("Authorization") String token  , @Path("idUser") int idUser , @Path("idDevice") int idDevice);


    @GET("/tmsm-2/timerset/permit/{idUser}/{privilege}")
    Call <RootReqTimer> getTimerPermit(@Header("Authorization") String token  , @Path("idUser") int idUser , @Path("privilege") int privilege);




    @DELETE("/tmsm-2/timerset/{idTimer}")
    Call <Status> deleteTimer(@Header("Authorization") String token  , @Path("idTimer") int idTimer);


    @PUT("/tmsm-2/timerset")
    Call <Status> updateReqTimer(@Header("Authorization") String token  , @Body ReqTimer reqTimer);


    @GET("/tmsm-2/reqdevices/check/{idUser}/{idDevice}")
    Call<Status> checkPermit(@Header("Authorization") String token  , @Path("idUser") int idUser , @Path("idDevice") int idDevice);

    @GET("/tmsm-2/sync/{idUser}/{privilege}")
    Call <OffLineResRoot> syncPrivilage(@Header("Authorization") String token  , @Path("idUser") int idUser , @Path("privilege") int privilege);

    @GET("/tmsm-2/reqdevices/permit/{idUser}/{idDevice}")

    Call <ResPrivilegeDeviceOfUser> getPrivilegeOfUser(@Header("Authorization") String token  , @Path("idUser") int idUser , @Path("idDevice") int idDevice  );


    @GET("/tmsm-2/mobile/things")
    Call <AllThing> getThings(@Header("Authorization") String token  );

    @GET("/tmsm-2/req-admin-area")
    Call <ReqAdminArea> getReqAdmin(@Header("Authorization") String token  );

    @GET("/tmsm-2/approve/{idUser}/{idArea}")
    Call <Status> permitReqArea(@Header("Authorization") String token , @Path("idUser") int idUser , @Path("idArea") int idArea  );



    @POST("/tmsm-2/authenticate")
    Call <ResLogin> userLogin( @Body ReqLogin reqLogin);

    @POST("/tmsm-2/webregis")
    Call <Status> userRegis( @Body Register registe);


    @DELETE("/tmsm-2/group/{idGroup}")
    Call <Status> deleteGroup(@Header("Authorization") String token  , @Path("idGroup") int idGroup);

    @PUT("/tmsm-2/group")
    Call <Status> updateGroup(@Header("Authorization") String token  , @Body GroupDevice groupDevice);

    @PUT("/tmsm-2/mobile/device")
    Call<Status> updateNameDevice(@Header("Authorization") String token  , @Body ResDeviceOfUser resDeviceOfUser);

    @DELETE("/tmsm-2/mobile/device/{idUser}/{idDevice}/{status}/{id}")
    Call<Status> deleteDeviceOfUser(@Header("Authorization") String token  , @Path("idUser") int idUser , @Path("idDevice") int idDevice , @Path("status") int status , @Path("id") int id);


    @POST("/tmsm-2/reqdevices")
    Call <Status> reqDevice(@Header("Authorization") String token  , @Body ReqPrivilege reqPrivilege);


    @GET("/tmsm-2/reqdevices/permitofuser/{id}")
    Call <RootResDeviceOfUser> getDeviceOfUser(@Header("Authorization") String token  , @Path("id") int reqUser);

    @POST("/tmsm-2/reqdevices/reqpermit")
    Call <Status> reqPirvilage(@Header("Authorization") String token  , @Body ReqPrivilege reqPrivilege);

    @GET("/tmsm-2/group/{id}")
    Call <RootGruopDevice> getDeviceGroup(@Header("Authorization") String token , @Path("id") int reqUser);


    @POST("/tmsm-2/group/{idGroup}/{idDevice}")
    Call <Status> deviceGroup(@Header("Authorization") String token  , @Path("idGroup") int idGroup , @Path("idDevice") int idDevice);


    @GET("/tmsm-2/mobile/privilege/{id}")
    Call <RootPrivilege> getPrivilege(@Header("Authorization") String token  , @Path("id") int idUser);


    @GET("/tmsm-2/mobile/device/io/{id}")
    Call <RootIODeviceOfUser> getDeviceIO(@Header("Authorization") String token  , @Path("id") int idUser);

    @GET("/tmsm-2/mobile/device/io/{id}/{group}")
    Call <RootIODeviceOfUser> getDeviceIO(@Header("Authorization") String token  , @Path("id") int idUser , @Path("group") int group);





    @GET("/tmsm-2/mobile/device/sensor/{id}")
    Call <RootIODeviceOfUser> getDeviceSensor(@Header("Authorization") String token  , @Path("id") int idUser);

    @GET("/tmsm-2/mobile/device/sensor/{id}/{group}")
    Call <RootIODeviceOfUser> getDeviceSensor(@Header("Authorization") String token  , @Path("id") int idUser , @Path("group") int group);




    @POST("/tmsm-2/group")
    Call <Status> createGroup(@Header("Authorization") String token  , @Body ReqCreateGroup reqCreateGroup);

    @POST("/tmsm-2/sensing/setting")
    Call <Status> settingSensing(@Header("Authorization") String token  , @Body RequestSensing requestSensing);


    @DELETE("/tmsm-2/group/remove/{gid}")
    Call <Status> deleteDeviceGroup(@Header("Authorization") String token  , @Path("gid") int gid);


    @GET("/tmsm-2/group/device/{idUser}/{idGroup}")
    Call <RootDeviceGroupUser> getDeviceGroup(@Header("Authorization") String token  , @Path("idUser") int idUser , @Path("idGroup") int idGroup);


}




