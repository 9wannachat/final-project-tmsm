
package app.coe.com.tmsm.models;

import java.io.Serializable;
import java.util.List;

public class AllThing  extends  Status implements Serializable {


    private List<Thing> things;


    public List<Thing> getThings() {
        return things;
    }

    public void setThings(List<Thing> things) {
        this.things = things;
    }
}
