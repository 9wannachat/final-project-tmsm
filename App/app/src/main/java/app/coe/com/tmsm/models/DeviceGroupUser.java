package app.coe.com.tmsm.models;

public class DeviceGroupUser {
    private int gdID;
    private int  groupID;
    private String deviceName;
    public int getGdID() {
        return gdID;
    }
    public void setGdID(int gdID) {
        this.gdID = gdID;
    }
    public int getGroupID() {
        return groupID;
    }
    public void setGroupID(int groupID) {
        this.groupID = groupID;
    }
    public String getDeviceName() {
        return deviceName;
    }
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
}
