package app.coe.com.tmsm.models;

public class Door {

    private int id;
    private int user;
    private int thing;
    private int status;
    private String dt;
    private String fname;
    private String lname;


    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getUser() {
        return user;
    }
    public void setUser(int user) {
        this.user = user;
    }
    public String getFname() {
        return fname;
    }
    public void setFname(String fname) {
        this.fname = fname;
    }
    public String getLname() {
        return lname;
    }
    public void setLname(String lname) {
        this.lname = lname;
    }
    public int getThing() {
        return thing;
    }
    public void setThing(int thing) {
        this.thing = thing;
    }
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public String getDt() {
        return dt;
    }
    public void setDt(String dt) {
        this.dt = dt;
    }


}
