package app.coe.com.tmsm.models;

public class DoorReq  {

    private int userID;
    private int thingID;
    private int status;



    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public int getUserID() {
        return userID;
    }
    public void setUserID(int userID) {
        this.userID = userID;
    }
    public int getThingID() {
        return thingID;
    }
    public void setThingID(int thingID) {
        this.thingID = thingID;
    }

}
