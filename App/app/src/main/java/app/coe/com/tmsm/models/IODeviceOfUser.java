package app.coe.com.tmsm.models;

public class IODeviceOfUser {

    private int defDevice;
    private int deviceCH;
    private String defName;
    private  int  permitStatus;
    private int  permitGrantStatus;
    private String permitTimemin;
    private String permitTimemax;
    private String permitPrivilage;
    private String detailPrivilege;
    private int thingsID;
    private int statusIO;
    private boolean realStatus = false;


    private String valSensor = "N/A";

    public String getValSensor() {
        return valSensor;
    }

    public void setValSensor(String valSensor) {
        this.valSensor = valSensor;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    private int unit;
    private String unitName;

    public int getUnit() {
        return unit;
    }


    public void setUnit(int unit) {
        this.unit = unit;
    }


    public String getUnitName() {
        return unitName;
    }





    public IODeviceOfUser() {}

    public boolean isRealStatus() {
        return realStatus;
    }

    public void setRealStatus(boolean realStatus) {
        this.realStatus = realStatus;
    }

    public int getStatusIO() {
        return statusIO;
    }


    public void setStatusIO(int statusIO) {
        this.statusIO = statusIO;
    }


    public int getDefDevice() {
        return defDevice;
    }
    public void setDefDevice(int defDevice) {
        this.defDevice = defDevice;
    }
    public int getDeviceCH() {
        return deviceCH;
    }
    public void setDeviceCH(int deviceCH) {
        this.deviceCH = deviceCH;
    }
    public String getDefName() {
        return defName;
    }
    public void setDefName(String defName) {
        this.defName = defName;
    }
    public int getPermitStatus() {
        return permitStatus;
    }
    public void setPermitStatus(int permitStatus) {
        this.permitStatus = permitStatus;
    }
    public int getPermitGrantStatus() {
        return permitGrantStatus;
    }
    public void setPermitGrantStatus(int permitGrantStatus) {
        this.permitGrantStatus = permitGrantStatus;
    }
    public String getPermitTimemin() {
        return permitTimemin;
    }
    public void setPermitTimemin(String permitTimemin) {
        this.permitTimemin = permitTimemin;
    }
    public String getPermitTimemax() {
        return permitTimemax;
    }
    public void setPermitTimemax(String permitTimemax) {
        this.permitTimemax = permitTimemax;
    }
    public String getPermitPrivilage() {
        return permitPrivilage;
    }
    public void setPermitPrivilage(String permitPrivilage) {
        this.permitPrivilage = permitPrivilage;
    }
    public String getDetailPrivilege() {
        return detailPrivilege;
    }
    public void setDetailPrivilege(String detailPrivilege) {
        this.detailPrivilege = detailPrivilege;
    }
    public int getThingsID() {
        return thingsID;
    }
    public void setThingsID(int thingsID) {
        this.thingsID = thingsID;
    }

}
