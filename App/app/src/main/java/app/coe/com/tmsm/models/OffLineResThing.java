package app.coe.com.tmsm.models;


import java.util.List;

public class OffLineResThing extends  Status {



    private int thingID;
    private String thingName;
    private String thingIP;

    private String thingMAC;

    private String thingNickName;





    public String getThingNickName() {
        return thingNickName;
    }
    public void setThingNickName(String thingNickName) {
        this.thingNickName = thingNickName;
    }


    public String getThingMAC() {
        return thingMAC;
    }

    public void setThingMAC(String thingMAC) {
        this.thingMAC = thingMAC;
    }

    private List<OfflineResDevice> device;



    public List<OfflineResDevice> getDevice() {
        return device;
    }
    public void setDevice(List<OfflineResDevice> device) {
        this.device = device;
    }
    public int getThingID() {
        return thingID;
    }
    public void setThingID(int thingID) {
        this.thingID = thingID;
    }
    public String getThingName() {
        return thingName;
    }
    public void setThingName(String thingName) {
        this.thingName = thingName;
    }
    public String getThingIP() {
        return thingIP;
    }
    public void setThingIP(String thingIP) {
        this.thingIP = thingIP;
    }


}


