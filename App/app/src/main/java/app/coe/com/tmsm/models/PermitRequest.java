package app.coe.com.tmsm.models;

public class PermitRequest {


    private int permitID;
    private int permitUser;
    private int permitStatus;
    private String permitPrivilage;
    private int permitGrantStatus;
    private String permitTimeMin;
    private String permitTimeMax;
    private String userFName;
    private String userLName;
    private String deviceName;


    private int permitDevice;

    private int type;



    public int getType() {
        return type;
    }
    public void setType(int type) {
        this.type = type;
    }



    public int getPermitDevice() {
        return permitDevice;
    }
    public void setPermitDevice(int permitDevice) {
        this.permitDevice = permitDevice;
    }




    public String getDeviceName() {
        return deviceName;
    }
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
    public int getPermitID() {
        return permitID;
    }
    public void setPermitID(int permitID) {
        this.permitID = permitID;
    }
    public int getPermitUser() {
        return permitUser;
    }
    public void setPermitUser(int permitUser) {
        this.permitUser = permitUser;
    }
    public int getPermitStatus() {
        return permitStatus;
    }
    public void setPermitStatus(int permitStatus) {
        this.permitStatus = permitStatus;
    }
    public String getPermitPrivilage() {
        return permitPrivilage;
    }
    public void setPermitPrivilage(String permitPrivilage) {
        this.permitPrivilage = permitPrivilage;
    }
    public int getPermitGrantStatus() {
        return permitGrantStatus;
    }
    public void setPermitGrantStatus(int permitGrantStatus) {
        this.permitGrantStatus = permitGrantStatus;
    }
    public String getPermitTimeMin() {
        return permitTimeMin;
    }
    public void setPermitTimeMin(String permitTimeMin) {
        this.permitTimeMin = permitTimeMin;
    }
    public String getPermitTimeMax() {
        return permitTimeMax;
    }
    public void setPermitTimeMax(String permitTimeMax) {
        this.permitTimeMax = permitTimeMax;
    }
    public String getUserFName() {
        return userFName;
    }
    public void setUserFName(String userFName) {
        this.userFName = userFName;
    }
    public String getUserLName() {
        return userLName;
    }
    public void setUserLName(String userLName) {
        this.userLName = userLName;
    }
}
