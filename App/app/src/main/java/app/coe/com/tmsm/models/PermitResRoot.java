package app.coe.com.tmsm.models;

import java.util.List;

public class PermitResRoot extends  Status {
    private List<PermitRes> data;

    public List<PermitRes> getData() {
        return data;
    }

    public void setData(List<PermitRes> data) {
        this.data = data;
    }
}