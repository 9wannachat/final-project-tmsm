package app.coe.com.tmsm.models;

public class ProfileUser  extends  Status{
    private ResUserProfile profile ;

    public ResUserProfile getProfile() {
        return profile;
    }

    public void setProfile(ResUserProfile profile) {
        this.profile = profile;
    }
}
