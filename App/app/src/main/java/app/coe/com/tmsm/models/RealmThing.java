package app.coe.com.tmsm.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RealmThing extends RealmObject {



    @PrimaryKey
    private int thingID;
    private String thingName;
    private String thingIP;
    private String thingMAC;
    private String thingNickname;



    public RealmThing(){}

    private String nameStatus;
    private int codeStatus;

    public String getThingMAC() {
        return thingMAC;
    }

    public void setThingMAC(String thingMAC) {
        this.thingMAC = thingMAC;
    }

    public String getNameStatus() {
        return nameStatus;
    }
    public void setNameStatus(String nameStatus) {
        this.nameStatus = nameStatus;
    }
    public int getCodeStatus() {
        return codeStatus;
    }
    public void setCodeStatus(int codeStatus) {
        this.codeStatus = codeStatus;
    }


    public int getThingID() {
        return thingID;
    }
    public void setThingID(int thingID) {
        this.thingID = thingID;
    }
    public String getThingName() {
        return thingName;
    }
    public void setThingName(String thingName) {
        this.thingName = thingName;
    }
    public String getThingIP() {
        return thingIP;
    }
    public void setThingIP(String thingIP) {
        this.thingIP = thingIP;
    }


    public String getThingNickname() {
        return thingNickname;
    }

    public void setThingNickname(String thingNickname) {
        this.thingNickname = thingNickname;
    }
}
