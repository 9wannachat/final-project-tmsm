package app.coe.com.tmsm.models;

import java.util.List;

public class ReqAdminArea  extends  Status{

    private List<AdminArea> data;

    public List<AdminArea> getData() {
        return data;
    }

    public void setData(List<AdminArea> data) {
        this.data = data;
    }
}
