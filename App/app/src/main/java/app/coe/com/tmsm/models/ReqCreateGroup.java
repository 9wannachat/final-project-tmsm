package app.coe.com.tmsm.models;

public class ReqCreateGroup {


    private int userID;
    private String nameGroup;
    public int getUserID() {
        return userID;
    }
    public void setUserID(int userID) {
        this.userID = userID;
    }
    public String getNameGroup() {
        return nameGroup;
    }
    public void setNameGroup(String nameGroup) {
        this.nameGroup = nameGroup;
    }


}
