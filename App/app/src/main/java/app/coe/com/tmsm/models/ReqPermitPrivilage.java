package app.coe.com.tmsm.models;

public class ReqPermitPrivilage {


    private int permitID;
    private String permitTimeMin;
    private String permitTimeMax;
    private int permitStatus;
    private int permitGrantStatus;
    private String privilege;


    private boolean enable;

    private int user;
    private int device;



    public int getUser() {
        return user;
    }
    public void setUser(int user) {
        this.user = user;
    }
    public int getDevice() {
        return device;
    }
    public void setDevice(int device) {
        this.device = device;
    }


    public boolean isEnable() {
        return enable;
    }
    public void setEnable(boolean enable) {
        this.enable = enable;
    }


    public String getPrivilege() {
        return privilege;
    }

    public void setPrivilege(String privilege) {
        this.privilege = privilege;
    }

    public int getPermitID() {
        return permitID;
    }
    public void setPermitID(int permitID) {
        this.permitID = permitID;
    }
    public String getPermitTimeMin() {
        return permitTimeMin;
    }
    public void setPermitTimeMin(String permitTimeMin) {
        this.permitTimeMin = permitTimeMin;
    }
    public String getPermitTimeMax() {
        return permitTimeMax;
    }
    public void setPermitTimeMax(String permitTimeMax) {
        this.permitTimeMax = permitTimeMax;
    }
    public int getPermitStatus() {
        return permitStatus;
    }
    public void setPermitStatus(int permitStatus) {
        this.permitStatus = permitStatus;
    }
    public int getPermitGrantStatus() {
        return permitGrantStatus;
    }
    public void setPermitGrantStatus(int permitGrantStatus) {
        this.permitGrantStatus = permitGrantStatus;
    }


}
