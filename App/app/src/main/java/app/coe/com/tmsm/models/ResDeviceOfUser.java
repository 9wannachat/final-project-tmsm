package app.coe.com.tmsm.models;

public class ResDeviceOfUser {

    private int defID;
    private int defDevice;
    private String defName;
    private int deviceESP;
    private int deviceCH;
    private int devicePriority;
    private String detailName;
    private String detailPrivilege;
    private int detailArea;
    private String area;
    private double thingOffsetX;
    private double thingOffsetY;
    private String thingImageArea;
    private String deviceArea;

    private int permitStatus;

    private int areaID;

    private String permitPrivilage;


    private int type;




    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


    public String getPermitPrivilage() {
        return permitPrivilage;
    }

    public void setPermitPrivilage(String permitPrivilage) {
        this.permitPrivilage = permitPrivilage;
    }

    public int getAreaID() {
        return areaID;
    }

    public void setAreaID(int areaID) {
        this.areaID = areaID;
    }

    public int getPermitStatus() {
        return permitStatus;
    }
    public void setPermitStatus(int permitStatus) {
        this.permitStatus = permitStatus;
    }



    public String getDeviceArea() {
        return deviceArea;
    }
    public void setDeviceArea(String deviceArea) {
        this.deviceArea = deviceArea;
    }
    public int getDefID() {
        return defID;
    }
    public void setDefID(int defID) {
        this.defID = defID;
    }
    public int getDefDevice() {
        return defDevice;
    }
    public void setDefDevice(int defDevice) {
        this.defDevice = defDevice;
    }
    public String getDefName() {
        return defName;
    }
    public void setDefName(String defName) {
        this.defName = defName;
    }
    public int getDeviceESP() {
        return deviceESP;
    }
    public void setDeviceESP(int deviceESP) {
        this.deviceESP = deviceESP;
    }
    public int getDeviceCH() {
        return deviceCH;
    }
    public void setDeviceCH(int deviceCH) {
        this.deviceCH = deviceCH;
    }
    public int getDevicePriority() {
        return devicePriority;
    }
    public void setDevicePriority(int devicePriority) {
        this.devicePriority = devicePriority;
    }
    public String getDetailName() {
        return detailName;
    }
    public void setDetailName(String detailName) {
        this.detailName = detailName;
    }
    public String getDetailPrivilege() {
        return detailPrivilege;
    }
    public void setDetailPrivilege(String detailPrivilege) {
        this.detailPrivilege = detailPrivilege;
    }
    public int getDetailArea() {
        return detailArea;
    }
    public void setDetailArea(int detailArea) {
        this.detailArea = detailArea;
    }
    public String getArea() {
        return area;
    }
    public void setArea(String area) {
        this.area = area;
    }
    public double getThingOffsetX() {
        return thingOffsetX;
    }
    public void setThingOffsetX(double thingOffsetX) {
        this.thingOffsetX = thingOffsetX;
    }
    public double getThingOffsetY() {
        return thingOffsetY;
    }
    public void setThingOffsetY(double thingOffsetY) {
        this.thingOffsetY = thingOffsetY;
    }
    public String getThingImageArea() {
        return thingImageArea;
    }
    public void setThingImageArea(String thingImageArea) {
        this.thingImageArea = thingImageArea;
    }

}
