package app.coe.com.tmsm.models;

import java.util.List;

public class ResDoor  extends Status{

    List<Door> door;

    public List<Door> getDoor() {
        return door;
    }

    public void setDoor(List<Door> door) {
        this.door = door;
    }


}
