package app.coe.com.tmsm.models;

import java.util.List;

public class RootDeviceGroupUser  extends  Status{
    private List<DeviceGroupUser> data;

    public List<DeviceGroupUser> getData() {
        return data;
    }

    public void setData(List<DeviceGroupUser> data) {
        this.data = data;
    }

}
