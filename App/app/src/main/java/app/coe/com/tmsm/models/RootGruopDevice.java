package app.coe.com.tmsm.models;

import java.util.List;

public class RootGruopDevice extends  Status{

    List<GroupDevice> groups;

    public List<GroupDevice> getGroups() {
        return groups;
    }

    public void setGroups(List<GroupDevice> groups) {
        this.groups = groups;
    }
}
