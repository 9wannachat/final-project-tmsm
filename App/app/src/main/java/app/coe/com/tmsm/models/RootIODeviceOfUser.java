package app.coe.com.tmsm.models;

import java.util.ArrayList;
import java.util.List;

public class RootIODeviceOfUser extends  Status {

    private ArrayList<IODeviceOfUser> devices;

    public ArrayList<IODeviceOfUser> getDevices() {
        return devices;
    }

    public void setDevices(ArrayList<IODeviceOfUser> devices) {
        this.devices = devices;
    }


}
