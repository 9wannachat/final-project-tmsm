package app.coe.com.tmsm.models;

import java.util.List;

public class RootPermitRequest extends  Status {


    private List<PermitRequest> data;

    public List<PermitRequest> getData() {
        return data;
    }

    public void setData(List<PermitRequest> data) {
        this.data = data;
    }
}
