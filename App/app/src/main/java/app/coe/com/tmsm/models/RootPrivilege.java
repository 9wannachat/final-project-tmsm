package app.coe.com.tmsm.models;

import java.util.List;

public class RootPrivilege extends Status {

    private List<Privilege> privileges;

    public List<Privilege> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(List<Privilege> privileges) {
        this.privileges = privileges;
    }

}
