package app.coe.com.tmsm.models;

import java.util.List;

public class RootReqTimer  extends Status{

    private List<ReqTimer> data;

    public List<ReqTimer> getData() {
        return data;
    }

    public void setData(List<ReqTimer> data) {
        this.data = data;
    }

}
