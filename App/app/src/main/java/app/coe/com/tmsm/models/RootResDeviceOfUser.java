package app.coe.com.tmsm.models;

import java.util.List;

public class RootResDeviceOfUser  extends  Status{


    private List<ResDeviceOfUser> data;

    public List<ResDeviceOfUser> getData() {
        return data;
    }

    public void setData(List<ResDeviceOfUser> data) {
        this.data = data;
    }

}
