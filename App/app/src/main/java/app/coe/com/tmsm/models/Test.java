package app.coe.com.tmsm.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Test  extends RealmObject {


    @PrimaryKey
    private int idSTD;
    private String nameSTD;


    public int getIdSTD() {
        return idSTD;
    }

    public void setIdSTD(int idSTD) {
        this.idSTD = idSTD;
    }

    public String getNameSTD() {
        return nameSTD;
    }

    public void setNameSTD(String nameSTD) {
        this.nameSTD = nameSTD;
    }
}
