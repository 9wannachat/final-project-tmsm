
package app.coe.com.tmsm.models;


import java.io.Serializable;
import java.util.List;

public class Thing    implements Serializable {


    private Long thingID;

    private String thingIP;

    private String thingMAC;

    private String thingName;

    private Long thingType;

    private String thingTypeName;

    private String thingArea;
    private String thingRootArea;

    private int idArea;

    private String thingNickName;

    public String getThingNickName() {
        return thingNickName;
    }

    public void setThingNickName(String thingNickName) {
        this.thingNickName = thingNickName;
    }

    public int getIdArea() {
        return idArea;
    }

    public void setIdArea(int idArea) {
        this.idArea = idArea;
    }


    public String getThingArea() {
        return thingArea;
    }

    public void setThingArea(String thingArea) {
        this.thingArea = thingArea;
    }

    public String getThingRootArea() {
        return thingRootArea;
    }

    public void setThingRootArea(String thingRootArea) {
        this.thingRootArea = thingRootArea;
    }

    private List<Device> devices;


    public List<Device> getDevices() {
        return devices;
    }

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }


    public String getThingTypeName() {
        return thingTypeName;
    }

    public void setThingTypeName(String thingTypeName) {
        this.thingTypeName = thingTypeName;
    }

    public Long getThingID() {
        return thingID;
    }

    public void setThingID(Long thingID) {
        this.thingID = thingID;
    }

    public String getThingIP() {
        return thingIP;
    }

    public void setThingIP(String thingIP) {
        this.thingIP = thingIP;
    }

    public String getThingMAC() {
        return thingMAC;
    }

    public void setThingMAC(String thingMAC) {
        this.thingMAC = thingMAC;
    }

    public String getThingName() {
        return thingName;
    }

    public void setThingName(String thingName) {
        this.thingName = thingName;
    }

    public Long getThingType() {
        return thingType;
    }

    public void setThingType(Long thingType) {
        this.thingType = thingType;
    }
}
