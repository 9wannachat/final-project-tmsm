package app.coe.com.tmsm.models;

public class ThingMap {

    private int thingID;
    private String thingName;
    private double offsetX;
    private double offsetY;
    private String image;
    private String typeName;
    private String rootArea;
    private String area;
    private double lat;
    private double lng;

    public ThingMap(){}
    public String getTypeName() {
        return typeName;
    }
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
    public int getThingID() {
        return thingID;
    }
    public void setThingID(int thingID) {
        this.thingID = thingID;
    }
    public String getThingName() {
        return thingName;
    }
    public void setThingName(String thingName) {
        this.thingName = thingName;
    }
    public double getOffsetX() {
        return offsetX;
    }
    public void setOffsetX(double offsetX) {
        this.offsetX = offsetX;
    }
    public double getOffsetY() {
        return offsetY;
    }
    public void setOffsetY(double offsetY) {
        this.offsetY = offsetY;
    }
    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public String getRootArea() {
        return rootArea;
    }
    public void setRootArea(String rootArea) {
        this.rootArea = rootArea;
    }
    public String getArea() {
        return area;
    }
    public void setArea(String area) {
        this.area = area;
    }
    public double getLat() {
        return lat;
    }
    public void setLat(double lat) {
        this.lat = lat;
    }
    public double getLng() {
        return lng;
    }
    public void setLng(double lng) {
        this.lng = lng;
    }


}
