package app.coe.com.tmsm.utility;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CryptoHash {


    public static String hash256(String data) {

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(data.getBytes());
            return bytesToHex(md.digest());
        }catch (NoSuchAlgorithmException ex){
            return null;
        }


    }
    public static String bytesToHex(byte[] bytes) {
        StringBuffer result = new StringBuffer();
        for (byte byt : bytes) result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
        return result.toString();
    }
}
