package app.coe.com.tmsm.utility;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import app.coe.com.tmsm.R;
import app.coe.com.tmsm.TestActivity;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.RootPrivilege;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Privilege {

    private static Realm realm;



    public static void callAPI(int userID , Context context){


        String url = TMSMPreferences.getURL(context);
        Log.i("url con " , url);

        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        FeedData feedData = retrofit.create(FeedData.class);
        String token = TMSMPreferences.getToken(context);
        Call<RootPrivilege> call =  feedData.getPrivilege(token , userID);
        call.enqueue(new Callback<RootPrivilege>() {
            @Override
            public void onResponse(Call<RootPrivilege> call, Response<RootPrivilege> response) {
                Log.i("onResponse" , "onResponse");

                createPrivilege(response.body().getPrivileges());
            }

            @Override
            public void onFailure(Call<RootPrivilege> call, Throwable t) {
                TMSMPreferences.toLogin(context);
                Log.i("onFailure" , "onFailure");
            }
        });


    }


    private static void  createPrivilege(List<app.coe.com.tmsm.models.Privilege> list){


        realm=  Realm.getDefaultInstance();

        realm.executeTransactionAsync(new Realm.Transaction() {
                                          @Override
                                          public void execute(Realm realm) {
                                              realm.delete(app.coe.com.tmsm.models.Privilege.class);
                                              for(app.coe.com.tmsm.models.Privilege privilege : list){

                                                  realm.insert(privilege);
                                              }
                                          }

                                      }, new Realm.Transaction.OnSuccess() {
                                          @Override
                                          public void onSuccess() {


                                              Log.i("onSuccess" , "SUCCESS");

                                              List<app.coe.com.tmsm.models.Privilege> list = realm.where(app.coe.com.tmsm.models.Privilege.class).findAll();

                                              Log.i("onSuccess" , list.size()+"");


                                          }
                                      }, new Realm.Transaction.OnError() {
                                          @Override
                                          public void onError(Throwable error) {
                                              Log.i("onError" , error.getMessage());
                                          }
                                      }

        );
    }
}
