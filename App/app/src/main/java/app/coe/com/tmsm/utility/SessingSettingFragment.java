package app.coe.com.tmsm.utility;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import app.coe.com.tmsm.R;
import app.coe.com.tmsm.adapter.SensingSensor;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.RequestSensing;
import app.coe.com.tmsm.models.RootIODeviceOfUser;
import app.coe.com.tmsm.models.Status;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class SessingSettingFragment extends Fragment {


    public SessingSettingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_sessing_setting, container, false);


        int deviceIO  = getArguments().getInt("deviceID");
        int sensorID  = getArguments().getInt("sensorID");
        RadioGroup radioGroupIO = (RadioGroup)v.findViewById(R.id.statusIO);
        RadioButton radioOpen =  (RadioButton) v.findViewById(R.id.open);
        RadioButton radioClose =  (RadioButton) v.findViewById(R.id.close);

        RadioGroup radioGroupSensor = (RadioGroup)v.findViewById(R.id.statusSensor);
        RadioButton active =  (RadioButton) v.findViewById(R.id.active);
        RadioButton nonActive =  (RadioButton) v.findViewById(R.id.nonActive);


        Button btn = (Button)v.findViewById(R.id.btnSetting);




        boolean statusIO =false;
        double statusSensor = 0.0;
        if(radioOpen.isChecked()){
            statusIO = true;
        }

        if(radioClose.isChecked()){
            statusIO = false;
        }

        if(active.isChecked()){
            statusSensor = 1.0;
        }

        if(nonActive.isChecked()){
            statusSensor = 0.0;
        }


        double finalStatusSensor = statusSensor;
        boolean finalStatusIO = statusIO;
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestSensing requestSensing = new RequestSensing();

                int idUser = TMSMPreferences.getUserID(getContext());
                requestSensing.setDevicesIO(deviceIO);
                requestSensing.setDevicesSensor(sensorID);
                requestSensing.setSettingIOStatus(finalStatusIO);
                requestSensing.setSettingDatas(finalStatusSensor);
                requestSensing.setSettingUser(idUser);
                requestSensing.setSettingSelect(true);

                callAPI(requestSensing);

            }
        });
        Log.i("syatss" , "" + statusIO + " " +  statusSensor + " " + deviceIO + " " + sensorID);
        return v;
    }

    public void callAPI(RequestSensing requestSensing){

        String url = TMSMPreferences.getURL(getContext());
        Log.i("url con " , url);
        Gson gson = new GsonBuilder() .setLenient() .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();
        FeedData feedData = retrofit.create(FeedData.class);

        String token = TMSMPreferences.getToken(getContext());

        feedData.settingSensing(token , requestSensing).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                Log.i("status" , response.code() + "");

                if(response.code() == 200){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Success");
                    builder.setMessage("คำขอถูกส่งไปแล้ว");
                    builder.show();

                    getFragmentManager().popBackStack();
                }
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {

            }
        });

    }

}
