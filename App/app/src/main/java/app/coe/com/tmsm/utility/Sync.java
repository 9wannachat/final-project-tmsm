package app.coe.com.tmsm.utility;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;

import app.coe.com.tmsm.R;
import app.coe.com.tmsm.api.FeedData;
import app.coe.com.tmsm.models.OffLineResRoot;
import app.coe.com.tmsm.models.OfflineResDevice;
import app.coe.com.tmsm.models.RealmDevice;
import app.coe.com.tmsm.models.RealmThing;
import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Sync {

    private Context context;
    private Realm realm;


    public Sync(Context context){
        realm = Realm.getDefaultInstance();
        this.context  = context;
    }

    public void getData(){

        String url = TMSMPreferences.getURL(context);
        Log.i("url con " , url);

        Log.i("getData"  , "OM");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build();

        final FeedData feedData = retrofit.create(FeedData.class);

        int uid = TMSMPreferences.getUserID(context);

        int privilege = TMSMPreferences.getPrivilege(context);


        String token = TMSMPreferences.getToken(context);

        feedData.syncPrivilage(token , uid , privilege).enqueue(new Callback<OffLineResRoot>() {
            @Override
            public void onResponse(Call<OffLineResRoot> call, Response<OffLineResRoot> response) {

                Log.i("onResponse "  , "200dd " + response.code());

                if(response.code() == 200){

                    if(response.body().getCodeStatus() == 200){


                        Log.i("response getCodeStatus "  , "200");
                        realm.executeTransactionAsync(
                                new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {


                                        realm.delete(RealmThing.class);
                                        realm.delete(RealmDevice.class);

                                        for(int i = 0; i < response.body().getData().size(); i++ ){

                                            int thingID = response.body().getData().get(i).getThingID();
                                            String thingName = response.body().getData().get(i).getThingName();
                                            String thingIP = response.body().getData().get(i).getThingIP();
                                            int codeStatus = response.body().getData().get(i).getCodeStatus();
                                            String thingNickName = response.body().getData().get(i).getThingNickName();

                                            String thingMAC = response.body().getData().get(i).getThingMAC();


                                            RealmThing thing = realm.createObject(RealmThing.class , thingID);
                                            thing.setThingIP(thingIP);
                                            thing.setThingName(thingName);
                                            thing.setThingMAC(thingMAC);
                                            thing.setCodeStatus(codeStatus);
                                            thing.setThingNickname(thingNickName);


                                            if(response.body().getData().get(i).getDevice().size() >= 1){
                                                for(OfflineResDevice devices : response.body().getData().get(i).getDevice()){

                                                    RealmDevice device = realm.createObject(RealmDevice.class , devices.getPermitDevice());

                                                    device.setPermitStatus(devices.getPermitStatus());
                                                    device.setPermitPrivilage(devices.getPermitPrivilage());
                                                    device.setPermitGrantStatus(devices.getPermitGrantStatus());
                                                    device.setPermitTimeMin(devices.getPermitTimeMin());
                                                    device.setPermitTimeMax(devices.getPermitTimeMax());
                                                    device.setDeviceCH(devices.getDeviceCH());
                                                    device.setDevicePriority(devices.getDevicePriority());
                                                    device.setDeviceVisibility(devices.getDeviceVisibility());
                                                    device.setDefName(devices.getDefName());
                                                    device.setThingID(thingID);

                                                }
                                            }



                                        }

                                    }
                                }, new Realm.Transaction.OnSuccess() {
                                    @Override
                                    public void onSuccess() {

                                        RealmResults<RealmThing> resultThing = realm.where(RealmThing.class).findAll();
                                        RealmResults<RealmDevice> resultDevice = realm.where(RealmDevice.class).findAll();

                                        Log.i("onSuccess resultThing" , resultThing.size()+"");
                                        Log.i("onSuccess resultDevice" , resultDevice.size()+"");
                                    }
                                }, new Realm.Transaction.OnError() {
                                    @Override
                                    public void onError(Throwable error) {
                                        error.printStackTrace();

                                    }
                                });



                    }


                }

            }

            @Override
            public void onFailure(Call<OffLineResRoot> call, Throwable t) {


                Log.i("onFailure" , t.getMessage());
            }
        });


    }
}
