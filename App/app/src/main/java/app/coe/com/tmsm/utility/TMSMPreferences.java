package app.coe.com.tmsm.utility;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import app.coe.com.tmsm.LoginActivity;
import app.coe.com.tmsm.R;

public class TMSMPreferences {


    public static  int getUserID(Context context){

        SharedPreferences sp =  context.getSharedPreferences("TMSM", Context.MODE_PRIVATE);
        int userID = sp.getInt("LOGIN_DATA", -1);
        return userID;

    }


    public static  int getPrivilege(Context context){

        SharedPreferences sp =  context.getSharedPreferences("TMSM", Context.MODE_PRIVATE);
        int userID = sp.getInt("LOGIN_Privilege", -1);
        return userID;

    }

    public static  String getToken(Context context){

        SharedPreferences sp =  context.getSharedPreferences("TMSM", Context.MODE_PRIVATE);
        String token = sp.getString("LOGIN_Token" , "0");
        return token;

    }




    public static void removeUserID(Context context){

        SharedPreferences sp =  context.getSharedPreferences("TMSM", Context.MODE_PRIVATE);
        sp.edit().remove("LOGIN_DATA").commit();
    }


    public static void removePrivilege(Context context){

        SharedPreferences sp =  context.getSharedPreferences("TMSM", Context.MODE_PRIVATE);
        sp.edit().remove("LOGIN_Privilege").commit();
    }

    public static void removeArea(Context context){

        SharedPreferences sp =  context.getSharedPreferences("TMSM", Context.MODE_PRIVATE);
        sp.edit().remove("TMSM_AREA").commit();
    }

    public static void removeURL(Context context){

        SharedPreferences sp =  context.getSharedPreferences("TMSM", Context.MODE_PRIVATE);
        sp.edit().remove("TMSM_URL").commit();
    }



    public static void setURL(Context context){


        Log.i("ssss" , "ssssss");
        SharedPreferences sp = context.getSharedPreferences("TMSM", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();


        String url = sp.getString("TMSM_URL",null);


        if(url == null){
            editor.putString("TMSM_URL", context.getString(R.string.baseURLPrimaryFinal));
            editor.commit();

            Log.i("URL" , "https://203.158.177.150:8181/ null");
        }else if(url.equals(context.getString(R.string.baseURLPrimaryFinal))){
            editor.putString("TMSM_URL", context.getString(R.string.baseURLFinal));
            editor.apply();

            Log.i("URL" , "https://159.65.9.17:8181/");

        }else if(url.equals(context.getString(R.string.baseURLFinal))){
            editor.putString("TMSM_URL", context.getString(R.string.baseURLPrimaryFinal));
            editor.apply();

            Log.i("URL" , "https://203.158.177.150:8181/ 159");
        }

    }

    public static String getURL(Context context){

        SharedPreferences sp =  context.getSharedPreferences("TMSM", Context.MODE_PRIVATE);
        String url = sp.getString("TMSM_URL" , context.getString(R.string.baseURLPrimaryFinal));
        return url;
    }

    public static void setAdminArea(Context context , List<Integer> data){

        SharedPreferences sp =  context.getSharedPreferences("TMSM", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sp.edit();

        int size = data.size();

        int[] list = new int[size];

        StringBuilder str = new StringBuilder();
        for (int i = 0; i < list.length; i++) {
            str.append(data.get(i)).append(",");
        }



        String datas = str.toString();

        editor.putString("TMSM_AREA", datas);
        editor.commit();

    }

    public static List<Integer> getAdminArea(Context context){

        List<Integer> area =  new ArrayList<Integer>();

        SharedPreferences sp =  context.getSharedPreferences("TMSM", Context.MODE_PRIVATE);

        String savedString = sp.getString("TMSM_AREA", "0");

        if(!savedString.equals("0")){
            Log.i("savedString" , savedString);

            String id[] = savedString.split(",");

            Log.i("iddd" , id.length+"");
            for (int i = 0; i < id.length; i++) {
                int ids = Integer.parseInt(id[i]);
                area.add(ids);
            }
        }else{
            area.add(0);
            Log.i("savedString" , savedString);
        }



        return area;

    }

    public static void toLogin(Context context){


        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("ผิดพลาด กรุณาออกจากระบบเพื่อทำการเชื่อมต่อใหม่อีกครั้ง")
                .setCancelable(false)
                .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things


                        removeUserID(context);
                        removePrivilege(context);

                        setURL(context);
                        context.startActivity(new Intent(context , LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

                    }
                });

        builder.show();



    }

    public static void session(Context context){


        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Session หมดอายุ กรุณาออกจากระบบเพื่อทำการเชื่อมต่อใหม่อีกครั้ง")
                .setCancelable(false)
                .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
//

                        removeUserID(context);
                        removePrivilege(context);

                        setURL(context);
                        context.startActivity(new Intent(context , LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();



    }
}
