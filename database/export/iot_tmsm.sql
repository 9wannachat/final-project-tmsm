-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 15, 2019 at 06:40 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iot_tmsm`
--

-- --------------------------------------------------------

--
-- Table structure for table `esp`
--

CREATE TABLE `esp` (
  `esp_id` int(4) UNSIGNED ZEROFILL NOT NULL,
  `esp_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `esp_mac` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `esp_ip` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `esp_type` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `esp_status`
--

CREATE TABLE `esp_status` (
  `status_id` int(10) UNSIGNED ZEROFILL NOT NULL,
  `status` char(8) DEFAULT NULL,
  `status_lastdate` datetime DEFAULT NULL,
  `status_usr` int(4) UNSIGNED ZEROFILL DEFAULT NULL,
  `status_esp` int(4) UNSIGNED ZEROFILL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `esp_type`
--

CREATE TABLE `esp_type` (
  `type_id` tinyint(4) NOT NULL,
  `type_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sensor_unit`
--

CREATE TABLE `sensor_unit` (
  `sensor_unit_id` int(4) UNSIGNED ZEROFILL NOT NULL,
  `sensor_unit_thing` int(4) UNSIGNED ZEROFILL DEFAULT NULL,
  `sensor_unit` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sensor_value`
--

CREATE TABLE `sensor_value` (
  `sensor_id` int(10) UNSIGNED ZEROFILL NOT NULL,
  `sensor_thing` int(4) UNSIGNED ZEROFILL DEFAULT NULL,
  `sensor_values` decimal(6,2) DEFAULT NULL,
  `sensor_last` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `setting_remote_sensing`
--

CREATE TABLE `setting_remote_sensing` (
  `setting_id` int(10) UNSIGNED ZEROFILL NOT NULL,
  `things_io` int(4) UNSIGNED ZEROFILL DEFAULT NULL,
  `things_sensor` int(4) UNSIGNED ZEROFILL DEFAULT NULL,
  `setting_user` int(4) UNSIGNED ZEROFILL DEFAULT NULL,
  `setting_min` decimal(6,2) DEFAULT NULL,
  `setting_max` decimal(6,2) DEFAULT NULL,
  `setting_status` tinyint(4) DEFAULT NULL,
  `setting_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `things`
--

CREATE TABLE `things` (
  `thing_id` int(4) UNSIGNED ZEROFILL NOT NULL,
  `thing_esp` int(4) UNSIGNED ZEROFILL DEFAULT NULL,
  `thnig_ch` tinyint(4) DEFAULT NULL,
  `thing_priority` tinyint(4) DEFAULT NULL,
  `thing_visibility` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `things_detail`
--

CREATE TABLE `things_detail` (
  `detail_id` int(4) UNSIGNED ZEROFILL NOT NULL,
  `detail_name` varchar(20) DEFAULT NULL,
  `detail_things` int(4) UNSIGNED ZEROFILL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `things_priority`
--

CREATE TABLE `things_priority` (
  `priority_id` tinyint(4) NOT NULL,
  `priority_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `thing_of_user`
--

CREATE TABLE `thing_of_user` (
  `thou_id` int(10) UNSIGNED ZEROFILL NOT NULL,
  `thou_usr` int(4) UNSIGNED ZEROFILL DEFAULT NULL,
  `thou_things` int(4) UNSIGNED ZEROFILL DEFAULT NULL,
  `thou_name` varchar(50) DEFAULT NULL,
  `thou_last` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `thing_status`
--

CREATE TABLE `thing_status` (
  `status_id` tinyint(4) NOT NULL,
  `status_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `thing_visibility`
--

CREATE TABLE `thing_visibility` (
  `visibility_id` tinyint(4) NOT NULL,
  `visibility_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `unit_id` tinyint(4) NOT NULL,
  `unit_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `usr_id` int(4) UNSIGNED ZEROFILL NOT NULL,
  `usr_user` varchar(50) DEFAULT NULL,
  `usr_pass` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_select_things`
--

CREATE TABLE `user_select_things` (
  `select_id` int(4) UNSIGNED ZEROFILL NOT NULL,
  `select_usr` int(4) UNSIGNED ZEROFILL DEFAULT NULL,
  `select_thing` int(4) UNSIGNED ZEROFILL DEFAULT NULL,
  `select_status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `esp`
--
ALTER TABLE `esp`
  ADD PRIMARY KEY (`esp_id`),
  ADD KEY `esp_type` (`esp_type`);

--
-- Indexes for table `esp_status`
--
ALTER TABLE `esp_status`
  ADD PRIMARY KEY (`status_id`),
  ADD KEY `status_usr` (`status_usr`),
  ADD KEY `status_esp` (`status_esp`);

--
-- Indexes for table `esp_type`
--
ALTER TABLE `esp_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `sensor_unit`
--
ALTER TABLE `sensor_unit`
  ADD PRIMARY KEY (`sensor_unit_id`),
  ADD KEY `sensor_unit_thing` (`sensor_unit_thing`),
  ADD KEY `sensor_unit` (`sensor_unit`);

--
-- Indexes for table `sensor_value`
--
ALTER TABLE `sensor_value`
  ADD PRIMARY KEY (`sensor_id`),
  ADD KEY `sensor_thing` (`sensor_thing`);

--
-- Indexes for table `setting_remote_sensing`
--
ALTER TABLE `setting_remote_sensing`
  ADD PRIMARY KEY (`setting_id`),
  ADD KEY `things_io` (`things_io`),
  ADD KEY `things_sensor` (`things_sensor`),
  ADD KEY `setting_user` (`setting_user`),
  ADD KEY `setting_status` (`setting_status`);

--
-- Indexes for table `things`
--
ALTER TABLE `things`
  ADD PRIMARY KEY (`thing_id`),
  ADD KEY `thing_esp` (`thing_esp`),
  ADD KEY `thing_prio` (`thing_priority`),
  ADD KEY `thing_visible` (`thing_visibility`);

--
-- Indexes for table `things_detail`
--
ALTER TABLE `things_detail`
  ADD PRIMARY KEY (`detail_id`),
  ADD KEY `detail_things` (`detail_things`);

--
-- Indexes for table `things_priority`
--
ALTER TABLE `things_priority`
  ADD PRIMARY KEY (`priority_id`);

--
-- Indexes for table `thing_of_user`
--
ALTER TABLE `thing_of_user`
  ADD PRIMARY KEY (`thou_id`),
  ADD KEY `thou_usr` (`thou_usr`),
  ADD KEY `thou_things` (`thou_things`);

--
-- Indexes for table `thing_status`
--
ALTER TABLE `thing_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `thing_visibility`
--
ALTER TABLE `thing_visibility`
  ADD PRIMARY KEY (`visibility_id`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`unit_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`usr_id`);

--
-- Indexes for table `user_select_things`
--
ALTER TABLE `user_select_things`
  ADD PRIMARY KEY (`select_id`),
  ADD KEY `select_usr` (`select_usr`),
  ADD KEY `select_thing` (`select_thing`),
  ADD KEY `select_status` (`select_status`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `esp`
--
ALTER TABLE `esp`
  MODIFY `esp_id` int(4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `esp_status`
--
ALTER TABLE `esp_status`
  MODIFY `status_id` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `esp_type`
--
ALTER TABLE `esp_type`
  MODIFY `type_id` tinyint(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sensor_unit`
--
ALTER TABLE `sensor_unit`
  MODIFY `sensor_unit_id` int(4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sensor_value`
--
ALTER TABLE `sensor_value`
  MODIFY `sensor_id` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `setting_remote_sensing`
--
ALTER TABLE `setting_remote_sensing`
  MODIFY `setting_id` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `things`
--
ALTER TABLE `things`
  MODIFY `thing_id` int(4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `things_detail`
--
ALTER TABLE `things_detail`
  MODIFY `detail_id` int(4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `things_priority`
--
ALTER TABLE `things_priority`
  MODIFY `priority_id` tinyint(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `thing_of_user`
--
ALTER TABLE `thing_of_user`
  MODIFY `thou_id` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `thing_status`
--
ALTER TABLE `thing_status`
  MODIFY `status_id` tinyint(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `thing_visibility`
--
ALTER TABLE `thing_visibility`
  MODIFY `visibility_id` tinyint(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `unit_id` tinyint(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `usr_id` int(4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_select_things`
--
ALTER TABLE `user_select_things`
  MODIFY `select_id` int(4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `esp`
--
ALTER TABLE `esp`
  ADD CONSTRAINT `esp_ibfk_1` FOREIGN KEY (`esp_type`) REFERENCES `esp_type` (`type_id`);

--
-- Constraints for table `esp_status`
--
ALTER TABLE `esp_status`
  ADD CONSTRAINT `esp_status_ibfk_1` FOREIGN KEY (`status_usr`) REFERENCES `user` (`usr_id`),
  ADD CONSTRAINT `esp_status_ibfk_2` FOREIGN KEY (`status_esp`) REFERENCES `esp` (`esp_id`);

--
-- Constraints for table `sensor_unit`
--
ALTER TABLE `sensor_unit`
  ADD CONSTRAINT `sensor_unit_ibfk_1` FOREIGN KEY (`sensor_unit_thing`) REFERENCES `things` (`thing_id`),
  ADD CONSTRAINT `sensor_unit_ibfk_2` FOREIGN KEY (`sensor_unit`) REFERENCES `unit` (`unit_id`);

--
-- Constraints for table `sensor_value`
--
ALTER TABLE `sensor_value`
  ADD CONSTRAINT `sensor_value_ibfk_1` FOREIGN KEY (`sensor_thing`) REFERENCES `things` (`thing_id`);

--
-- Constraints for table `setting_remote_sensing`
--
ALTER TABLE `setting_remote_sensing`
  ADD CONSTRAINT `setting_remote_sensing_ibfk_1` FOREIGN KEY (`things_io`) REFERENCES `things` (`thing_id`),
  ADD CONSTRAINT `setting_remote_sensing_ibfk_2` FOREIGN KEY (`things_sensor`) REFERENCES `things` (`thing_id`),
  ADD CONSTRAINT `setting_remote_sensing_ibfk_3` FOREIGN KEY (`setting_user`) REFERENCES `user` (`usr_id`),
  ADD CONSTRAINT `setting_remote_sensing_ibfk_4` FOREIGN KEY (`setting_status`) REFERENCES `thing_status` (`status_id`);

--
-- Constraints for table `things`
--
ALTER TABLE `things`
  ADD CONSTRAINT `thing_prio` FOREIGN KEY (`thing_priority`) REFERENCES `things_priority` (`priority_id`),
  ADD CONSTRAINT `thing_visible` FOREIGN KEY (`thing_visibility`) REFERENCES `thing_visibility` (`visibility_id`),
  ADD CONSTRAINT `things_ibfk_1` FOREIGN KEY (`thing_esp`) REFERENCES `esp` (`esp_id`);

--
-- Constraints for table `things_detail`
--
ALTER TABLE `things_detail`
  ADD CONSTRAINT `things_detail_ibfk_1` FOREIGN KEY (`detail_things`) REFERENCES `things` (`thing_id`);

--
-- Constraints for table `thing_of_user`
--
ALTER TABLE `thing_of_user`
  ADD CONSTRAINT `thing_of_user_ibfk_1` FOREIGN KEY (`thou_usr`) REFERENCES `user` (`usr_id`),
  ADD CONSTRAINT `thing_of_user_ibfk_2` FOREIGN KEY (`thou_things`) REFERENCES `things` (`thing_id`);

--
-- Constraints for table `user_select_things`
--
ALTER TABLE `user_select_things`
  ADD CONSTRAINT `user_select_things_ibfk_1` FOREIGN KEY (`select_usr`) REFERENCES `user` (`usr_id`),
  ADD CONSTRAINT `user_select_things_ibfk_2` FOREIGN KEY (`select_thing`) REFERENCES `things` (`thing_id`),
  ADD CONSTRAINT `user_select_things_ibfk_3` FOREIGN KEY (`select_status`) REFERENCES `thing_status` (`status_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
