use iot_tmsm;

create table grant_status(
	gs_id tinyint auto_increment primary key,
    gs_na varchar(20)
)collate utf8_general_ci;

create table position (
	position_id smallint auto_increment primary key,
    position_name varchar(50)
)collate utf8_general_ci;

create table status_setting(
	status_set_id int(3) auto_increment primary key,
	status_set_name varchar(50)
)collate utf8_general_ci;

create table privilege (
	privilege_id tinyint auto_increment primary key,
    privilege_name varchar(50) not null,
    privilege_access char(3) not null
)collate utf8_general_ci;

create table devices_priority (
	priority_id tinyint auto_increment primary key,
    priority_name varchar(50)
)collate utf8_general_ci;

create table devices_visibility (
	visibility_id tinyint auto_increment primary key,
    visibility_name varchar(50) 
)collate utf8_general_ci;

create table unit(
	unit_id tinyint auto_increment primary key,
    unit_name varchar(50)
)collate utf8_general_ci;

create table devices_status (
	status_id tinyint auto_increment primary key,
    status_name varchar(50)
)collate utf8_general_ci;

create table things_type(
	type_id tinyint auto_increment primary key,
    type_name varchar(50)       
)collate utf8_general_ci;

create table area_of_devices (
  area_of_device_id int(5) auto_increment primary key,
  area_of_device_name varchar(100) DEFAULT NULL,
  area_latitude decimal(20,15),
  area_longitudes decimal(20,15)
)collate utf8_general_ci;

create table detail_area(
	dear_id smallint zerofill auto_increment primary key,
    dear_name varchar(100) character set utf8,
    dear_image varchar(256),
    dear_area int(5),
    foreign key (dear_area) references area_of_devices(area_of_device_id)
)collate utf8_general_ci;

create table things(
	things_id int(4) zerofill auto_increment primary key,
    things_name varchar(50),
    things_ip varchar(30),
    macaddress char(17),
    things_type tinyint,
    things_offsetx decimal(10,5),
    things_offsety decimal(10,5),
    things_area smallint zerofill,
    foreign key (things_type) references things_type(type_id),
    foreign key (things_area) references detail_area(dear_id)
)collate utf8_general_ci;

create table devices(
	device_id int(4) zerofill auto_increment primary key,
    device_esp int(4) zerofill,
    device_ch tinyint,
    device_priority tinyint,
    device_visibility tinyint,
    foreign key(device_esp) references things(things_id),
    foreign key (device_priority) references devices_priority(priority_id),
	foreign key (device_visibility) references devices_visibility(visibility_id)
)collate utf8_general_ci;

create table devices_detail( 
	detail_id int(4) zerofill auto_increment primary key,
    detail_name varchar(20) character set utf8,
    detail_device int(4) zerofill,
    detail_privilege char(3),
    detail_area smallint zerofill,    
    foreign key(detail_device) references devices(device_id),
    foreign key (detail_area) references detail_area(dear_id)
)collate utf8_general_ci;

create table user (
  usr_id int(4) UNSIGNED ZEROFILL NOT NULL,
  usr_user varchar(50) DEFAULT NULL,
  usr_pass varchar(70) DEFAULT NULL
)collate utf8_general_ci;

create table user_detail(
  user_detail_id int(4) UNSIGNED ZEROFILL NOT NULL,
  user_detail_fname varchar(50) DEFAULT NULL,
  user_detail_lname varchar(50) DEFAULT NULL,
  user_detail_privilege tinyint(4) DEFAULT NULL,
  user_detail_position smallint(6) DEFAULT NULL,
  foreign key (user_detail_id) references iot_tmsm.user(usr_id),
  foreign key (user_detail_privilege) references privilege(privilege_id),
  foreign key (user_detail_position) references iot_tmsm.position(position_id)
)collate utf8_general_ci;

create table user_select_devices (
	select_id int(4)  zerofill auto_increment primary key,
	select_usr int(4)  zerofill ,
	select_device int(4)  zerofill ,
	select_status tinyint(4),
    foreign key (select_usr) references iot_tmsm.user(usr_id),
    foreign key (select_device) references devices(device_id),
    foreign key (select_status) references devices_status(status_id)
)collate utf8_general_ci;

create table devices_of_user (
	def_id int zerofill auto_increment primary key,
    def_usr int(4) zerofill,
    def_device int(4) zerofill,
    def_name varchar(50),
    def_last timestamp default current_timestamp,
    foreign key (def_usr) references iot_tmsm.user(usr_id),
    foreign key (def_device) references devices(device_id)
)collate utf8_general_ci;

create table area_administrator (
  area_admin_id int(3) NOT NULL,
  area_admin_user int(4) UNSIGNED ZEROFILL DEFAULT NULL,
  area int(5) DEFAULT NULL,
  foreign key (area) references area_of_devices (area_of_device_id),
  foreign key (area_admin_user) references  iot_tmsm.user (usr_id)
) collate utf8_general_ci;

create table timer_setting(
	timer_id int auto_increment primary key,
    timer_devices int(4) zerofill,
    timer_user int(4) zerofill,
    timer_max timestamp default current_timestamp,
    timer_min timestamp default current_timestamp,
    timer_status boolean,
    timer_access tinyint,
    timer_create timestamp default current_timestamp,
    foreign key (timer_devices)references devices(device_id),
    foreign key (timer_user) references iot_tmsm.user(usr_id),
    foreign key (timer_access) references devices_status(status_id)
)collate utf8_general_ci;

create table group_device_of_user(
	g_id smallint auto_increment primary key,
    g_name varchar(50),
    g_usr int(4) zerofill,
    foreign key (g_usr) references iot_tmsm.user(usr_id)
)collate utf8_general_ci;

create table group_device(
	gd_id smallint auto_increment primary key,
    gd_group smallint,
    gd_device int(4) zerofill,
    foreign key (gd_group) references group_device_of_user (g_id),
    foreign key (gd_device) references devices(device_id)
)collate utf8_general_ci;

create table permit_user(
	permit_id int(5) auto_increment primary key,
    permit_user int(4) zerofill,
    permit_device int(4) zerofill,
    permit_status tinyint,
    permit_privilage char(3),
    permit_grant_status tinyint,
    permit_timemin timestamp default current_timestamp,
    permit_timemax timestamp default current_timestamp,
    foreign key (permit_user) references iot_tmsm.user(usr_id),
    foreign key (permit_device) references devices (device_id),
    foreign key (permit_grant_status) references grant_status (gs_id)
)collate utf8_general_ci;

create table sensor_unit (
	sensor_unit_id int(4) zerofill auto_increment primary key,
    sensor_unit_device int(4) zerofill,
    sensor_unit tinyint,
    foreign key (sensor_unit_device) references devices(device_id),
    foreign key (sensor_unit) references unit(unit_id)
)collate utf8_general_ci;

create table sensor_value (
	sensor_id int zerofill auto_increment primary key,
    sensor_device int(4) zerofill,
    sensor_values decimal(6,2),
    sensor_last datetime,
    foreign key (sensor_device) references devices(device_id)
)collate utf8_general_ci;

create table setting_remote_sensing (
	setting_id int zerofill auto_increment primary key,
    device_io int(4) zerofill,
    device_sensor int(4) zerofill,
    setting_user int(4) zerofill,
    setting_min decimal(6,2),
    setting_max decimal(6,2),  
    setting_status tinyint,
    setting_date datetime,    
    foreign key (device_io) references devices(device_id),
    foreign key (device_sensor) references devices(device_id),
    foreign key (setting_user) references iot_tmsm.user(usr_id),
    foreign key (setting_status) references devices_status(status_id)
)collate utf8_general_ci;