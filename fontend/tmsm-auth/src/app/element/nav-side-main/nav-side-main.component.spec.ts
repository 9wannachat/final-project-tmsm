import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavSideMainComponent } from './nav-side-main.component';

describe('NavSideMainComponent', () => {
  let component: NavSideMainComponent;
  let fixture: ComponentFixture<NavSideMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavSideMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavSideMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
