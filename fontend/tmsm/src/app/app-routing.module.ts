import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManageThingsComponent } from './things/manage-things/manage-things.component';
import { ManageRightsComponent } from './user/manage-rights/manage-rights.component';
import { AcceptPermitUserComponent } from './user/accept-permit-user/accept-permit-user.component';
import { ApproveReqThingsComponent } from './user/approve-req-things/approve-req-things.component';
import { ThingsMarkerComponent } from './things/things-marker/things-marker.component';
import { AcceptDoorComponent } from './user/accept-door/accept-door.component';
import { ControllComponent } from './things/controll/controll.component';
import { ControllDoorComponent } from './things/controll-door/controll-door.component';
import { PositionComponent } from './things/position/position.component';
import { DetailpositionComponent } from './things/detailposition/detailposition.component';
import { ActpositionComponent } from './things/actposition/actposition.component';
import { ActdetailComponent } from './things/actdetail/actdetail.component';
import { ViewArchiComponent } from './things/view-archi/view-archi.component';
import { LoginComponent } from './option/login/login.component';
import { ElectrictBillComponent } from './things/electrict-bill/electrict-bill.component';
import { ProvidePositionComponent } from './user/provide-position/provide-position.component';



const routes: Routes = [
  {path : 'manage', component: ManageThingsComponent},
  {path : 'rights', component: ManageRightsComponent},
  {path : 'accept', component: AcceptPermitUserComponent},
  {path : 'approvereq', component: ApproveReqThingsComponent},
  {path : 'marker/:id', component: ThingsMarkerComponent},
  {path : 'acdoor', component: AcceptDoorComponent},
  {path : 'controlldoor', component: ControllDoorComponent},
  {path : 'position', component: ActpositionComponent,
   children: [
     {path : 'all', component: PositionComponent, pathMatch: 'full'},
     {path : 'detail', component: ActdetailComponent,
      children: [
        {path: 'all/:id', component: DetailpositionComponent, pathMatch: 'full'},
        {path: 'view/:id', component: ViewArchiComponent, pathMatch: 'full'},
        {path: 'bills/:id', component: ElectrictBillComponent, pathMatch: 'full' },
        {path: 'ctrl/:id', component: ControllComponent, pathMatch: 'full'},
      ]
    }
   ]
  },
  {path: 'login', component: LoginComponent},
  {path: 'provide', component: ProvidePositionComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
  DetailpositionComponent
]
