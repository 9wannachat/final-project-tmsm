import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ManageThingsComponent } from './things/manage-things/manage-things.component';

import { NavbarMainComponent } from './element/navbar-main/navbar-main.component';
import { SidebarMainComponent } from './element/sidebar-main/sidebar-main.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatIconModule } from '@angular/material/icon';
import { EnableDialogComponent } from './dialog/enable-dialog/enable-dialog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule, MatSortModule, MatDialogModule, MatFormFieldModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { ManageRightsComponent } from './user/manage-rights/manage-rights.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { ToastrModule, ToastContainerModule } from 'ngx-toastr';
import { OwlDateTimeModule, OwlNativeDateTimeModule, } from 'ng-pick-datetime';
import { OwlMomentDateTimeModule, OWL_MOMENT_DATE_TIME_ADAPTER_OPTIONS } from 'ng-pick-datetime-moment';
import { DatePipe } from '@angular/common';
import { AcceptPermitUserComponent } from './user/accept-permit-user/accept-permit-user.component';
import { SettimgThingsDialogComponent } from './dialog/settimg-things-dialog/settimg-things-dialog.component';
import { ApproveReqThingsComponent } from './user/approve-req-things/approve-req-things.component';
import { ThingsMarkerComponent } from './things/things-marker/things-marker.component';
import { AcceptDoorComponent } from './user/accept-door/accept-door.component';
import { ControllComponent } from './things/controll/controll.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { ControllDoorComponent } from './things/controll-door/controll-door.component';
import { PositionComponent } from './things/position/position.component';
import { MapDialogComponent } from './dialog/map-dialog/map-dialog.component';
import { AgmCoreModule } from '@agm/core';
import { DetailpositionComponent } from './things/detailposition/detailposition.component';
import { ActpositionComponent } from './things/actposition/actposition.component';
import { ActdetailComponent } from './things/actdetail/actdetail.component';
import { ViewArchiComponent } from './things/view-archi/view-archi.component';
import { ExpensDevicesComponent } from './things/expens-devices/expens-devices.component';
import { AddSubareaComponent } from './dialog/add-subarea/add-subarea.component';
import { ThingsMapComponent } from './things/things-map/things-map.component';
import { MarkerMapComponent } from './dialog/marker-map/marker-map.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatButtonModule } from '@angular/material/button';
import { LoginComponent } from './option/login/login.component';
import { SignupComponent } from './option/signup/signup.component';
import { ElectrictBillComponent } from './things/electrict-bill/electrict-bill.component';
import { ProvidePositionComponent } from './user/provide-position/provide-position.component';
import { HttpModule } from '@angular/http';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';

@NgModule({
  declarations: [
    AppComponent,
    ManageThingsComponent,
    NavbarMainComponent,
    SidebarMainComponent,
    EnableDialogComponent,
    ManageRightsComponent,
    AcceptPermitUserComponent,
    SettimgThingsDialogComponent,
    ApproveReqThingsComponent,
    ThingsMarkerComponent,
    AcceptDoorComponent,
    ControllComponent,
    ControllDoorComponent,
    PositionComponent,
    MapDialogComponent,
    DetailpositionComponent,
    ActpositionComponent,
    ActdetailComponent,
    ViewArchiComponent,
    ExpensDevicesComponent,
    AddSubareaComponent,
    ThingsMapComponent,
    MarkerMapComponent,
    MainNavComponent,
    LoginComponent,
    SignupComponent,
    ElectrictBillComponent,
    ProvidePositionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatIconModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatSortModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    NgxMaterialTimepickerModule,
    ToastrModule.forRoot(),
    ToastContainerModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    OwlMomentDateTimeModule,
    MatSlideToggleModule,
    AgmCoreModule.forRoot({
      apiKey : 'AIzaSyCIa7TFIuyxPuLF6UW6OW9nZ1Jw3s0cEZM'
    }),
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    LayoutModule,
    MatButtonModule,
    HttpModule
  ],
  providers: [
    DatePipe,
    { provide: OWL_MOMENT_DATE_TIME_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }    
  ],
  entryComponents: [
    EnableDialogComponent,
    SettimgThingsDialogComponent,
    MapDialogComponent,
    AddSubareaComponent,
    MarkerMapComponent,
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
