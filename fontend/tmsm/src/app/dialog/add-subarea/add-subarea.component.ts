import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-add-subarea',
  templateUrl: './add-subarea.component.html',
  styleUrls: ['./add-subarea.component.css']
})
export class AddSubareaComponent implements OnInit {

  public form: FormGroup;
  public fileToUpload: File = null;
  public fileName: string = 'Choose File';
  public formData: FormData = new FormData;

  constructor(private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data: any, private toastr: ToastrService, private http: HttpClient) {

    this.form = this.formBuilder.group({
      id: Number(data),
      name: [null, Validators.required],
    });
   }

  ngOnInit() {
  }

  upload(files: FileList) {
    this.fileToUpload = files.item(0);
    this.fileName = files[0].name;
  }

  onSubmit(data) {
   
    // if (data.name === "" || data.name === null) {
    //   console.log(data);
    if (data.name === "" || data.name === null || this.fileToUpload === null) {
      this.toastr.warning('กรุณาป้อนข้อมูลให้ครบถ้วน', 'แจ้งเตือน');
    } else {
      this.formData.append('images', this.fileToUpload);
      this.formData.append('areaName', data.name);
      this.formData.append('areaID', data.id);

      console.log(this.formData);

      this.http.post(environment.apiUrl + '/image', this.formData).subscribe(data => {
        console.log(data);
      });
    }
    // } else {
      
    // }
  }


}
