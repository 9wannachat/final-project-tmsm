import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-enable-dialog',
  templateUrl: './enable-dialog.component.html',
  styleUrls: ['./enable-dialog.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class EnableDialogComponent implements OnInit {

  public Id: any = null;
  public Type: any;
  public Area: any;
  public SelectArea: Array<{ id: number, name: string, detail: any }> = [];
  public Ch: Array<{ thing: any, Ch: any, nameCh: any, }> = [];
  public Subarea: Array<{ dearId: number, dearArea: number, dearImg: string, dearName: string }> = [];
  public idArea: number = null;
  public idSubArea: number = null;
  public NameAreaFromData: any;
  public NameSubArea: any;
  public key: number;
  public form: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private http: HttpClient, private toastr: ToastrService,
              private formBuilder: FormBuilder) {
    this.Id = data.data.thingID;
    this.Type = data.data.thingType;
    this.key = data.data.devices.length;
    // console.log(data.data);
    if (data.data.thingRootArea !== null && data.data.thingArea !== null) {
      this.NameAreaFromData = data.data.thingRootArea;
      this.NameSubArea = data.data.thingArea;
    }

    this.form = this.formBuilder.group({
      name: ''
    });
  }

  ngOnInit() {
    this.makeChannel();
    this.getData();
  }

  makeChannel() {
    if (this.Type !== 4 && this.Type !== 5) {
      const makeCh = this.Type - this.key;
      for (let i = 1; i <= makeCh; i++) {
        this.Ch.push({ thing: this.Id, Ch: i, nameCh: i });
      }
    }
  }

  getData() {
    this.http.get(environment.apiUrl + '/allarea').subscribe(data => {
      const feedData = JSON.parse(JSON.stringify(data));
      this.Area = feedData.areaDevices;
      // console.log(this.Area);
      const key = this.Area.length;
      for (let i = 0; i < key; i++) {
        if (this.NameAreaFromData === this.Area[i].areaName) {
          this.NameAreaFromData = this.Area[i].areaId;
        }
        this.SelectArea.push({ id: this.Area[i].areaId, name: this.Area[i].areaName, detail: this.Area[i].detailAreas });
        const subkey = this.Area[i].detailAreas.length;
        for (let j = 0; j < subkey; j++) {
          if (this.Area[i].detailAreas[j].dearName === this.NameSubArea) {
            this.NameSubArea = this.Area[i].detailAreas[j].dearId;
          }

          if (this.Area[i].detailAreas[j].dearArea === this.NameAreaFromData) {
            // console.log(this.Area[i].detailAreas[j]);
            this.Subarea.push({
              dearId: this.Area[i].detailAreas[j].dearId, dearArea: this.Area[i].detailAreas[j].dearArea,
              dearImg: this.Area[i].detailAreas[j].dearImg, dearName: this.Area[i].detailAreas[j].dearName
            });
          }
        }
      }
    });
  }

  getArea(id, list) {
    this.idArea = id;
    this.Subarea = list;
  }

  getSubarea(id) {
    this.idSubArea = id;
  }

  createDevices(a, b) {
    // const body = {
    //   thing : 148,
    //   ch : 1,
    //   priority: 1,
    //   visibility: 1,
    //   nameDevice: 'Chanel 1',
    //   privilege: '111',
    //   area: 2
    // };
  }

  toat() {

    if (this.idSubArea !== null) {
      const body = {
        thingId: this.Id,
        areaId: this.idSubArea
      };

      console.log(body);
      this.http.put(environment.apiUrl + '/credevices/updatearea', body).subscribe(response => {
        const data = JSON.parse(JSON.stringify(response));
        console.log(data);
        if (data.codeStatus === 200) {
          this.toastr.success('อัพเดทสถานที่ของสรรพสิ่งสำเร็จ', 'แจ้งเตือน');
        } else {
          this.toastr.error('อัพเดทสถานที่ของสรรพสิ่งไม่สำเร็จ', 'แจ้งเตือน');
        }
      });
    } else {
      this.toastr.warning('กรุณาเลือกสถานให้ครบถ้วน', 'แจ้งเตือน');
    }
  }

}
