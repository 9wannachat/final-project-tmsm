import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-map-dialog',
  templateUrl: './map-dialog.component.html',
  styleUrls: ['./map-dialog.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MapDialogComponent implements OnInit {

  lat = 7.201208782914364;
  lng = 100.6027169158832;
  zoom = 16;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { 
    // console.log(data.data);
    this.lat = data.data.areaLat;
    this.lng = data.data.areaLng;
  }

  ngOnInit() {
  }

}
