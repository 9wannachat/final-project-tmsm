import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-marker-map',
  templateUrl: './marker-map.component.html',
  styleUrls: ['./marker-map.component.css']
})
export class MarkerMapComponent implements OnInit {

  lat = 7.201208782914364;
  lng = 100.6027169158832;
  zoom = 16;

  constructor() { }

  ngOnInit() {
  }

}
