import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettimgThingsDialogComponent } from './settimg-things-dialog.component';

describe('SettimgThingsDialogComponent', () => {
  let component: SettimgThingsDialogComponent;
  let fixture: ComponentFixture<SettimgThingsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettimgThingsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettimgThingsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
