import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-settimg-things-dialog',
  templateUrl: './settimg-things-dialog.component.html',
  styleUrls: ['./settimg-things-dialog.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SettimgThingsDialogComponent implements OnInit {

  public Id: any;
  public priority: any;
  public visibility: any;
  public devices: any;
  public valPriority: any;
  public valVisibilty: any;
  public nameDevice: string = null;
  public privilage: string = null;
  public Ch: number;
  public Create: number;

  public read = 0;
  public write = 0;
  public execute = 0;
  public form: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private http: HttpClient,
    private formBuilder: FormBuilder, private toastr: ToastrService,
    private matdialogRef: MatDialogRef<SettimgThingsDialogComponent>) {
    console.log(data.data);
    this.Id = data.data.thingID;
    this.devices = data.data.devices;

    if (this.devices.length !== data.data.thingType) {
      if (data.data.thingType === 1) {
        this.devices.push({
          detailName: 'Chanel 1', deviceCH: 1,
          devicePriority: 1, deviceVisibility: 1, create: 1, detailPrivilege: '111', deviceThing: this.Id
        });
      } else if (data.data.thingType === 2) {
        if (this.devices.length === 1) {
          if (this.devices[0].deviceCH === 1) {
            this.devices.push({
              deviceID: 1, detailName: 'Chanel 2', deviceCH: 2, devicePriority: 1,
              deviceVisibility: 1, create: 1, detailPrivilege: '111', deviceThing: this.Id
            });
          } else {
            this.devices.push({
              deviceID: 1, detailName: 'Chanel 1', deviceCH: 1,
              devicePriority: 1, deviceVisibility: 1, detailPrivilege: '111', deviceThing: this.Id
            });
          }
        } else {
          this.devices.push({
            deviceID: 1, detailName: 'Chanel 1', deviceCH: 1,
            devicePriority: 1, deviceVisibility: 1, create: 1, detailPrivilege: '111', deviceThing: this.Id
          },
            {
              deviceID: 1, detailName: 'Chanel 2', deviceCH: 2, devicePriority: 1,
              deviceVisibility: 1, create: 1, detailPrivilege: '111', deviceThing: this.Id
            });
        }
      }
    }

    this.form = this.formBuilder.group({
      deviceName: ['']
    });
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.http.get(environment.apiUrl + '/optdevice').subscribe(response => {
      const data = JSON.parse(JSON.stringify(response));
      this.priority = data.priorities;
      this.visibility = data.visibilities;
    });
  }

  selectDevice(ch) {
    const key = this.devices.length;
    this.Ch = ch;
    for (let i = 0; i < key; i++) {
      if (ch === this.devices[i].deviceCH) {
        this.privilage = this.devices[i].detailPrivilege;
        this.valPriority = this.devices[i].devicePriority;
        this.valVisibilty = this.devices[i].deviceVisibility;
        this.form = this.formBuilder.group({
          deviceName: this.devices[i].detailName
        });

        if (this.devices[i].create === 1) {
          this.Create = 1;
        }
      }
    }

  }

  selectRead(ch) {
    if (ch.checked === true) {
      this.read = 1;
    } else if (ch.checked === false) {
      this.read = 0;
    }
    this.privilage = this.read + '' + this.write + '' + this.execute;
  }

  selectWrite(ch) {
    if (ch.checked === true) {
      this.write = 1;
    } else if (ch.checked === false) {
      this.write = 0;
    }
    this.privilage = this.read + '' + this.write + '' + this.execute;
  }

  selectExec(ch) {
    if (ch.checked === true) {
      this.execute = 1;
    } else if (ch.checked === false) {
      this.execute = 0;
    }
    this.privilage = this.read + '' + this.write + '' + this.execute;
  }

  selectPriority(id) {
    this.valPriority = id;
  }

  selectVisibility(id) {
    this.valVisibilty = id;
  }

  updateData() {
    // console.log(this.Create);

    if (this.Create === 1) {

      const body = {
        thing: this.Id,
        ch: this.Ch,
        priority: this.valPriority,
        visibility: this.valVisibilty,
        nameDevice: this.form.value.deviceName,
        privilege: this.privilage,
        area: 2
      };

      // console.log(body);
      this.http.post(environment.apiUrl + '/credevices', body).subscribe(response => {
        console.log(response);
        const data = JSON.parse(JSON.stringify(response));
        if (data.codeStatus === 200) {
            this.toastr.success('สร้างอุปกรณ์ของสรรพสิ่งที่ ' + this.Id + ' ช่องที่ ' + this.Ch + ' สำเร็จ', 'แจ้งเตือน');
        } else {
          this.toastr.error('สร้างอุปกรณ์ของสรรพสิ่งที่ ' + this.Id + ' ช่องที่ ' + this.Ch + ' สำเร็จ', 'แจ้งเตือน');
        }
      });

    } else {
      const body = {
        thing: this.Id,
        ch: this.Ch,
        priority: this.valPriority,
        visibility: this.valVisibilty,
        nameDevice: this.form.value.deviceName,
        privilege: this.privilage
      };

      // console.log(body);
      this.http.put(environment.apiUrl + '/optdevice', body).subscribe(response => {
        const data = JSON.parse(JSON.stringify(response));
        if (data.codeStatus === 200) {
          this.toastr.success('อัพเดทข้อมูลของสรรพที่ ' + this.Id + ' ช่องที่ ' + this.Ch + ' สำเร็จ', 'แจ้งเตือน');
        } else {
          this.toastr.error('อัพเดทข้อมูลของสรรพที่ ' + this.Id + ' ช่องที่ ' + this.Ch + ' ไม่สำเร็จ', 'เกิดข้อผิดพลาด');
        }
      });
    }
  }

  closeDialog() {
    this.matdialogRef.close();
  }

}
