import { HttpClient } from '@angular/common/http';

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';
import { sha256 } from 'js-sha256';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, RequestOptions } from '@angular/http';
import { Headers } from '@angular/http';
import { AuthenticationService } from 'src/app/_services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public form: FormGroup;
  returnUrl: string;
  error = '';
  loading = false;
  submitted = false;

  constructor(private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient,
    private authentication : AuthenticationService
  ) {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    if (this.authentication.currentUserValue) {
      this.router.navigate(['/']);
    }

    this.authentication.logout();

  }

  ngOnInit() {
    // this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    // var headers_object = new HttpHeaders();
    // headers_object.append('Content-Type', 'application/json');
    // headers_object.append("Authorization", "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwb20iLCJleHAiOjE1NzEwODA5NDQsImlhdCI6MTU3MTA2Mjk0NH0.vwUTcVaAOOvXsx7IgzS0nloL0bol45y1YvJzK_4hO7RbMTA5OnNgMx4YbvIete7k_5B0LMm4h8_MkzkcbT-j1Q");

    // const httpOptions = {
    //   headers: headers_object
    // };
   
  }

  authentiate(value) {
    this.submitted = true;

    // stop here if form is invalid
    if (this.form.invalid) {
      this.toastr.warning('กรุณาป้อนข้อมูลให้ครบถ้วน', 'แจ้งเตือน');
      return;
    }

    this.loading = true;
    this.authentication.login(value.username, sha256(value.password))
        .pipe(first())
        .subscribe(
            data => {
              console.log(data)
              // this.router.navigate([this.returnUrl]);
            },
            error => {
                this.error = error;
                this.loading = false;
            });
  }

}
