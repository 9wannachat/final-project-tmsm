import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActpositionComponent } from './actposition.component';

describe('ActpositionComponent', () => {
  let component: ActpositionComponent;
  let fixture: ComponentFixture<ActpositionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActpositionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActpositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
