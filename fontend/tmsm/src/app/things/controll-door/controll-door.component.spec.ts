import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControllDoorComponent } from './controll-door.component';

describe('ControllDoorComponent', () => {
  let component: ControllDoorComponent;
  let fixture: ComponentFixture<ControllDoorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControllDoorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControllDoorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
