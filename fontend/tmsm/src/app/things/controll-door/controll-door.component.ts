import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-controll-door',
  templateUrl: './controll-door.component.html',
  styleUrls: ['./controll-door.component.css']
})
export class ControllDoorComponent implements OnInit {
  public interval: any;
  public Status = 'N/A';
  constructor(private http: HttpClient) { }

  ngOnInit() {
   this.feedBack();
  //  this.startTimer();
  }

  // startTimer() {
  //   this.interval = setInterval(() => {
  //    this.feedBack();
  //   }, 3000);
  // }

  feedBack() {
    // this.http.get('http://localhost:3333/feedbackdoor').subscribe(res => {
    //   console.log(res);
    //   const raw = JSON.parse(JSON.stringify(res));
    //   console.log(raw.status);
    //   if (raw.status === 0) {
    //     this.Status = 'ประตูกำลังปิด';
    //   } else if(raw.status === 1) {
    //     this.Status = 'ประตูกำลังเปิด';
    //   }
    // });
  }

  doorController(status) {
    console.log(status);
    this.http.get('http://localhost:3333/ctrldoor/' + status + '/' + 3).subscribe(res => {
      console.log(res);
    })
  }

}
