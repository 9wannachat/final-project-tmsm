import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-controll',
  templateUrl: './controll.component.html',
  styleUrls: ['./controll.component.css']
})
export class ControllComponent implements OnInit {

  public data: any;
  public mkData: Array<{}> = [];
  public status;
  public device;
  public ch;
  public things;
  public userId;
  public setStatus;
  public Id;

  constructor(private http: HttpClient, private route: ActivatedRoute) { 
    this.Id = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.http.get(environment.apiUrl + '/webctrl/' + this.Id).subscribe(response => {
      
      this.data = JSON.parse(JSON.stringify(response));
      console.log(this.data.devices[0]);
      this.data = this.data.devices;
      const objKey = this.data.length;
      console.log(objKey);
      for (let i = 0; i < objKey; i++) {
        console.log(this.data[i]);
        const realSt = this.data[i].status[0].status;
        if (realSt === 2 || realSt === 4 || realSt === 0) {
          this.setStatus = false;
        } else {
          this.setStatus = true;
        }
        this.mkData.push({
          deviceCH: this.data[i].deviceCH,
          deviceID: this.data[i].deviceID,
          deviceName: this.data[i].deviceName,
          devicePriority: this.data[i].devicePriority,
          devicePrivilege: this.data[i].devicePrivilege,
          deviceThing: this.data[i].deviceThing,
          deviceVisibility: this.data[i].deviceVisibility,
          deviceStatus: this.setStatus
        });
      }

      console.log(this.mkData);
    });
  }

  on(event, data) {
    console.log(event.checked);


    if (event.checked === true) {
      this.status = 1;
      this.device = data.deviceID;
      this.ch = data.deviceCH;
      this.things = data.deviceThing;
      this. userId = 3;
    } else {
      this.status = 0;
      this.device = data.deviceID;
      this.ch = data.deviceCH;
      this.things = data.deviceThing;
      this.userId = 3;
    }

    this.http.get('http://localhost:3333/control/' + this.status + '/' + this.device + '/' + this.ch + '/' + this.things + '/' + this.userId).subscribe(res => {
      console.log(res);
    })
  }

}
