import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailpositionComponent } from './detailposition.component';

describe('DetailpositionComponent', () => {
  let component: DetailpositionComponent;
  let fixture: ComponentFixture<DetailpositionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailpositionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailpositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
