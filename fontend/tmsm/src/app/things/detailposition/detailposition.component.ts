import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { AddSubareaComponent } from 'src/app/dialog/add-subarea/add-subarea.component';

@Component({
  selector: 'app-detailposition',
  templateUrl: './detailposition.component.html',
  styleUrls: ['./detailposition.component.css']
})
export class DetailpositionComponent implements OnInit {

  public data: any;
  public rawData: any;
  public Id: any;

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router, private dialog : MatDialog) { 
    this.Id = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.http.get(environment.apiUrl + '/allarea').subscribe(res => {
      // console.log(res);
      const raw = JSON.parse(JSON.stringify(res));
      this.rawData = raw.areaDevices;
      const objKey = this.rawData.length;
      for (let i = 0; i < objKey; i++) {
        
        if (this.rawData[i].areaId === Number(this.Id)) {
          console.log(this.rawData[i].detailAreas);
          this.data = this.rawData[i].detailAreas;
        }
      }      

    });
  }

  redirect(data) {
    this.router.navigate(['../../../../position/detail/bills/' + data], {relativeTo: this.route})
  }

  ctrlDirect(data) {
    this.router.navigate(['../../../../position/detail/ctrl/' + data], {relativeTo: this.route})
  }

  viewMap(data) {
    this.router.navigate(['../../view/' + data], {relativeTo: this.route})
  }

  addArea() {
    this.dialog.open(AddSubareaComponent,  {
      width: '600px', height: '350px', data: this.Id
    });
  }
}
