import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectrictBillComponent } from './electrict-bill.component';

describe('ElectrictBillComponent', () => {
  let component: ElectrictBillComponent;
  let fixture: ComponentFixture<ElectrictBillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElectrictBillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElectrictBillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
