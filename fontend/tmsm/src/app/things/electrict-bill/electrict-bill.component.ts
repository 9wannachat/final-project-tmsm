import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-electrict-bill',
  templateUrl: './electrict-bill.component.html',
  styleUrls: ['./electrict-bill.component.css']
})
export class ElectrictBillComponent implements OnInit {

  public _Id: any;
  public data: any;

  constructor(private http: HttpClient, private route: ActivatedRoute) {
    this._Id = this.route.snapshot.paramMap.get('id');
   }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.http.get(environment.apiUrl + '/electric/' + this._Id).subscribe(res => {
      const raw = JSON.parse(JSON.stringify(res));
      this.data = raw.bills;
    });
  }

}
