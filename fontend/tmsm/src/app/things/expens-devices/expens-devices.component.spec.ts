import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpensDevicesComponent } from './expens-devices.component';

describe('ExpensDevicesComponent', () => {
  let component: ExpensDevicesComponent;
  let fixture: ComponentFixture<ExpensDevicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpensDevicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpensDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
