import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { MatDialog } from '@angular/material';
import { EnableDialogComponent } from 'src/app/dialog/enable-dialog/enable-dialog.component';
import { SettimgThingsDialogComponent } from 'src/app/dialog/settimg-things-dialog/settimg-things-dialog.component';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-manage-things',
  templateUrl: './manage-things.component.html',
  styleUrls: ['./manage-things.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ManageThingsComponent implements OnInit {

  public thingsObject: any;

  constructor(private http: HttpClient, 
              private dialog: MatDialog, 
              private route: Router, 
              private toastr: ToastrService,
              ) { }

  ngOnInit() {
    this.getData();
  }

  getData() {

    this.http.get(environment.apiUrl + '/mobile/things').subscribe(response => {
      const getThings = JSON.parse(JSON.stringify(response));
      this.thingsObject = getThings.things;
      console.log(this.thingsObject);
    });
    
    
    
  }

  dialogEnable(data) {
    this.dialog.open(EnableDialogComponent, {
      width: '600px', height: '450px', data: {data}
    });
  }

  dialogSetting(data) {
    this.dialog.open(SettimgThingsDialogComponent, {
      width: '700px', height: '330px', data: {data}
    });
  }

  directMarker(data) {
    if (data.thingArea === null || data.thingArea === '') {
      this.toastr.info('กรุณาตั้งค่าที่อยู่ของสรรพสิ่งให้สมบูรณ์', 'แจ้งเตือน');
      this.dialog.open(EnableDialogComponent, {
        width: '600px', height: '450px', data: {data}
      });
    } else {
        this.route.navigateByUrl('/marker/' + data.thingID);
    }
  }
}
