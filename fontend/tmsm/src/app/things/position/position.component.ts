import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { MatDialog } from '@angular/material';
import { MapDialogComponent } from 'src/app/dialog/map-dialog/map-dialog.component';
import { Router, ActivatedRoute } from '@angular/router';
import { MarkerMapComponent } from 'src/app/dialog/marker-map/marker-map.component';

@Component({
  selector: 'app-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.component.css']
})
export class PositionComponent implements OnInit {

  public data: any;

  constructor(private http: HttpClient,  private dialog: MatDialog, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.http.get(environment.apiUrl + '/allarea').subscribe(res => {
      // console.log(res);
      const raw = JSON.parse(JSON.stringify(res));
      this.data = raw.areaDevices;
    });
  }

  viewMap(data){
    console.log(data);
    this.dialog.open(MapDialogComponent, {
      width: '600px', height: '450px', data: {data}
    });
  }

  MarkerMap() {
    this.dialog.open(MarkerMapComponent, {
      width: '1000px', height: '600px'
    });
  }

  redirect(id) {
    this.router.navigate(['../detail/all/' + id], {relativeTo: this.route});
  }

}
