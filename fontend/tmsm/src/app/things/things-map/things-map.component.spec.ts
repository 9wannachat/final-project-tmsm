import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThingsMapComponent } from './things-map.component';

describe('ThingsMapComponent', () => {
  let component: ThingsMapComponent;
  let fixture: ComponentFixture<ThingsMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThingsMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThingsMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
