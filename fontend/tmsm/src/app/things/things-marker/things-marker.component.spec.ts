import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThingsMarkerComponent } from './things-marker.component';

describe('ThingsMarkerComponent', () => {
  let component: ThingsMarkerComponent;
  let fixture: ComponentFixture<ThingsMarkerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThingsMarkerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThingsMarkerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
