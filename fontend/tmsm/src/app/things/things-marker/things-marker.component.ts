import { Component, OnInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-things-marker',
  templateUrl: './things-marker.component.html',
  styleUrls: ['./things-marker.component.css']
})
export class ThingsMarkerComponent implements OnInit {

  @ViewChild('canvas', { static: false }) canvasEl: ElementRef;
  private scrHeight: any;
  private scrWidth: any;
  private _CANVAS: any;
  private _CONTEXT: any;
  public image: string;


  screenW: number;
  screenH: number;

  private ID: string;

  constructor(private http: HttpClient, private route: ActivatedRoute, private tostr: ToastrService) {
    this.ID = this.route.snapshot.paramMap.get('id');
    console.log(this.ID);
    this.getScreenSize();
  }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;
    // console.log(this.scrHeight, this.scrWidth);
  }

  ngOnInit() {
    this.canvasInit();
  }

  getOffset(e) {
    const detailOffSetX = e.offsetX * (1024 / this._CONTEXT.width);
    const detailOffSetY = e.offsetY * (2048 / this.screenH);

    const body = {
      thingsOffsetX : detailOffSetX,
      thingsOffsetY: detailOffSetY,
      thingsID: Number(this.ID)
    };

    this.http.put(environment.apiUrl + '/area', body).subscribe(data => {
      const res = JSON.parse(JSON.stringify(data));
      if (res.codeStatus === 200) {
        this.tostr.success('อัพเดทข้อมูลสำเร็จ', 'แจ้งเตือน');

        this.getPoint();
      } else {
        this.tostr.error('อัพเดทข้อมูลไม่สำเร็จ', 'แจ้งเตือน');
      }
    });


  }

  canvasInit() {

    this.http.get(environment.apiUrl + '/area/position').subscribe(data => {
      const stringData = JSON.stringify(data);
      const jsonToObj = JSON.parse(stringData);
      console.log(data);
      const countArray = jsonToObj.datas.length;

      for (let i = 0; i < countArray; i++) {
        if (jsonToObj.datas[i].thingsID === Number(this.ID)) {
          this.image = jsonToObj.datas[i].devices[0].image;
        }
      }


      this._CANVAS = (this.canvasEl.nativeElement as HTMLCanvasElement).getContext('2d');

      const img = new Image();
      img.src = environment.imgsUrl + this.image;

      this.screenW = this.scrWidth;
      this.screenH = this.scrHeight - ((10 / 100) * this.scrHeight);

      img.onload = () => {
        this._CONTEXT = this.canvasEl.nativeElement;
        this._CONTEXT.height = this.screenH;
        this._CONTEXT.width = this._CONTEXT.height * 0.5;

        this._CANVAS.drawImage(img, 0, 0, this.screenH * 0.5, this.screenH);

        for (let i = 0; i < countArray; i++) {
          if (jsonToObj.datas[i].thingsID === Number(this.ID) && jsonToObj.datas[i].thingsOffsetX != null) {
            const offsetX = jsonToObj.datas[i].thingsOffsetX / (1024 / this._CONTEXT.width);
            const offsetY = jsonToObj.datas[i].thingsOffsetY / (2048 / this.screenH);
            if (offsetX !== 0 && offsetY !== 0) {
              this.drawRec(offsetX, offsetY);
            }
          }
        }
      };

    });

  }

  drawRec(offsetX, offsetY) {
    this._CANVAS.beginPath();
    this._CANVAS.save();

    const greenColor = '#07DA47';

    this._CANVAS.fillStyle = greenColor;
    this._CANVAS.arc(offsetX, offsetY, 10, 0, 2 * Math.PI);
    this._CANVAS.fill();
  }

  getPoint() {
    this.http.get(environment.apiUrl + '/area/position').subscribe(data => {

      const stringData = JSON.stringify(data);
      const jsonToObj = JSON.parse(stringData);
      const countArray = jsonToObj.datas.length;

      for (let i = 0; i < countArray; i++) {
        if (jsonToObj.datas[i].thingsID === Number(this.ID) && jsonToObj.datas[i].thingsOffsetX !== null) {

          const offsetX = jsonToObj.datas[i].thingsOffsetX / (1024 / this._CONTEXT.width);;
          const offsetY = jsonToObj.datas[i].thingsOffsetY / (2048 / this.screenH);
          this.image = jsonToObj.datas[i].devices[0].image;

          this.drawRec(offsetX, offsetY);
        }
      }

    });
  }

}
