import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewArchiComponent } from './view-archi.component';

describe('ViewArchiComponent', () => {
  let component: ViewArchiComponent;
  let fixture: ComponentFixture<ViewArchiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewArchiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewArchiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
