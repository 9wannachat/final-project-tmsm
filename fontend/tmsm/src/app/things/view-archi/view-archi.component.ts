import { Component, OnInit, ElementRef, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-view-archi',
  templateUrl: './view-archi.component.html',
  styleUrls: ['./view-archi.component.css']
})
export class ViewArchiComponent implements OnInit {

  public Id: any;
  public img: any;

  @ViewChild('canvas', { static: false }) canvasEl: ElementRef;
  private scrHeight: any;
  private scrWidth: any;
  private _CANVAS: any;
  private _CONTEXT: any;
  public image: string;


  screenW: number;
  screenH: number;

  private ID: string;

  constructor(private route: ActivatedRoute, private http: HttpClient) { 
      this.Id = this.route.snapshot.paramMap.get('id');
      this.getScreenSize();
  }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;
    // console.log(this.scrHeight, this.scrWidth);
  }

  ngOnInit() {
    this.canvasInit();
  }

  getData() {
    this.http.get(environment.apiUrl + '/allarea/detail/' + this.Id).subscribe(res => {
      console.log(res);
      const data = JSON.parse(JSON.stringify(res));
      this.image = data.dearImg;
    });
  }

  canvasInit() {

    this.http.get(environment.apiUrl + '/allarea/detail/' + this.Id).subscribe(data => {
      const stringData = JSON.stringify(data);
      const jsonToObj = JSON.parse(stringData);
      console.log(data);
      const countArray = jsonToObj.things.length;
      console.log(countArray);
      this.image = jsonToObj.dearImg;


      this._CANVAS = (this.canvasEl.nativeElement as HTMLCanvasElement).getContext('2d');

      const img = new Image();
      img.src = environment.imgsUrl + this.image;

      this.screenW = this.scrWidth;
      this.screenH = this.scrHeight - ((10 / 100) * this.scrHeight);

      img.onload = () => {
        this._CONTEXT = this.canvasEl.nativeElement;
        this._CONTEXT.height = this.screenH;
        this._CONTEXT.width = this._CONTEXT.height * 0.5;

        this._CANVAS.drawImage(img, 0, 0, this.screenH * 0.5, this.screenH);

        for (let i = 0; i < countArray; i++) {
          if (jsonToObj.things[i].thingsOffsetX != null) {
              const offsetX = jsonToObj.things[i].thingsOffsetX / (1024 / this._CONTEXT.width);
              const offsetY = jsonToObj.things[i].thingsOffsetY / (2048 / this.screenH);
              if (offsetX !== 0 && offsetY !== 0) {
                console.log(offsetX + "" + offsetY);
                this.drawRec(offsetX, offsetY);
              }
          }
        }
      };

    });

  }

  drawRec(offsetX, offsetY) {
    this._CANVAS.beginPath();
    this._CANVAS.save();

    const greenColor = '#07DA47';

    this._CANVAS.fillStyle = greenColor;
    this._CANVAS.arc(offsetX, offsetY, 10, 0, 2 * Math.PI);
    this._CANVAS.fill();
  }

  getPoint() {
    this.http.get(environment.apiUrl + '/area/position').subscribe(data => {

      const stringData = JSON.stringify(data);
      const jsonToObj = JSON.parse(stringData);
      const countArray = jsonToObj.datas.length;

      for (let i = 0; i < countArray; i++) {
        if (jsonToObj.datas[i].thingsID === Number(this.ID) && jsonToObj.datas[i].thingsOffsetX !== null) {

          const offsetX = jsonToObj.datas[i].thingsOffsetX / (1024 / this._CONTEXT.width);;
          const offsetY = jsonToObj.datas[i].thingsOffsetY / (2048 / this.screenH);
          this.image = jsonToObj.datas[i].devices[0].image;

          this.drawRec(offsetX, offsetY);
        }
      }

    });
  }

  getDataThings(event) {
    // console.log(event);
  }

}
