import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptDoorComponent } from './accept-door.component';

describe('AcceptDoorComponent', () => {
  let component: AcceptDoorComponent;
  let fixture: ComponentFixture<AcceptDoorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcceptDoorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptDoorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
