import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-accept-door',
  templateUrl: './accept-door.component.html',
  styleUrls: ['./accept-door.component.css']
})
export class AcceptDoorComponent implements OnInit {

  public data: any;

  constructor(private http: HttpClient, private toastr: ToastrService) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.http.get(environment.apiUrl + '/door').subscribe(response => {

      const raw = JSON.parse(JSON.stringify(response));
      this.data = raw.door;
      // console.log(this.data);
    });
  }

  postData(usr) {
    const body = {
      userID: usr,
      status: 1
    };

    this.http.put(environment.apiUrl + '/door', body).subscribe(response => {
      const raw = JSON.parse(JSON.stringify(response));
      console.log(response);
      if (raw.codeStatus === 200) {
        this.toastr.success('ยืนยันสิทธิ์การใช้งานประตูสำเร็จ', 'แจ้งเตือน');
      } else {
        this.toastr.error('เกิดข้อผิดพลาดบางประการ', 'เเจ้งเตือน');
      }
    });
  }

}
