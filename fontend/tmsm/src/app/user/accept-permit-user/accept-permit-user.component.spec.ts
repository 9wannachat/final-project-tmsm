import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptPermitUserComponent } from './accept-permit-user.component';

describe('AcceptPermitUserComponent', () => {
  let component: AcceptPermitUserComponent;
  let fixture: ComponentFixture<AcceptPermitUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcceptPermitUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptPermitUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
