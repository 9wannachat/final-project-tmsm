import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-accept-permit-user',
  templateUrl: './accept-permit-user.component.html',
  styleUrls: ['./accept-permit-user.component.css']
})
export class AcceptPermitUserComponent implements OnInit {

  public dataPermition: any;

  constructor(private http: HttpClient, private tostr: ToastrService) { }

  ngOnInit() {
    this.getDatas();
  }

  getDatas() {
    this.http.get(environment.apiUrl + '/reqdevices/permit').subscribe(response => {
      const data = JSON.parse(JSON.stringify(response));
      this.dataPermition = data.data;
      console.log(data);
    });
  }

  postData(id, fname, lname) {
    const body = {
      permitID: id,
      permitTimeMin: '2019-08-11 21:45:49.0',
      permitTimeMax: '2019-08-11 21:45:49.0',
      permitStatus: 1,
      permitGrantStatus: 1
    };

    this.http.post(environment.apiUrl + '/reqdevices/permition', body).subscribe(response => {
      const data = JSON.parse(JSON.stringify(response));
      // console.log(data);
      if (data.codeStatus === 200) {
          this.tostr.success('อนุมัติสิทธิ์ คุณ ' + fname + ' ' + lname + ' สำเร็จ', 'แจ้งเตือน');
          this.getDatas();
      } else {
        this.tostr.error('อนุมัติสิทธิ์ คุณ ' + fname + ' ' + lname + ' ไม่สำเร็จสำเร็จ', 'แจ้งเตือน');
      }
    });

  }

}
