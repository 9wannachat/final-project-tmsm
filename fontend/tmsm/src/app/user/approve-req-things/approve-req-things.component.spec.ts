import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveReqThingsComponent } from './approve-req-things.component';

describe('ApproveReqThingsComponent', () => {
  let component: ApproveReqThingsComponent;
  let fixture: ComponentFixture<ApproveReqThingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveReqThingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveReqThingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
