import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-approve-req-things',
  templateUrl: './approve-req-things.component.html',
  styleUrls: ['./approve-req-things.component.css']
})
export class ApproveReqThingsComponent implements OnInit {

  public userApprove: any;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.http.get(environment.apiUrl + '/reqdevices/permit').subscribe(response => {
      const rawData = JSON.parse(JSON.stringify(response));
      this.userApprove = rawData.data;
      console.log(rawData);
    });
  }

  approve(list) {

  }
}
