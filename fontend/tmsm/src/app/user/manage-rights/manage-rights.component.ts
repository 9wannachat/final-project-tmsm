import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { DateTimeAdapter } from 'ng-pick-datetime';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-manage-rights',
  templateUrl: './manage-rights.component.html',
  styleUrls: ['./manage-rights.component.css']
})
export class ManageRightsComponent implements OnInit {

  public data: any;
  public grant: any;
  public selected: any;
  public permit: any = null;
  public form: FormGroup;
  public selectedMoment: any = null;
  public DateTimeSt: any;
  public DateTimeEn: any;
  public Status: Array<{ id: number, name: string }> = [
    { id: 1, name: 'อนุมัติ' },
    { id: 0, name: 'ไม่อนุมัติ' }
  ];

  public stUpdate: any = null;

  constructor(private http: HttpClient, private dateTimeAdapter: DateTimeAdapter<any>,
              private datePipe: DatePipe, private toastr: ToastrService) {
    this.dateTimeAdapter.setLocale('th-THA');
  }

  updateStatus(id) {
    this.stUpdate = id;
  }

  ngOnInit() {
    this.getData();
    this.getGrantData();
  }

  getData() {
    this.http.get(environment.apiUrl + '/reqdevices/permit/allow').subscribe(response => {
      const rawData = JSON.parse(JSON.stringify(response));
      this.data = rawData.data;
    });
  }

  getGrantData() {
    this.http.get(environment.apiUrl + '/grantstatus').subscribe(response => {
      const rawData = JSON.parse(JSON.stringify(response));
      this.grant = rawData.grantStatus;
      this.selected = 1;
    });
  }

  setPermit(id) {
    this.permit = id;
  }

  setTime(e) {
    console.log(e);
  }

  updateData(id, status, grant, oldSt, oldEn, name, device) {

    if (this.selectedMoment !== null) {
      const stTm = this.selectedMoment[0]._i;
      const endTm = this.selectedMoment[1]._i;
      const mSt = stTm.month + 1;
      const mEn = endTm.month + 1;
      const fullSt = stTm.year + '-' + mSt + '-' + stTm.date;
      const fullEn = endTm.year + '-' + mEn + '-' + endTm.date;
      const DateSt = this.datePipe.transform(fullSt, 'yyyy-MM-dd');
      const DateEn = this.datePipe.transform(fullEn, 'yyyy-MM-dd');

      this.DateTimeSt = DateSt + ' ' + stTm.hours + ':' + stTm.minutes;
      this.DateTimeEn = DateEn + ' ' + endTm.hours + ':' + endTm.minutes;
      this.DateTimeSt = this.datePipe.transform(this.DateTimeSt, 'yyyy-MM-dd hh:mm');
      this.DateTimeEn = this.datePipe.transform(this.DateTimeEn, 'yyyy-MM-dd hh:mm');

    } else {
      this.DateTimeSt = oldSt;
      this.DateTimeEn = oldEn;
    }

    if (this.permit === null) {
      this.permit = grant;
    }

    if (this.stUpdate === null) {
      this.stUpdate = status;
    }

    const body = {
      permitID: id,
      permitTimeMin: this.DateTimeSt,
      permitTimeMax: this.DateTimeEn,
      permitStatus: this.stUpdate,
      permitGrantStatus: this.permit
    };


    this.http.post(environment.apiUrl + '/reqdevices/permition', body).subscribe(response => {
      const data = JSON.parse(JSON.stringify(response));
      if (data.codeStatus === 200) {
          this.toastr.success('แจ้งเตือน', 'อัพเดทสิทธิ์ของ ' + name + ' ที่อุปกรณ์ ' + device + ' สำเร็จ');
      } else {
        this.toastr.error('แจ้งเตือน', 'อัพเดทสิทธิ์ของ ' + name + ' ที่อุปกรณ์ ' + device + ' ไม่สำเร็จ');
      }
    });
  }

}
