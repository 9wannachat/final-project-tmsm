import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvidePositionComponent } from './provide-position.component';

describe('ProvidePositionComponent', () => {
  let component: ProvidePositionComponent;
  let fixture: ComponentFixture<ProvidePositionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvidePositionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvidePositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
