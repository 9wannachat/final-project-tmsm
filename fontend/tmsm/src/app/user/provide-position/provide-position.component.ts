import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-provide-position',
  templateUrl: './provide-position.component.html',
  styleUrls: ['./provide-position.component.css']
})
export class ProvidePositionComponent implements OnInit {

  public data: any;
  public positions: any;
  public privilaeges: any;

  constructor(private http: HttpClient, private toatsr: ToastrService) { }

  ngOnInit() {
    this.getData();
    this.getPosition();
    this.getPrivilege();
  }

  getData() {
    this.http.get(environment.apiUrl + '/member').subscribe(res => {
      const raw = JSON.parse(JSON.stringify(res));
      this.data = raw.members;
    })
  }

  getPosition() {
    this.http.get(environment.apiUrl + '/allposition').subscribe(res => {
      const row = JSON.parse(JSON.stringify(res));
      this.positions = row.positions;
    });
  }

  getPrivilege() {
    this.http.get(environment.apiUrl + '/allprivilege').subscribe(res => {
      const raw = JSON.parse(JSON.stringify(res));
      this.privilaeges = raw.privileges;
    });
  }

  changePrivilege(id, old, user) {
    const body = {
      usrId : user,
      privilege : id
    }

    if(id !== old){
      this.http.put(environment.apiUrl + '/provide/privilege', body).subscribe(res => {
        const raw = JSON.parse(JSON.stringify(res));
        if(raw.codeStatus === 200){
          this.toatsr.success('อัพเดทสิทธิพิเศษของผู้ใช้สำเร็จ', 'แจ้งเตือน');
          this.getData();
        }else{
          this.toatsr.warning('อัพเดทสิทธิพิเศษของผู้ใช้ไม่สำเร็จ', 'เกิดข้อผิดพลาด');
        }
      });
    }
  }

  changePosition(id, old, user) {

    const body = {
      userId: user,
	    position: id
    }
    
    if(id !== old){
      this.http.put(environment.apiUrl + '/provide/position', body).subscribe(res => {
        const raw = JSON.parse(JSON.stringify(res));
        if(raw.codeStatus === 200){
          this.toatsr.success('อัพเดทตำแหน่งของผู้ใช้สำเร็จ', 'แจ้งเตือน');
          this.getData();
        }else{
          this.toatsr.warning('อัพเดทตำแหน่งของผู้ใช้ไม่สำเร็จ', 'เกิดข้อผิดพลาด');
        }
      })
    }
  }

}
